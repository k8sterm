//#define K8STERM_SELPASTE_DEBUG


////////////////////////////////////////////////////////////////////////////////
// selection
static void xfixsel (void) {
  if (lastSelStr != NULL) {
    XSetSelectionOwner(xw.dpy, XA_PRIMARY, xw.win, CurrentTime);
    XSetSelectionOwner(xw.dpy, XA_CLIPBOARD, xw.win, CurrentTime);
  } else {
    if (XGetSelectionOwner(xw.dpy, XA_PRIMARY) == xw.win) XSetSelectionOwner(xw.dpy, XA_PRIMARY, None, CurrentTime);
    if (XGetSelectionOwner(xw.dpy, XA_CLIPBOARD) == xw.win) XSetSelectionOwner(xw.dpy, XA_CLIPBOARD, None, CurrentTime);
  }
  XFlush(xw.dpy);
}


static void xevtcbselclear (XEvent *e) {
  (void)e;
  //k8t_selClear(curterm);
  for (int f = 0; f < term_count; ++f) {
    K8Term *t = term_array[f];
    k8t_selClear(t);
  }
}


static void k8stx_more_selection (void) {
  if (!xw.paste_term) return;

  #ifdef K8STERM_SELPASTE_DEBUG
  fprintf(stderr, "SELCOMES: paste_waiting_targets=%d; paste_incr_mode=%d; paste_utf8=%d; past_wasbrcp=%d; paste_cp=%u\n", xw.paste_waiting_targets, xw.paste_incr_mode, xw.paste_utf8, xw.past_wasbrcp, xw.paste_cp);
  #endif

  if (!curterm || xw.paste_term != curterm) {
    // abort
    k8stx_finish_paste();
    XDeleteProperty(xw.dpy, xw.win, XA_VT_SELECTION);
    return;
  }

  // this is processed by selection event handler (at least it should)
  if (xw.paste_waiting_targets) return;

  if (K8T_DATA(curterm)->cmdline.cmdMode == K8T_CMDMODE_MESSAGE) {
    // abort
    k8stx_finish_paste();
    XDeleteProperty(xw.dpy, xw.win, XA_VT_SELECTION);
    return;
  }

  unsigned long nitems, ofs, rem;
  Atom type;
  int format;
  uint8_t *data = NULL;

  ofs = 0;
  if (XGetWindowProperty(xw.dpy, xw.win, XA_VT_SELECTION, ofs, BUFSIZ/4, False, AnyPropertyType, &type, &format, &nitems, &rem, &data)) {
    #ifdef K8STERM_SELPASTE_DEBUG
    fprintf(stderr, "Clipboard allocation failed\n");
    #endif
    // abort
    k8stx_finish_paste();
    XDeleteProperty(xw.dpy, xw.win, XA_VT_SELECTION);
    return;
  }

  // end-of-paste signal
  if (nitems == 0) {
    XFree(data);
    k8stx_finish_paste();
    XDeleteProperty(xw.dpy, xw.win, XA_VT_SELECTION);
    return;
  }

  // waiting for the first part?
  if (xw.paste_incr_mode == 0) {
    xw.paste_cp = 0;
    if (type == XA_INCR) {
      if (format != 32 || nitems != 1) {
        #ifdef K8STERM_SELPASTE_DEBUG
        fprintf(stderr, "SELECTION: invalid INCR protocol handshake! (nitems=%u; format=%u)\n", (unsigned)nitems, (unsigned)format);
        #endif
        XFree(data);
        // abort
        k8stx_finish_paste();
        return;
      }
      uint32_t plen = *(const uint32_t *)data;
      #ifdef K8STERM_SELPASTE_DEBUG
      fprintf(stderr, "SELECTION: INCR protocol (total size=%u)\n", plen);
      #endif
      XFree(data);
      if (plen == 0 || plen > 0x1fffffffU) {
        // done
        k8stx_finish_paste();
        return;
      }
      xw.paste_incr_mode = (int)plen+1;
      xw.paste_last_activity = mclock_ticks();
      // ask for data
      XDeleteProperty(xw.dpy, xw.win, XA_VT_SELECTION);
      return;
    }
    xw.paste_incr_mode = -1;
    xw.past_wasbrcp = 0;
  }

  char *ucbuf = NULL;
  int ucbufsize = 0;
  char ecbuf[4];

  for (;;) {
    int blen = (int)(format != 8 ? nitems*format/8 : nitems);
    #ifdef K8STERM_SELPASTE_DEBUG
    fprintf(stderr, "  XSEL-STATE: paste_waiting_targets=%d; paste_incr_mode=%d; paste_utf8=%d; past_wasbrcp=%d; paste_cp=%u; blen=%d; rem=%u\n", xw.paste_waiting_targets, xw.paste_incr_mode, xw.paste_utf8, xw.past_wasbrcp, xw.paste_cp, blen, (unsigned)rem);
    #endif
    if (!blen) break;

    if (!xw.paste_utf8) {
      // non-utf
      int newsz = blen*4+64;
      if (ucbufsize < newsz) {
        char *n = realloc(ucbuf, newsz);
        if (n == NULL) {
          // abort
          k8stx_finish_paste();
          break;
        }
        ucbuf = n;
        ucbufsize = newsz;
      }
      blen = loc2utf(ucbuf, (const char *)data, blen);
      k8stx_output(ucbuf, blen);
      if (xw.paste_incr_mode > 1) {
        if ((xw.paste_incr_mode -= blen) < 1) xw.paste_incr_mode = 1;
      }
    } else {
      // utf
      uint8_t *dp = data;
      int left = blen;
      if (!sxed_utf8_valid_cp(xw.paste_cp)) {
        while (left && !sxed_utf8_valid_cp(xw.paste_cp)) {
          xw.paste_cp = sxed_utf8d_consume(xw.paste_cp, *dp++);
          --left;
          if (xw.paste_incr_mode > 1) --xw.paste_incr_mode;
        }

        if (!sxed_utf8_valid_cp(xw.paste_cp)) {
          if (left != 0) {
            // this should not happen!
            #ifdef K8STERM_SELPASTE_DEBUG
            fprintf(stderr, "k8stx_more_selection: utf decoding error!\n");
            #endif
            // abort
            k8stx_finish_paste();
            break;
          }
          if (!sxed_utf8_printable_cp(xw.paste_cp)) xw.paste_cp = SXED_UTF8_REPLACEMENT_CP;
          int eclen = k8t_UTF8Encode(ecbuf, xw.paste_cp);
          k8stx_output(ecbuf, eclen);
        }
      }

      int newsz = blen*4+64;
      if (ucbufsize < newsz) {
        char *n = realloc(ucbuf, newsz);
        if (n == NULL) {
          // abort
          k8stx_finish_paste();
          break;
        }
        ucbuf = n;
        ucbufsize = newsz;
      }

      int ucbufpos = 0;
      while (left--) {
        if (xw.paste_incr_mode > 1) --xw.paste_incr_mode;
        xw.paste_cp = sxed_utf8d_consume(xw.paste_cp, *dp++);
        if (sxed_utf8_valid_cp(xw.paste_cp)) {
          if (!sxed_utf8_printable_cp(xw.paste_cp)) xw.paste_cp = SXED_UTF8_REPLACEMENT_CP;
          int eclen = k8t_UTF8Encode(ucbuf+ucbufpos, xw.paste_cp);
          ucbufpos += eclen;
        }
      }

      k8stx_output(ucbuf, ucbufpos);
    }

    /* number of 32-bit chunks returned */
    ofs += nitems*format/32;

    if (!rem || xw.paste_incr_mode == 1) break;

    XFree(data);
    data = NULL;
    if (XGetWindowProperty(xw.dpy, xw.win, XA_VT_SELECTION, ofs, BUFSIZ/4, False, AnyPropertyType, &type, &format, &nitems, &rem, &data)) {
      data = NULL; // just in case
      #ifdef K8STERM_SELPASTE_DEBUG
      fprintf(stderr, "Clipboard allocation failed\n");
      #endif
      // abort
      k8stx_finish_paste();
      XDeleteProperty(xw.dpy, xw.win, XA_VT_SELECTION);
      return;
    }
  }
  if (data) XFree(data);

  if (ucbuf != NULL) free(ucbuf);

  xw.paste_last_activity = mclock_ticks();

  #ifdef K8STERM_SELPASTE_DEBUG
  fprintf(stderr, "  XSEL-STATE: paste_waiting_targets=%d; paste_incr_mode=%d; paste_utf8=%d; past_wasbrcp=%d; paste_cp=%u\n", xw.paste_waiting_targets, xw.paste_incr_mode, xw.paste_utf8, xw.past_wasbrcp, xw.paste_cp);
  #endif

  // done INCR paste?
  if (xw.paste_incr_mode == 1 || xw.paste_incr_mode < 0) {
    #ifdef K8STERM_SELPASTE_DEBUG
    fprintf(stderr, "*** PASTE COMPLETE\n");
    #endif
    k8stx_finish_paste();
    // anyway, the sender may want to send us zero-length "end of paste" signal
    XDeleteProperty(xw.dpy, xw.win, XA_VT_SELECTION);
  } else {
    // ask for more
    #ifdef K8STERM_SELPASTE_DEBUG
    fprintf(stderr, "*** ASKING FOR MORE!\n");
    #endif
    XDeleteProperty(xw.dpy, xw.win, XA_VT_SELECTION);
  }
}


static void xevtcbpropnotify (XEvent *e) {
  if (e->xproperty.state == PropertyNewValue) {
    #ifdef K8STERM_SELPASTE_DEBUG
    char *pn = XGetAtomName(xw.dpy, e->xproperty.atom);
    fprintf(stderr, "PROP: new value for property '%s' (time=%u)\n", pn, (unsigned)e->xproperty.time);
    XFree(pn);
    #endif
    if (e->xproperty.atom == XA_VT_SELECTION) k8stx_more_selection();
  } else if (e->xproperty.state == PropertyDelete) {
    #ifdef K8STERM_SELPASTE_DEBUG
    char *pn = XGetAtomName(xw.dpy, e->xproperty.atom);
    fprintf(stderr, "PROP: deleted property '%s' (time=%u)\n", pn, (unsigned)e->xproperty.time);
    XFree(pn);
    #endif
  } else {
    //fprintf(stderr, "PROP: unknown event subtype!\n");
  }
}


static void xevtcbselnotify (XEvent *e) {
  unsigned long nitems, rem;
  int format;
  uint8_t *data;
  Atom type;
  XSelectionEvent *se = (XSelectionEvent *)e;

  #ifdef K8STERM_SELPASTE_DEBUG
  char *prpname = XGetAtomName(xw.dpy, se->property);
  char *tgtname = XGetAtomName(xw.dpy, se->target);
  fprintf(stderr, "SELEVENT: prop=[%s]; target=[%s]\n", prpname, tgtname);
  fprintf(stderr, "SSTATE: paste_waiting_targets=%d; paste_incr_mode=%d; paste_utf8=%d; past_wasbrcp=%d; paste_cp=%u\n", xw.paste_waiting_targets, xw.paste_incr_mode, xw.paste_utf8, xw.past_wasbrcp, xw.paste_cp);
  XFree(prpname);
  XFree(tgtname);
  #endif

  if (!xw.paste_term || xw.paste_term != curterm) {
    k8stx_finish_paste();
    return;
  }

  if (curterm == NULL || K8T_DATA(curterm)->cmdline.cmdMode == K8T_CMDMODE_MESSAGE) return;
  if (se->property != XA_VT_SELECTION) return;

  if (xw.paste_term) {
    // if we are not waiting for targets, do nothing
    // this can happen if the sender generates selection event for each selection part
    if (!xw.paste_waiting_targets) {
      // just in case, this may be encoding type
      if (xw.paste_incr_mode == 0) {
             if (se->target == XA_UTF8) xw.paste_utf8 = 1;
        else if (se->target == XA_STRING || se->target == XA_CSTRING) xw.paste_utf8 = 0;
      }
      return;
    }
  }

  if (se->target != XA_TARGETS) {
    // abort
    k8stx_finish_paste();
    return;
  }

  // parse targets
  Atom rqtype = None, *targ;
  if (XGetWindowProperty(xw.dpy, xw.win, se->property, 0, 65536, False, XA_ATOM, &type, &format, &nitems, &rem, &data)) {
    //fprintf(stderr, "no targets\n");
    rqtype = XA_STRING;
  } else {
    for (targ = (Atom *)data; nitems > 0; --nitems, ++targ) {
           if (*targ == XA_UTF8) rqtype = XA_UTF8;
      else if (*targ == XA_STRING && rqtype == None) rqtype = XA_STRING;
    }
    XFree(data);
  }
  if (rqtype != None) {
    xw.paste_utf8 = (rqtype == XA_UTF8);
    XConvertSelection(xw.dpy, se->selection, rqtype, XA_VT_SELECTION, xw.win, CurrentTime);
  } else {
    // abort
    k8stx_finish_paste();
  }

  xw.paste_last_activity = mclock_ticks();
  xw.paste_waiting_targets = 0;

  // ask for more
  XDeleteProperty(xw.dpy, xw.win, XA_VT_SELECTION);
}


static void selpaste (K8Term *term, Atom which) {
  if (term == NULL) return;
  if (XGetSelectionOwner(xw.dpy, which) == None) return;
  //XConvertSelection(xw.dpy, which, term->sel.xtarget, XA_VT_SELECTION, xw.win, CurrentTime);
  //FIXME: do we need to remove selection atom here?
  k8stx_finish_paste();
  if (!curterm || K8T_DATA(curterm)->cmdline.cmdMode == K8T_CMDMODE_MESSAGE) return;
  xw.paste_term = curterm;
  xw.paste_waiting_targets = 1;
  xw.paste_incr_mode = 0;
  xw.paste_utf8 = 0;
  xw.past_wasbrcp = 0;
  xw.paste_cp = 0;
  xw.paste_last_activity = mclock_ticks();
  #ifdef K8STERM_SELPASTE_DEBUG
  char *name = XGetAtomName(xw.dpy, which);
  fprintf(stderr, "ASKED FOR selection: [%s]\n", name);
  XFree(name);
  #endif
  XDeleteProperty(xw.dpy, xw.win, XA_VT_SELECTION);
  XConvertSelection(xw.dpy, which, XA_TARGETS, XA_VT_SELECTION, xw.win, CurrentTime);
  /*
  if (which == XA_PRIMARY) selpaste(XA_SECONDARY);
  else if (which == XA_SECONDARY) selpaste(XA_CLIPBOARD);
  */
}


static void xevtcbselrequest (XEvent *e) {
  XSelectionRequestEvent *xsre;
  XSelectionEvent xev;
  //
  if (lastSelStr == NULL) return;
  xsre = (XSelectionRequestEvent *)e;
  xev.type = SelectionNotify;
  xev.requestor = xsre->requestor;
  xev.selection = xsre->selection;
  xev.target = xsre->target;
  xev.time = xsre->time;
  /* reject */
  xev.property = None;
  if (xsre->target == XA_TARGETS) {
    /* respond with the supported type */
    Atom tlist[3] = {XA_UTF8, XA_STRING, XA_TARGETS};
    //
    XChangeProperty(xsre->display, xsre->requestor, xsre->property, XA_ATOM, 32, PropModeReplace, (uint8_t *)tlist, 3);
    xev.property = xsre->property;
  } else if (xsre->target == XA_UTF8 && lastSelStr != NULL) {
    XChangeProperty(xsre->display, xsre->requestor, xsre->property, XA_UTF8, 8, PropModeReplace, (uint8_t *)lastSelStr, strlen(lastSelStr));
    xev.property = xsre->property;
  } else if (xsre->target == XA_STRING && lastSelStr != NULL) {
    char *s = malloc(strlen(lastSelStr)*4+8);
    //
    if (s != NULL) {
      int len = utf2loc(s, lastSelStr, strlen(lastSelStr));
      //
      XChangeProperty(xsre->display, xsre->requestor, xsre->property, XA_STRING, 8, PropModeReplace, (uint8_t *)s, len);
      xev.property = xsre->property;
      free(s);
    }
  }
  /* all done, send a notification to the listener */
  if (!XSendEvent(xsre->display, xsre->requestor, True, 0, (XEvent *)&xev)) {
    #ifdef K8STERM_SELPASTE_DEBUG
    fprintf(stderr, "Error sending SelectionNotify event\n");
    #endif
  }
}
