////////////////////////////////////////////////////////////////////////////////
static K8Term *oldTerm;
static int oldTermIdx;
static K8Term *newTerm;
static int newTermIdx;
static int newTermSwitch;


#define CMDLD(_t)  (K8T_DATA(_t)->cmdline)


typedef void (*CmdHandlerFn) (const char *cmdname, char *argstr);

typedef struct {
  const char *name;
  CmdHandlerFn fn;
} Command;


static void cmdPastePrimary (const char *cmdname, char *argstr) {
  (void)cmdname; (void)argstr;
  selpaste(curterm, XA_PRIMARY);
}


static void cmdPasteSecondary (const char *cmdname, char *argstr) {
  (void)cmdname; (void)argstr;
  selpaste(curterm, XA_SECONDARY);
}


static void cmdPasteClipboard (const char *cmdname, char *argstr) {
  (void)cmdname; (void)argstr;
  selpaste(curterm, XA_CLIPBOARD);
}


static void cmdHideSelection (const char *cmdname, char *argstr) {
  (void)cmdname; (void)argstr;
  k8t_selHide(curterm);
}


static void cmdExec (const char *cmdname, char *argstr) {
  (void)cmdname;
  if (curterm != NULL) {
    if (K8T_DATA(curterm)->execcmd != NULL) free(K8T_DATA(curterm)->execcmd);
    K8T_DATA(curterm)->execcmd = (argstr[0] ? strdup(argstr) : NULL);
  }
}


static int parseTabArgs (char *argstr, int *noswitch, int nowrap, int idx) {
  for (;;) {
    char *arg;
    //
    while (*argstr && isspace(*argstr)) ++argstr;
    if (!argstr[0]) break;
    if (iniParseArguments(argstr, "s-R-", &arg, &argstr) != NULL) break;
    //
    if (strcasecmp(arg, "noswitch") == 0) *noswitch = 1;
    else if (strcasecmp(arg, "switch") == 0) *noswitch = 0;
    else if (strcasecmp(arg, "nowrap") == 0) nowrap = 1;
    else if (strcasecmp(arg, "wrap") == 0) nowrap = 0;
    else if (strcasecmp(arg, "first") == 0) idx = 0;
    else if (strcasecmp(arg, "last") == 0) idx = term_count-1;
    else if (strcasecmp(arg, "prev") == 0) idx = -1;
    else if (strcasecmp(arg, "next") == 0) idx = -2;
    else {
      long int n = -1;
      char *eptr;
      //
      n = strtol(arg, &eptr, 0);
      if (!eptr[0] && n >= 0 && n < term_count) idx = n;
    }
  }
  switch (idx) {
    case -1: // prev
      if ((idx = termidx-1) < 0) idx = nowrap ? 0 : term_count-1;
      break;
    case -2: // next
      if ((idx = termidx+1) >= term_count) idx = nowrap ? term_count-1 : 0;
      break;
  }
  return idx;
}


static void flushNewTerm (void) {
  if (newTerm != NULL) {
    if (newTermSwitch && curterm != NULL) curterm->lastActiveTime = mclock_ticks();
    //curterm = newTerm;
    //termidx = newTermIdx;
    k8t_tmInitialize(newTerm, term_array[0]->col, term_array[0]->row, opt_maxhistory);
    //termCreateXPixmap(newTerm);
    //newTerm->drawSetFG(newTerm, newTerm->defbg);
    k8t_tmClearRegion(newTerm, 0, 0, newTerm->col-1, newTerm->row-1);
    k8t_tmFullDirty(newTerm);
    //
    if (k8t_ttyNew(newTerm) != 0) {
      curterm = oldTerm;
      termidx = oldTermIdx;
      termfree(newTermIdx);
    } else {
      k8t_selInit(newTerm);
      k8t_ttyResize(newTerm);
      if (newTermSwitch) {
        curterm = NULL;
        termidx = 0;
        switchToTerm(newTermIdx, 1);
        oldTerm = curterm;
        oldTermIdx = termidx;
      } else {
        curterm = oldTerm;
        termidx = oldTermIdx;
      }
    }
    newTerm = NULL;
  }
}


static void cmdNewTab (const char *cmdname, char *argstr) {
  (void)cmdname;
  int noswitch = 0, idx;
  //
  if (opt_disabletabs) return;
  flushNewTerm();
  if ((newTerm = k8t_termalloc()) == NULL) return;
  k8t_tmInitialize(newTerm, term_array[0]->col, term_array[0]->row, opt_maxhistory);
  /*idx =*/ parseTabArgs(argstr, &noswitch, 0, termidx);
  idx = term_count-1;
  if (!noswitch) {
    if (curterm != NULL) curterm->lastActiveTime = mclock_ticks();
    oldTermIdx = idx;
  }
  newTermIdx = termidx = idx;
  curterm = newTerm;
  newTermSwitch = !noswitch;
}


static void cmdCloseTab (const char *cmdname, char *argstr) {
  (void)cmdname; (void)argstr;
  flushNewTerm();
  if (curterm != NULL && !curterm->dead) kill(K8T_DATA(curterm)->pid, SIGTERM);
}


static void cmdKillTab (const char *cmdname, char *argstr) {
  (void)cmdname; (void)argstr;
  flushNewTerm();
  if (!curterm->dead) kill(K8T_DATA(curterm)->pid, SIGKILL);
}


static void cmdSwitchToTab (const char *cmdname, char *argstr) {
  (void)cmdname;
  int noswitch = 0, idx;
  //
  flushNewTerm();
  idx = parseTabArgs(argstr, &noswitch, 0, -666);
  if (idx >= 0) {
    switchToTerm(idx, 1);
    oldTerm = curterm;
    oldTermIdx = termidx;
  }
}


static void cmdMoveTabTo (const char *cmdname, char *argstr) {
  (void)cmdname;
  int noswitch = 0, idx;
  //
  flushNewTerm();
  idx = parseTabArgs(argstr, &noswitch, 0, termidx);
  if (idx != termidx && idx >= 0 && idx < term_count) {
    K8Term *t = term_array[termidx];
    //
    // remove current term
    for (int f = termidx+1; f < term_count; ++f) term_array[f-1] = term_array[f];
    // insert term
    for (int f = term_count-2; f >= idx; --f) term_array[f+1] = term_array[f];
    term_array[idx] = t;
    termidx = idx;
    oldTerm = t;
    oldTermIdx = idx;
    fixFirstTab();
    updateTabBar = 1;
  }
}


static void cmdDefaultFG (const char *cmdname, char *argstr) {
  (void)cmdname;
  char *s = NULL;
  int c;
  //
  if (iniParseArguments(argstr, "i{0,511}|s-", &c, &s) == NULL) {
    if (s != NULL && tolower(s[0]) == 'g') defaultFG = c; else curterm->deffg = c;
  }
}


static void cmdDefaultBG (const char *cmdname, char *argstr) {
  (void)cmdname;
  char *s = NULL;
  int c;
  //
  if (iniParseArguments(argstr, "i{0,511}|s-", &c) == NULL) {
    if (s != NULL && tolower(s[0]) == 'g') {
      defaultBG = c;
    } else {
      curterm->defbg = c;
      XSetWindowBackground(xw.dpy, xw.win, getColor(curterm->defbg));
      if (newTerm == NULL) {
        k8t_tmFullDirty(curterm);
        k8t_drawTerm(curterm, 1);
        xclearunused();
      }
    }
  }
}


static void scrollHistory (K8Term *term, int delta) {
  if (term == NULL || term->maxhistory < 1) return; // no history
  term->topline += delta;
  if (term->topline > term->maxhistory) term->topline = term->maxhistory;
  if (term->topline < 0) term->topline = 0;
  k8t_tmFullDirty(term);
  k8t_drawTerm(term, 1);
}


static void cmdScrollHistoryLineUp (const char *cmdname, char *argstr) {
  (void)cmdname; (void)argstr;
  scrollHistory(curterm, 1);
}


static void cmdScrollHistoryPageUp (const char *cmdname, char *argstr) {
  (void)cmdname; (void)argstr;
  scrollHistory(curterm, curterm->row);
}


static void cmdScrollHistoryLineDown (const char *cmdname, char *argstr) {
  (void)cmdname; (void)argstr;
  scrollHistory(curterm, -1);
}


static void cmdScrollHistoryPageDown (const char *cmdname, char *argstr) {
  (void)cmdname; (void)argstr;
  scrollHistory(curterm, -curterm->row);
}


static void cmdScrollHistoryTop (const char *cmdname, char *argstr) {
  (void)cmdname; (void)argstr;
  scrollHistory(curterm, curterm->linecount);
}


static void cmdScrollHistoryBottom (const char *cmdname, char *argstr) {
  (void)cmdname; (void)argstr;
  scrollHistory(curterm, -curterm->linecount);
}


static void cmdCommandMode (const char *cmdname, char *argstr) {
  (void)cmdname; (void)argstr;
  if (curterm != NULL) {
    if (CMDLD(curterm).cmdMode == K8T_CMDMODE_NONE) {
      tcmdlineinit(curterm, &CMDLD(curterm));
    } else {
      tcmdlinehide(curterm, &CMDLD(curterm));
    }
  }
}


// [show|hide]
static void cmdCursor (const char *cmdname, char *argstr) {
  (void)cmdname;
  if (curterm != NULL) {
    char *s;
    //
    if (iniParseArguments(argstr, "s!-", &s) != NULL) {
      tcmdlinemsgf(curterm, &CMDLD(curterm), "cursor is %s", ((curterm->c.state&K8T_CURSOR_HIDE) ? "hidden" : "visible"));
    } else {
      if (strcasecmp(s, "show") == 0) curterm->c.state &= ~K8T_CURSOR_HIDE;
      else if (strcasecmp(s, "hide") == 0) curterm->c.state |= K8T_CURSOR_HIDE;
      k8t_tmWantRedraw(curterm, 0);
    }
  }
}


static void cmdReset (const char *cmdname, char *argstr) {
  (void)cmdname;
  char *s = NULL;
  //
  if (curterm != NULL) {
    if (iniParseArguments(argstr, "|s-", &s) == NULL) {
      if (s != NULL) {
        switch (tolower(s[0])) {
          case 'a': // all
            k8t_tmResetMode(curterm);
            return;
          case 'c': // colors
            k8t_tmResetAttrs(curterm);
            return;
          case 'g': // graphics
            curterm->mode &= ~(K8T_MODE_GFX0|K8T_MODE_GFX1);
            curterm->charset = K8T_MODE_GFX0;
            return;
          case 'u': // cursor
            curterm->csaved.state = curterm->c.state = K8T_CURSOR_DEFAULT;
            return;
        }
      }
    }
    tcmdlinemsgf(curterm, &CMDLD(curterm), "Reset (a)ll | (c)olor | (g)raphics | c(u)rsor");
  }
}


// [norm|alt]
static void cmdScreen (const char *cmdname, char *argstr) {
  (void)cmdname;
  if (curterm != NULL) {
    char *s;
    //
    if (iniParseArguments(argstr, "s!-", &s) != NULL) {
      tcmdlinemsgf(curterm, &CMDLD(curterm), "screen: %s", (K8T_ISSET(curterm, K8T_MODE_ALTSCREEN) ? "alt" : "norm"));
    } else {
      if (strcasecmp(s, "norm") == 0) {
        if (K8T_ISSET(curterm, K8T_MODE_ALTSCREEN)) k8t_tmSwapScreen(curterm);
      } else if (strcasecmp(s, "alt") == 0) {
        if (!K8T_ISSET(curterm, K8T_MODE_ALTSCREEN)) k8t_tmSwapScreen(curterm);
      }
    }
  }
}


// [on|off]
static void cmdMouseReports (const char *cmdname, char *argstr) {
  (void)cmdname;
  if (curterm != NULL) {
    int b;
    //
    if (iniParseArguments(argstr, "b", &b) != NULL) {
      tcmdlinemsgf(curterm, &CMDLD(curterm), "mouse reports are o%s", (K8T_ISSET(curterm, K8T_MODE_MOUSE) ? "n" : "ff"));
    } else {
      if (b) curterm->mode |= K8T_MODE_MOUSEBTN; else curterm->mode &= ~K8T_MODE_MOUSEBTN;
    }
  }
}


static int cmd_parseIntArg (const char *fmt, char *argstr, int *b, int *global, int *toggle) {
  while (argstr[0]) {
    char *s = NULL;
    //
    if (iniParseArguments(argstr, "R-", &argstr) != NULL) break;
    if (!argstr[0]) break;
    //
    if (iniParseArguments(argstr, "s!-R-", &s, &argstr) != NULL) break;
    if (global && tolower(s[0]) == 'g') {
      *global = 1;
    } else if (toggle && tolower(s[0]) == 't') {
      *toggle = 1;
    } else {
      if (!b || iniParseArguments(s, fmt, b) != NULL) return -1;
    }
  }
  return 0;
}


static void cmdUTF8Locale (const char *cmdname, char *argstr) {
  (void)cmdname;
  int b = -1, toggle = 0;
  //
  if (cmd_parseIntArg("b", argstr, &b, NULL, &toggle) != 0) return;
  if (b == -1) {
    tcmdlinemsgf(curterm, &CMDLD(curterm), "UTF8Locale: %s", ((needConversion ? !curterm->needConv : 1) ? "yes" : "no"));
  } else {
    if (!needConversion) b = 1;
    if (curterm != NULL) curterm->needConv = !b;
  }
}


static void cmdAudibleBell (const char *cmdname, char *argstr) {
  (void)cmdname;
  int b = -1, toggle = 0, global = 0;
  //
  if (curterm == NULL) return;
  if (cmd_parseIntArg("b", argstr, &b, &global, &toggle) != 0) return;
  //
  if (b == -1) {
    tcmdlinemsgf(curterm, &CMDLD(curterm), "AudibleBell: %s", (global ? opt_audiblebell : (curterm->belltype&K8T_BELL_AUDIO)) ? "yes" : "no");
  } else {
    if (toggle) {
      if (global) opt_audiblebell = !opt_audiblebell; else curterm->belltype ^= K8T_BELL_AUDIO;
    } else {
      if (global) opt_audiblebell = b; else curterm->belltype = (curterm->belltype&~K8T_BELL_AUDIO)|(b!=0?K8T_BELL_AUDIO:0);
    }
  }
}


static void cmdUrgentBell (const char *cmdname, char *argstr) {
  (void)cmdname;
  int b = -1, toggle = 0, global = 0;
  //
  if (curterm == NULL) return;
  if (cmd_parseIntArg("b", argstr, &b, &global, &toggle) != 0) return;
  //
  if (b == -1) {
    tcmdlinemsgf(curterm, &CMDLD(curterm), "UrgentBell: %s", (global ? opt_urgentbell : (curterm->belltype&K8T_BELL_URGENT)) ? "yes" : "no");
  } else {
    if (toggle) {
      if (global) opt_urgentbell = !opt_urgentbell; else curterm->belltype ^= K8T_BELL_URGENT;
    } else {
      if (global) opt_urgentbell = b; else curterm->belltype = (curterm->belltype&~K8T_BELL_URGENT)|(b!=0?K8T_BELL_URGENT:0);
    }
  }
}


static void cmdMonochrome (const char *cmdname, char *argstr) {
  (void)cmdname;
  int b = -1, global = 0;
  //
  if (curterm == NULL) return;
  if (cmd_parseIntArg("i{0,3}", argstr, &b, &global, NULL) != 0) return;
  //
  if (b == -1) {
    b = (global ? globalBW : curterm->blackandwhite);
    tcmdlinemsgf(curterm, &CMDLD(curterm), "Monochrome: %d", b);
  } else {
    if (global) {
      if (b == 3) tcmdlinemsg(curterm, &CMDLD(curterm), "Monochrome-global can't be '3'!");
      else globalBW = b;
    } else {
      curterm->blackandwhite = b;
    }
  }
  k8t_tmFullDirty(curterm);
  updateTabBar = 1;
}


static void cmdCursorBlink (const char *cmdname, char *argstr) {
  (void)cmdname;
  int b = -1, global = 0;
  //
  if (curterm == NULL) return;
  if (cmd_parseIntArg("i{0,10000}", argstr, &b, &global, NULL) != 0) return;
  //
  if (b == -1) {
    b = (global ? opt_cursorBlink : curterm->curblink);
    tcmdlinemsgf(curterm, &CMDLD(curterm), "CursorBlink: %d", b);
  } else {
    if (global) {
      opt_cursorBlink = b;
    } else {
      curterm->curblink = b;
      curterm->curbhidden = 0;
    }
  }
  k8t_tmFullDirty(curterm);
  updateTabBar = 1;
}


static void cmdCursorBlinkInactive (const char *cmdname, char *argstr) {
  (void)cmdname;
  int b = -1, global = 0, toggle = 0, *iptr;
  //
  if (curterm == NULL) return;
  if (cmd_parseIntArg("b", argstr, &b, &global, &toggle) != 0) return;
  //
  iptr = (global ? &opt_cursorBlinkInactive : &curterm->curblinkinactive);
  if (b != -1) {
    if (toggle) *iptr = !(*iptr); else *iptr = b;
    k8t_drawTerm(curterm, 0);
  }
}


static void cmdIgnoreClose (const char *cmdname, char *argstr) {
  (void)cmdname;
  int b = -666;
  //
  if (curterm == NULL) return;
  if (cmd_parseIntArg("i{-1,1}", argstr, &b, NULL, NULL) != 0) return;
  //
  if (b == -666) {
    tcmdlinemsgf(curterm, &CMDLD(curterm), "IgnoreClose: %d", opt_ignoreclose);
  } else {
    opt_ignoreclose = b;
  }
}


static void cmdMaxHistory (const char *cmdname, char *argstr) {
  (void)cmdname;
  int b = -1, global = 0;
  //
  if (curterm == NULL) return;
  if (cmd_parseIntArg("i{0,65535}", argstr, &b, &global, NULL) != 0) return;
  //
  if (b == -1) {
    tcmdlinemsgf(curterm, &CMDLD(curterm), "MaxHistory: %d", (global?opt_maxhistory:curterm->maxhistory));
  } else {
    if (!global) k8t_tmAdjMaxHistory(curterm, b); else opt_maxhistory = b;
  }
}


static void cmdMaxDrawTimeout (const char *cmdname, char *argstr) {
  (void)cmdname;
  int b = -1;
  //
  if (curterm == NULL) return;
  if (cmd_parseIntArg("i{100,60000}", argstr, &b, NULL, NULL) != 0) return;
  //
  if (b == -1) {
    tcmdlinemsgf(curterm, &CMDLD(curterm), "MaxDrawTimeout: %d", opt_maxdrawtimeout);
  } else {
    opt_maxdrawtimeout = b;
  }
}


static void cmdDrawTimeout (const char *cmdname, char *argstr) {
  (void)cmdname;
  int b = -1;
  //
  if (curterm == NULL) return;
  if (cmd_parseIntArg("i{5,30000}", argstr, &b, NULL, NULL) != 0) return;
  //
  if (b == -1) {
    tcmdlinemsgf(curterm, &CMDLD(curterm), "DrawTimeout: %d", opt_drawtimeout);
  } else {
    opt_drawtimeout = b;
  }
}


static void cmdTabPosition (const char *cmdname, char *argstr) {
  (void)cmdname;
  int newpos = -1;
  //
  while (argstr[0]) {
    char *s = NULL;
    //
    if (iniParseArguments(argstr, "R-", &argstr) != NULL) break;
    if (!argstr[0]) break;
    //
    if (iniParseArguments(argstr, "s!-R-", &s, &argstr) != NULL) break;
    if (tolower(s[0]) == 't') newpos = 1;
    else if (tolower(s[0]) == 'b') newpos = 0;
    else if (iniParseArguments(s, "i{0,1}", &newpos) != NULL) return;
  }
  //
  if (newpos == -1) {
    tcmdlinemsgf(curterm, &CMDLD(curterm), "TabPostion: %s", (opt_tabposition == 0 ? "bottom" : "top"));
  } else if (opt_tabposition != newpos) {
    opt_tabposition = newpos;
    updateTabBar = 1;
    k8t_tmFullDirty(curterm);
    k8t_drawTerm(curterm, 1);
    xdrawTabBar();
    xclearunused();
  }
}


static void cmdTabCount (const char *cmdname, char *argstr) {
  (void)cmdname;
  int b = -1;
  //
  if (curterm == NULL) return;
  if (cmd_parseIntArg("i{1,128}", argstr, &b, NULL, NULL) != 0) return;
  //
  if (b == -1) {
    tcmdlinemsgf(curterm, &CMDLD(curterm), "TabCount: %d", opt_tabcount);
  } else if (opt_tabcount != b) {
    opt_tabcount = b;
    fixFirstTab();
    updateTabBar = 1;
    xdrawTabBar();
  }
}


static void cmdDoFullRedraw (const char *cmdname, char *argstr) {
  (void)cmdname; (void)argstr;
  updateTabBar = 1;
  k8t_tmFullDirty(curterm);
  xclearunused();
  k8t_drawTerm(curterm, 1);
  xdrawTabBar();
}


static void cmdTitle (const char *cmdname, char *argstr) {
  (void)cmdname;
  if (curterm != NULL) {
    char *s = NULL;
    //
    if (iniParseArguments(argstr, "s-", &s) != NULL || s == NULL) return;
    memset(curterm->title, 0, sizeof(curterm->title));
    while (strlen(s) > K8T_ESC_TITLE_SIZ) k8t_UTF8ChopLast(s);
    fprintf(stderr, "[%s]\n", s);
    strncpy(curterm->title, s, K8T_ESC_TITLE_SIZ);
    fixWindowTitle(curterm);
    updateTabBar = 1;
    xdrawTabBar();
  }
}


static void cmdFastRedraw (const char *cmdname, char *argstr) {
  (void)cmdname;
  int b = -1, global = 0, toggle = 0, *iptr;
  //
  if (curterm == NULL) return;
  if (cmd_parseIntArg("b", argstr, &b, &global, &toggle) != 0) return;
  //
  iptr = (global ? &opt_fastredraw : &curterm->fastredraw);
  if (b != -1) {
    if (toggle) *iptr = !(*iptr); else *iptr = b;
    k8t_drawTerm(curterm, 0);
  } else {
    tcmdlinemsgf(curterm, &CMDLD(curterm), "FastRedraw: %s", (*iptr ? "yes" : "no"));
  }
}


static void cmdScrollClear (const char *cmdname, char *argstr) {
  (void)cmdname;
  int b = -666, global = 0, *iptr;
  //
  if (curterm == NULL) return;
  if (cmd_parseIntArg("i{-1,2}", argstr, &b, &global, NULL) != 0) return;
  //
  iptr = (global ? &opt_scrollclear : &curterm->clearOnPartialScrollMode);
  if (b == -666) {
    tcmdlinemsgf(curterm, &CMDLD(curterm), "ScrollClear: %d", *iptr);
  } else if (b != -666) {
    *iptr = b;
  }
}


static void cmdAbout (const char *cmdname, char *argstr) {
  (void)cmdname; (void)argstr;
  if (curterm != NULL) {
    tcmdlinemsgf(curterm, &CMDLD(curterm), "k8sterm " VERSION);
  }
}


static void cmdDebugDump (const char *cmdname, char *argstr) {
  (void)cmdname;
  if (curterm != NULL) {
    char *s = NULL;
    //
    if (iniParseArguments(argstr, "s-", &s) == NULL) {
      if (s != NULL) {
        switch (tolower(s[0])) {
          case 'o': // off
          case 'n': // none
            curterm->dumpescapes = 0;
            curterm->dumpeskeys = 0;
            return;
          case 'a': // all
            curterm->dumpescapes = 1;
            curterm->dumpeskeys = 1;
            return;
          case 'e': // escapes
            curterm->dumpescapes = !curterm->dumpescapes;
            return;
          case 'k': // keys
            curterm->dumpeskeys = !curterm->dumpeskeys;
            return;
        }
      }
    }
    tcmdlinemsgf(curterm, &CMDLD(curterm), "DbgDump (n)one | (a)ll | toggle (e)scapes | toggle extended (k)eys");
  }
}


// 0: none
// 1: in center
// 2: at left
// 3: at right
static void cmdTabEllipsis (const char *cmdname, char *argstr) {
  (void)cmdname;
  int b = -1;
  //
  if (curterm == NULL) return;
  if (cmd_parseIntArg("i{0,3}", argstr, &b, NULL, NULL) != 0) return;
  //
  if (b != -1) {
    if (opt_tabellipsis != b) {
      opt_tabellipsis = b;
      updateTabBar = 1;
      xdrawTabBar();
    }
  } else {
    tcmdlinemsgf(curterm, &CMDLD(curterm), "TabEllipsis: %d", opt_tabellipsis);
  }
}


static const Command commandList[] = {
  {"PastePrimary", cmdPastePrimary},
  {"PasteSecondary", cmdPasteSecondary},
  {"PasteClipboard", cmdPasteClipboard},
  {"HideSelection", cmdHideSelection},
  {"exec", cmdExec},
  {"NewTab", cmdNewTab}, // 'noswitch' 'next' 'prev' 'first' 'last'
  {"CloseTab", cmdCloseTab},
  {"KillTab", cmdKillTab},
  {"SwitchToTab", cmdSwitchToTab}, // 'next' 'prev' 'first' 'last' 'nowrap' 'wrap'
  {"MoveTabTo", cmdMoveTabTo}, // 'next' 'prev' 'first' 'last' 'nowrap' 'wrap'
  {"DefaultFG", cmdDefaultFG},
  {"DefaultBG", cmdDefaultBG},
  {"ScrollHistoryLineUp", cmdScrollHistoryLineUp},
  {"ScrollHistoryPageUp", cmdScrollHistoryPageUp},
  {"ScrollHistoryLineDown", cmdScrollHistoryLineDown},
  {"ScrollHistoryPageDown", cmdScrollHistoryPageDown},
  {"ScrollHistoryTop", cmdScrollHistoryTop},
  {"ScrollHistoryBottom", cmdScrollHistoryBottom},
  {"UTF8Locale", cmdUTF8Locale}, // 'on', 'off'
  {"AudibleBell", cmdAudibleBell},
  {"UrgentBell", cmdUrgentBell},
  {"CommandMode", cmdCommandMode},
  {"Cursor", cmdCursor},
  {"Reset", cmdReset},
  {"Screen", cmdScreen},
  {"MouseReports", cmdMouseReports},
  {"Monochrome", cmdMonochrome},
  {"Mono", cmdMonochrome},
  {"CursorBlink", cmdCursorBlink},
  {"CursorBlinkInactive", cmdCursorBlinkInactive},
  {"IgnoreClose", cmdIgnoreClose},
  {"MaxHistory", cmdMaxHistory},
  {"MaxDrawTimeout", cmdMaxDrawTimeout},
  //{"SwapDrawTimeout", cmdSwapDrawTimeout},
  {"DrawTimeout", cmdDrawTimeout},
  {"TabPosition", cmdTabPosition},
  {"TabCount", cmdTabCount},
  {"DoFullRedraw", cmdDoFullRedraw},
  {"Title", cmdTitle},
  {"FastRedraw", cmdFastRedraw},
  //{"FastUpdate", cmdFastUpdate},
  {"ScrollClear", cmdScrollClear},
  {"TabEllipsis", cmdTabEllipsis},
  //
  {"KeyBind", NULL},
  {"KeyTrans", NULL},
  {"KeyMap", NULL},
  {"UniMap", NULL},
  //
  {"DbgDump", cmdDebugDump},
  //
  {"About", cmdAbout},
/*
  {"term", cmdTermName},
  {"title", cmdWinTitle},
  {"tabsize", cmdTabSize},
  {"defaultcursorfg", cmdDefaultCursorFG},
  {"defaultcursorbg", cmdDefaultCursorBG},
  {"defaultinactivecursorfg", cmdDefaultInactiveCursorFG},
  {"defaultinactivecursorbg", cmdDefaultInactiveCursorBG},
  {"defaultboldfg", cmdDefaultBoldFG},
  {"defaultunderlinefg", cmdDefaultUnderlineFG},
  */
  {NULL, NULL}
};


static const char *findCommandCompletion (const char *str, int slen, const char *prev) {
  const char *res = NULL;
  int phit = 0;
  //
  if (slen < 1) return NULL;
  for (int f = 0; commandList[f].name != NULL; ++f) {
    if ((int)strlen(commandList[f].name) >= slen && strncasecmp(commandList[f].name, str, slen) == 0) {
      if (prev == NULL || phit) return commandList[f].name;
      if (strcasecmp(commandList[f].name, prev) == 0) phit = 1;
      if (res == NULL) res = commandList[f].name;
    }
  }
  return res;
}


// !0: NewTab command
static int executeCommand (const char *str, int slen) {
  const char *e;
  char *cmdname;
  int cmdfound = 0;
  //
  if (str == NULL) return 0;
  if (slen < 0) slen = strlen(str);
  //
  for (int f = 0; f < slen; ++f) if (!str[f]) { slen = f; break; }
  //
  while (slen > 0 && isspace(*str)) { ++str; --slen; }
  if (slen < 1 || !str[0]) return 0;
  //
  for (e = str; slen > 0 && !isspace(*e); ++e, --slen) ;
  //
  if (e-str > 127) return 0;
  cmdname = alloca(e-str+8);
  if (cmdname == NULL) return 0;
  memcpy(cmdname, str, e-str);
  cmdname[e-str] = 0;
  if (opt_disabletabs && strcasecmp(cmdname, "NewTab") == 0) return 1;
  //
  while (slen > 0 && isspace(*e)) { ++e; --slen; }
  //FIXME: ugly copypaste!
  //
  for (int f = 0; commandList[f].name != NULL; ++f) {
    if (strcasecmp(commandList[f].name, cmdname) == 0 && commandList[f].fn != NULL) {
      char *left = calloc(slen+2, 1);
      //
      if (left != NULL) {
        if (slen > 0) memcpy(left, e, slen);
        //fprintf(stderr, "command: [%s]; args: [%s]\n", cmdname, left);
        commandList[f].fn(cmdname, left);
        free(left);
      }
      cmdfound = 1;
      break;
    }
  }
  //
  if (!cmdfound) {
    char *left = calloc(slen+2, 1);
    //
    if (left != NULL) {
      if (slen > 0) memcpy(left, e, slen);
      processMiscCmds(cmdname, left);
      free(left);
    }
  }
  //
  return 0;
}


/*
static const char *cmdpSkipStr (const char *str) {
  while (*str && isspace(*str)) ++str;
  if (str[0] && str[0] != ';') {
    char qch = ' ';
    //
    while (*str) {
      if (*str == ';' && qch == ' ') break;
      if (qch != ' ' && *str == qch) { qch = ' '; ++str; continue; }
      if (*str == '"' || *str == '\'') {
        if (qch == ' ') qch = *str;
        ++str;
        continue;
      }
      if (*str++ == '\\' && *str) ++str;
    }
  }
  return str;
}
*/


static void executeCommands (const char *str) {
  oldTerm = curterm;
  oldTermIdx = termidx;
  newTerm = NULL;
  newTermSwitch = 0;
  if (str == NULL) return;
  while (*str) {
    const char *ce;
    char qch;
    //
    while (*str && isspace(*str)) ++str;
    if (!*str) break;
    if (*str == ';') { ++str; continue; }
    //
    ce = str;
    qch = ' ';
    while (*ce) {
      if (*ce == ';' && qch == ' ') break;
      if (qch != ' ' && *ce == qch) { qch = ' '; ++ce; continue; }
      if (*ce == '"' || *ce == '\'') {
        if (qch == ' ') qch = *ce;
        ++ce;
        continue;
      }
      if (*ce++ == '\\' && *ce) ++ce;
    }
    //
    if (executeCommand(str, ce-str)) break;
    if (*ce) str = ce+1; else break;
  }
  flushNewTerm();
  switchToTerm(oldTermIdx, 1);
}
