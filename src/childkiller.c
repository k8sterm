#include <sys/signalfd.h>


static void processDeadChildren (int sigfd) {
  struct signalfd_siginfo si;
  while (read(sigfd, &si, sizeof(si)) > 0) {} // ignore errors here
  for (;;) {
    int status;
    pid_t pid = waitpid(-1, &status, WNOHANG);
    if (pid <= 0) break; // no more dead children
    // find terminal with this pid
    for (int f = 0; f < term_count; ++f) {
      if (K8T_DATA(term_array[f])->pid == pid) {
        // this terminal should die
        K8Term *tt = term_array[f];
        if (xw.paste_term == tt) xw.paste_term = NULL;
        K8T_DATA(tt)->pid = 0;
        if (WIFEXITED(status)) {
          //fprintf(stderr, "found dead child: %d (term #%d of %d)\n", pid, f, term_count);
          tt->dead = 1;
          K8T_DATA(tt)->exitcode = WEXITSTATUS(status);
        } else {
          //fprintf(stderr, "found murdered child: %d (term #%d of %d)\n", pid, f, term_count);
          tt->dead = 0; // we have no child, but we are not dead yet
          K8T_DATA(tt)->exitcode = 127;
          K8T_DATA(tt)->waitkeypress = 1;
          if (K8T_DATA(tt)->exitmsg != NULL) free(K8T_DATA(tt)->exitmsg);
          if (WIFSIGNALED(status)) {
            K8T_DATA(tt)->exitmsg = SPrintf("Process %u died due to signal %u!", (unsigned)pid, (unsigned)(WTERMSIG(status)));
          } else {
            K8T_DATA(tt)->exitmsg = SPrintf("Process %u died due to unknown reason!", (unsigned)pid);
          }
        }
        updateTabBar = 1;
        break;
      }
    }
  }
}


static int summonChildSaviour (void) {
  sigset_t mask;
  sigemptyset(&mask);
  sigaddset(&mask, SIGCHLD);
  sigprocmask(SIG_BLOCK, &mask, NULL); // we block the signal
  //pthread_sigmask(SIG_BLOCK, &mask, NULL); // we block the signal
  return signalfd(-1, &mask, SFD_NONBLOCK|SFD_CLOEXEC);
}
