// return char length
static inline int xStringCharSize (const char *str, int bytelen) {
  return (bytelen >= K8T_UTF_SIZ || k8t_UTF8IsFull(str, bytelen) ? k8t_UTF8Size(str) : 0);
}


static int xStringWidth (int fontset, const char *str, int bytelen) {
  //XFontSet xfontset = dc.font[fontset].set;
  //XRectangle r;
  //memset(&r, 0, sizeof(r));
  //if (bytelen < 0) bytelen = strlen(str);
  //Xutf8TextExtents(xfontset, str, bytelen, &r, NULL);
  //return r.width-r.x;
  return xUtf8StringWidth(fontset, str, bytelen);
}


static void xDrawStringPart (int x, int y, int fontset, const char *str, int bytelen) {
  //XFontSet xfontset = dc.font[fontset].set;
  y += dc.font[fontset].ascent;
  //Xutf8DrawImageString(xw.dpy, xw.pictab, xfontset, dc.gc, x, y, str, bytelen);
  xDrawUtf8ImageString(x, y, fontset, (Drawable)xw.pictab, str, bytelen);
}


/*
static const char *utf8prev (const void *str) {
  const uint8_t *s = (const uint8_t *)str;
  if (s == NULL || !s[0]) return (const char *)s;
  if (s[0] < 128) return (const char *)s-1;
  for (--s; (s[0]&0xc0) == 0x80; --s) ;
  return (const char *)s;
}
*/


static void xDrawStringEllCenter (int x, int y, int width, int fontset, const char *str) {
  if (width > 0 && str != NULL && str[0]) {
    static int *charp = NULL; /* offset of each char in str */
    static int charp_size = 0;
    int charlen = 0;
    int bytelen = strlen(str);
    int left = bytelen;
    if (bytelen+1 > charp_size) {
      int *ncp = realloc(charp, sizeof(charp[0])*(bytelen+1));
      if (ncp == NULL) { fprintf(stderr, "FATAL: out of memory!\n"); return; }
      charp = ncp;
      charp_size = bytelen+1;
    }
    // calculate positions for all chars
    for (const char *s = str; left > 0 && *s; ) {
      int clen = xStringCharSize(s, left);
      if (clen == 0) break;
      charp[charlen] = (int)(s-str);
      ++charlen;
      s += clen;
      left -= clen;
    }
    // last char pos
    bytelen -= left;
    charp[charlen] = bytelen;
    if (charlen == 0) return; // nothing to do here
    if (xStringWidth(fontset, str, bytelen) <= width) {
      // it's ok
      xDrawStringPart(x, y, fontset, str, bytelen);
    } else {
      // need to write it with ellipsis
      const char *estr = "...";
      const int estrlen = (int)strlen(estr);
      const int ellen = xStringWidth(fontset, estr, estrlen);
      const int wmid = width/2-ellen/2;
      int left_cnt, right_start, blen;
      // calculate left_cnt
      left_cnt = 0;
      while (left_cnt < charlen) {
        int ww = xStringWidth(fontset, str, charp[left_cnt+1]);
        ++left_cnt;
        if (ww > wmid) break;
      }
      // draw left part
      if (left_cnt > 0) xDrawStringPart(x, y, fontset, str, charp[left_cnt]);
      // calculate right_start
      right_start = charlen-1;
      blen = charp[right_start+1]-charp[right_start];
      while (right_start > 0) {
        int clen = charp[right_start]-charp[right_start-1];
        int ww = xStringWidth(fontset, str+charp[right_start-1], blen+clen);
        --right_start;
        blen += clen;
        if (ww > wmid) break;
      }
      // draw right part
      if (right_start > 0) {
        int ww = xStringWidth(fontset, str+charp[right_start], blen);
        xDrawStringPart(x+width-ww, y, fontset, str+charp[right_start], blen);
      }
      // draw ellipsis
      xDrawStringPart(x+(width-ellen)/2, y, fontset, estr, estrlen);
    }
  }
}


static void xDrawStringEllRight (int x, int y, int width, int fontset, const char *str) {
  if (width > 0) {
    int bytelen = strlen(str);
    int wdt = xStringWidth(fontset, str, bytelen);
    //
    if (wdt <= width) {
      xDrawStringPart(x, y, fontset, str, bytelen);
    } else {
      const char *s = str;
      int bl = bytelen;
      int ellen = xStringWidth(fontset, "...", 3);
      //
      while (bl > 0) {
        int l = xStringCharSize(s, bl);
        int cw;
        //
        if (l < 1) break;
        cw = xStringWidth(fontset, str, s+l-str);
        if (cw > width) break;
        s += l;
        bl -= l;
      }
      xDrawStringPart(x, y, fontset, str, s-str);
      xDrawStringPart(x+width-ellen, y, fontset, "...", 3);
    }
  }
}


static void xDrawStringNoEll (int x, int y, int width, int fontset, const char *str) {
  if (width > 0) {
    xDrawStringPart(x, y, fontset, str, strlen(str));
  }
}


static void xdrawTabBar (void) {
  if (xw.tabheight > 0 && updateTabBar) {
    xclearunused();
    if (updateTabBar > 0) {
      int tabw = xw.w/opt_tabcount, n = (opt_tabposition == 0 ? 1 : 0);
      //XFontSet fontset = dc.font[2].set;
      XSetForeground(xw.dpy, dc.gc, getColor(normalTabBG));
      XFillRectangle(xw.dpy, xw.pictab, dc.gc, 0, 0, xw.w, xw.tabheight);
      for (int f = firstVisibleTab; f < firstVisibleTab+opt_tabcount; ++f) {
        int x = (f-firstVisibleTab)*tabw;
        const char *title;
        char *tit;
        if (f >= term_count) {
          XSetForeground(xw.dpy, dc.gc, getColor(normalTabBG));
          XFillRectangle(xw.dpy, xw.pictab, dc.gc, x, 0, xw.w, xw.tabheight);
          XSetForeground(xw.dpy, dc.gc, getColor(normalTabFG));
          XDrawLine(xw.dpy, xw.pictab, dc.gc, x/*+tabw-1*/, 0, x/*+tabw-1*/, xw.tabheight);
          break;
        }
        title = term_array[f]->title;
        if (!title[0]) title = opt_title;
        tit = SPrintf("[%d]%s", f, title);
        //title = tit;
        XSetForeground(xw.dpy, dc.gc, getColor(f == termidx ? activeTabBG : normalTabBG));
        XFillRectangle(xw.dpy, xw.pictab, dc.gc, x, 0, tabw, xw.tabheight);
        XSetBackground(xw.dpy, dc.gc, getColor(f == termidx ? activeTabBG : normalTabBG));
        XSetForeground(xw.dpy, dc.gc, getColor(f == termidx ? activeTabFG : normalTabFG));
        switch (opt_tabellipsis) {
          case 1: // center
            xDrawStringEllCenter(x+2, n, tabw-6, 2, tit);
            break;
          case 2: // left
            //xDrawStringEllLeft(x+2, n, tabw-6, 2, tit);
            break;
          case 3: // right
            xDrawStringEllRight(x+2, n, tabw-6, 2, tit);
            break;
          default: // none
            xDrawStringNoEll(x+2, n, tabw-6, 2, tit);
            break;
        }
        free(tit);
        XSetForeground(xw.dpy, dc.gc, getColor(f == termidx ? activeTabBG : normalTabBG));
        XFillRectangle(xw.dpy, xw.pictab, dc.gc, x+tabw-2, 0, 2, xw.tabheight);
        if (f > firstVisibleTab) {
          XSetForeground(xw.dpy, dc.gc, getColor(normalTabFG));
          XDrawLine(xw.dpy, xw.pictab, dc.gc, x/*+tabw-1*/, 0, x/*+tabw-1*/, xw.tabheight);
        }
      }
      XSetForeground(xw.dpy, dc.gc, getColor(normalTabFG));
      //XDrawRectangle(xw.dpy, xw.pictab, dc.gc, -1, 0, xw.w+1, xw.tabheight+1);
      if (opt_tabposition == 0) {
        XDrawLine(xw.dpy, xw.pictab, dc.gc, 0, 0, xw.w, 0);
      } else {
        XDrawLine(xw.dpy, xw.pictab, dc.gc, 0, xw.tabheight-1, xw.w, xw.tabheight-1);
      }
    }
    if (opt_tabposition == 0) {
      XCopyArea(xw.dpy, xw.pictab, xw.win, dc.gc, 0, 0, xw.w, xw.tabheight, 0, xw.h-xw.tabheight);
    } else {
      XCopyArea(xw.dpy, xw.pictab, xw.win, dc.gc, 0, 0, xw.w, xw.tabheight, 0, 0);
    }
  }
  updateTabBar = 0;
}
