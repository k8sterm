static K8TTimeMSec clockTicksCB (K8Term *term) {
  (void)term;
  return mclock_ticks();
}


static int isFocusedCB (K8Term *term) {
  (void)term;
  return (xw.state&K8T_WIN_FOCUSED ? 1 : 0);
}


static int isVisibleCB (K8Term *term) {
  (void)term;
  return (xw.state&K8T_WIN_VISIBLE ? 1 : 0);
}


static void drawSetFGCB (K8Term *term, int clr) {
  if (term == curterm) {
    XSetForeground(xw.dpy, dc.gc, getColor(clr));
  }
}


static void drawSetBGCB (K8Term *term, int clr) {
  if (term == curterm) {
    XSetBackground(xw.dpy, dc.gc, getColor(clr));
  }
}


static void drawRectCB (K8Term *term, int x0, int y0, int cols, int rows) {
  if (cols > 0 && rows > 0 && term == curterm) {
    XDrawRectangle(xw.dpy, xw.picscr, dc.gc, x0*xw.cw, y0*xw.ch, cols*xw.cw-1, rows*xw.ch-1);
  }
}


static void drawFillRectCB (K8Term *term, int x0, int y0, int cols, int rows) {
  if (cols > 0 && rows > 0 && term == curterm) {
    XFillRectangle(xw.dpy, xw.picscr, dc.gc, x0*xw.cw, y0*xw.ch, cols*xw.cw-1, rows*xw.ch-1);
  }
}


// copy pixmap to screen
static void drawCopyAreaCB (K8Term *term, int x0, int y0, int cols, int rows) {
  if (cols > 0 && rows > 0 && term == curterm) {
    int src_x = x0*xw.cw, src_y = y0*xw.ch, src_w = cols*xw.cw, src_h = rows*xw.ch;
    int dst_x = src_x, dst_y = src_y;
    //
    if (opt_tabposition == 1) { dst_y += xw.tabheight; }
    XCopyArea(xw.dpy, xw.picscr, xw.win, dc.gc, src_x, src_y, src_w, src_h, dst_x, dst_y);
  }
}


// brian: Back to old mthod
// cols can be used for underlining
static void drawStringCB (K8Term *term, int x, int y, int cols, const K8TGlyph *base, int fontset, const char *s, int bytelen) {
  if (term == curterm) {
    int winx = x*xw.cw, winy = y*xw.ch+dc.font[fontset].ascent;
  #ifdef X11_USE_FUCKED_FONTSETS
    XFontSet xfontset = dc.font[fontset].set;
    #ifndef _NEW_DRAWLINE_
    if (K8T_ISGFX(base->attr)) {
      XmbDrawImageString(xw.dpy, xw.picscr, xfontset, dc.gc, winx, winy, s, bytelen);
    } else if (!needConversion) {
      XmbDrawImageString(xw.dpy, xw.picscr, xfontset, dc.gc, winx, winy, s, bytelen);
    } else {
      if (bytelen > 0) {
        //k8: dunno why, but Xutf8DrawImageString() ignores ascii (at least for terminus)
        const char *pos = s;
        int xpos = winx;
        //
        while (pos < s+bytelen) {
          const char *e;
          int clen;
          //
          if ((unsigned char)(pos[0]) < 128) {
            for (e = pos+1; e < s+bytelen && (unsigned char)(*e) < 128; ++e) ;
            clen = e-pos;
            XmbDrawImageString(xw.dpy, xw.picscr, xfontset, dc.gc, xpos, winy, pos, e-pos);
          } else {
            for (clen = 0, e = pos; e < s+bytelen && (unsigned char)(*e) >= 128; ++e) {
              if (((unsigned char)(e[0])&0xc0) == 0xc0) ++clen;
            }
            Xutf8DrawImageString(xw.dpy, xw.picscr, xfontset, dc.gc, xpos, winy, pos, e-pos);
          }
          xpos += xw.cw*clen;
          pos = e;
        }
      }
    }
    #else
    Xutf8DrawImageString(xw.dpy, xw.picscr, xfontset, dc.gc, winx, winy, s, bytelen);
    #endif
  #else
    xDrawUtf8ImageString(winx, winy, fontset, (Drawable)xw.picscr, s, bytelen);
  #endif
    if (opt_drawunderline && (base->attr&K8T_ATTR_UNDERLINE)) {
      XDrawLine(xw.dpy, xw.picscr, dc.gc, winx, winy+1, winx+(cols*xw.cw)-1, winy+1);
    }
  }
}

static int drawOverlayCB (K8Term *term, int x0, int x1, int scry, int lineno, int dontcopy) {
  (void)x0; (void)x1; (void)lineno; (void)dontcopy;
  if (term == curterm) {
    if (scry == term->row-1 && K8T_DATA(term)->cmdline.cmdMode != K8T_CMDMODE_NONE) {
      xdrawcmdline(term, &K8T_DATA(term)->cmdline, scry);
      return 1;
    }
  }
  return 0;
}


static void fixSelectionCB (K8Term *term) {
  if (lastSelStr != NULL) free(lastSelStr);
  lastSelStr = (term->sel.clip != NULL ? strdup(term->sel.clip) : NULL);
  //
  if (term == curterm) {
    xfixsel();
  }
}


static void clearSelectionCB (K8Term *term) {
  (void)term;
  if (lastSelStr != NULL) free(lastSelStr);
  lastSelStr = NULL;
}


static void titleChangedCB (K8Term *term) {
  K8T_DATA(term)->titleset = 1;
  checkAndFixWindowTitle(term, 1);
}

#ifdef USE_CRAPBERRA
#include <dlfcn.h>
#ifndef __MACOSX__
static void *ca_hnd = NULL;
static void *ca_ctx = NULL;

typedef int (*ca_context_create_hnd) (void**);
typedef int (*ca_context_play_hnd) (void*, unsigned int, ...);
typedef int (*ca_context_destroy_hnd) (void*);

static ca_context_create_hnd ca_context_create = NULL;
static ca_context_play_hnd ca_context_play = NULL;
static ca_context_destroy_hnd ca_context_destroy = NULL;

static void *ca_sym (void *hnd, const char *sym) {
  void *hsym = dlsym(hnd, sym);
  if (dlerror()) return NULL;
  return hsym;
}
#endif
#endif

static void ca_init (void) {
#ifndef __MACOSX__
# ifdef USE_CRAPBERRA
    void *hnd = NULL,
         *ctx = NULL;
    if (!(hnd = dlopen("libcanberra.so", RTLD_LAZY)) &&
        !(hnd = dlopen("libcanberra.so.0", RTLD_LAZY)) &&
        !(hnd = dlopen("libcanberra.so.0.2.5", RTLD_LAZY)))
        return;
    if (!(ca_context_create = ca_sym(hnd, "ca_context_create")) ||
        !(ca_context_play = ca_sym(hnd, "ca_context_play")) ||
        !(ca_context_destroy = ca_sym(hnd, "ca_context_destroy"))) {
        dlclose(hnd);
        return;
    }
    if (0 != ca_context_create(&ctx)) {
        ca_context_destroy(ctx);
        dlclose(hnd);
        return;
    }
    ca_hnd = hnd;
    ca_ctx = ctx;
# endif
#endif
}

static void ca_done (void) {
#ifndef __MACOSX__
# ifdef USE_CRAPBERRA
  if (ca_ctx) ca_context_destroy(ca_ctx);
  if (ca_hnd) dlclose(ca_hnd);
# endif
#endif
}

static void bell (void) {
#ifdef USE_CRAPBERRA
  if (ca_context_play)
  #ifdef __MACOSX__
    cocoa_system_beep();
  #else
    ca_context_play(ca_ctx, 0, "event.id", "bell", "event.description", "sterm bell", NULL);
  #endif
  else
#endif
  XBell(xw.dpy, 100);
}

static void doBellCB (K8Term *term) {
  if (!(xw.state&K8T_WIN_FOCUSED) && (term->belltype&K8T_BELL_URGENT)) xseturgency(1);
  if (term->belltype&K8T_BELL_AUDIO) bell(); //XBell(xw.dpy, 100);
}


//  0: no
//  1: fully
// -1: partially
static int isUnderOverlayCB (K8Term *term, int x, int y, int w) {
  if (w > 0 && K8T_DATA(term)->cmdline.cmdMode != K8T_CMDMODE_NONE) {
    int xe = x+w-1;
    //
    if (xe >= 0 && x < term->col && y < term->row) {
      if (x < 0) x = 0;
      if (y < 0) y = 0;
      if (xe >= term->col) xe = term->col-1;
      //
      if (xe >= x) {
        if (y == term->row-1) return 1; // cmdline is active, and we are inside
      }
    }
  }
  //
  return 0;
}


static void clipToOverlayCB (K8Term *term, int *x0, int y, int *w) {
  int ovr = term->isUnderOverlay(term, *x0, y, *w);
  //
  if (ovr < 0) {
    // partially inside
    // easy deal for now: do nothing
  } else if (ovr == 0) {
    // outside
    *w = 0;
  } // else it's fully inside, do nothing
}
