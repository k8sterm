#define FONT      "-*-fixed-medium-r-normal-*-18-*-*-*-*-*-iso10646-1"
#define FONTBOLD  "-*-fixed-bold-r-normal-*-18-*-*-*-*-*-iso10646-1"
#define FONTTAB   "-*-fixed-medium-r-normal-*-14-*-*-*-*-*-iso10646-1"


/* Default shell to use if SHELL is not set in the env */
#define SHELL  "/bin/sh"


/* Terminal colors (16 first used in escape sequence) */
static const char *defcolornames[] = {
#if 1
  /* 8 normal colors */
  "black",
  "red3",
  "green3",
  "yellow3",
  "blue2",
  "magenta3",
  "cyan3",
  "gray90",
  /* 8 bright colors */
  "gray50",
  "red",
  "green",
  "yellow",
  "#5c5cff",
  "magenta",
  "cyan",
  "white",
#else
  /* 8 normal colors */
  "#000000",
  "#b21818",
  "#18b218",
  "#b26818",
  "#1818b2",
  "#b218b2",
  "#18b2b2",
  "#b2b2b2",
  /* 8 bright colors */
  "#686868",
  "#ff5454",
  "#54ff54",
  "#ffff54",
  "#5454ff",
  "#ff54ff",
  "#54ffff",
  "#ffffff",
#endif
};


/* more colors can be added after 255 to use with DefaultXX */
static const char *defextcolornames[] = {
  "#cccccc", /* 256 */
  "#333333", /* 257 */
  /* root terminal fg and bg */
  "#809a70", /* 258 */
  "#002000", /* 259 */
  /* bold and underline */
  "#00afaf", /* 260 */
  "#00af00", /* 261 */
};


/* Default colors (colorname index) foreground, background, cursor, unfocused cursor */
#define DEFAULT_FG   (7)
#define DEFAULT_BG   (0)
#define DEFAULT_CS   (256)
#define DEFAULT_UCS  (257)

#define TNAME  "xterm"

/* double-click timeout (in milliseconds) between clicks for selection */
#define DOUBLECLICK_TIMEOUT  (300)
#define TRIPLECLICK_TIMEOUT  (2*DOUBLECLICK_TIMEOUT)
//#define SELECT_TIMEOUT  (20) /* 20 ms */
#define DRAW_TIMEOUT    (25) /* msecs; ~40 FPS */

#define TAB  (8)


////////////////////////////////////////////////////////////////////////////////
#define MAX_COLOR  (511)

/* XEMBED messages */
#define XEMBED_FOCUS_IN   (4)
#define XEMBED_FOCUS_OUT  (5)


#define CMDLINE_SIZE   (256)
