////////////////////////////////////////////////////////////////////////////////
typedef struct {
  KeySym src;
  KeySym dst;
} KeyTransDef;


static KeyTransDef *keytrans = NULL;
static int keytrans_size = 0;
static int keytrans_used = 0;


typedef struct {
  KeySym key;
  uint32_t mask;
  int kp;
  char *str;
} KeyInfoDef;


static KeyInfoDef *keybinds = NULL;
static int keybinds_size = 0;
static int keybinds_used = 0;

static KeyInfoDef *keymap = NULL;
static int keymap_size = 0;
static int keymap_used = 0;


////////////////////////////////////////////////////////////////////////////////
static void keytrans_reset (void) {
  if (keytrans) free(keytrans);
  keytrans = NULL;
  keytrans_size = 0;
  keytrans_used = 0;
}


static void keytrans_add (const char *src, const char *dst) {
  KeySym kssrc = XStringToKeysym(src);
  KeySym ksdst = XStringToKeysym(dst);
  //
  if (kssrc == NoSymbol) k8t_die("invalid keysym: '%s'", src);
  if (ksdst == NoSymbol) k8t_die("invalid keysym: '%s'", dst);
  if (kssrc == ksdst) return; // idiot
  //
  for (int f = 0; f < keytrans_used; ++f) {
    if (keytrans[f].src == kssrc) {
      // replace
      keytrans[f].dst = ksdst;
      return;
    }
  }
  //
  if (keytrans_used >= keytrans_size) {
    int newsize = keytrans_size+64;
    KeyTransDef *n = realloc(keytrans, sizeof(KeyTransDef)*newsize);
    //
    if (n == NULL) k8t_die("out of memory");
    keytrans_size = newsize;
    keytrans = n;
  }
  keytrans[keytrans_used].src = kssrc;
  keytrans[keytrans_used].dst = ksdst;
  ++keytrans_used;
}


////////////////////////////////////////////////////////////////////////////////
static void parsekeyname (const char *str, KeySym *ks, uint32_t *mask, int *kp) {
  char *s = alloca(strlen(str)+1);
  //
  if (s == NULL) k8t_die("out of memory");
  strcpy(s, str);
  *kp = 0;
  *ks = NoSymbol;
  *mask = XK_NO_MOD;
  //
  while (*s) {
    char *e, oc;
    uint32_t mm = 0;
    int mod = 1;
    //
    while (*s && isspace(*s)) ++s;
    for (e = s; *e && !isspace(*e) && *e != '+'; ++e) ;
    oc = *e; *e = 0;
    //
         if (strcasecmp(s, "alt") == 0) mm = Mod1Mask;
    else if (strcasecmp(s, "win") == 0) mm = Mod4Mask;
    else if (strcasecmp(s, "ctrl") == 0) mm = ControlMask;
    else if (strcasecmp(s, "shift") == 0) mm = ShiftMask;
    else if (strcasecmp(s, "any") == 0) mm = XK_NO_MOD; //!
    else if (strcasecmp(s, "kpad") == 0) *kp = 1;
    else {
      mod = 0;
      if ((*ks = XStringToKeysym(s)) == NoSymbol) break;
      //fprintf(stderr, "[%s] %d %d %d\n", s, (*mask)&ControlMask, (*mask)&Mod1Mask, (*mask)&ShiftMask);
    }
    //
    *e = oc;
    s = e;
    while (*s && isspace(*s)) ++s;
    if (mod) {
      if (*s != '+') { *ks = NoSymbol; break; }
      ++s;
      if (mm != 0) {
        if (mm == XK_NO_MOD) *mask = XK_ANY_MOD;
        else if (*mask == XK_NO_MOD) *mask = mm;
        else if (*mask != XK_ANY_MOD) *mask |= mm;
      }
    } else {
      if (*s) { *ks = NoSymbol; break; }
    }
  }
  if (*ks == NoSymbol) k8t_die("invalid key name: '%s'", str);
  //fprintf(stderr, "mask=0x%08x, kp=%d\n", *mask, *kp);
}


////////////////////////////////////////////////////////////////////////////////
static void keybinds_reset (void) {
  if (keybinds) free(keybinds);
  keybinds = NULL;
  keybinds_size = 0;
  keybinds_used = 0;
}


static void keybind_add (const char *key, const char *act) {
  KeySym ks;
  uint32_t mask;
  int kp;
  //
  parsekeyname(key, &ks, &mask, &kp);
  //
  for (int f = 0; f < keybinds_used; ++f) {
    if (keybinds[f].key == ks && keybinds[f].mask == mask) {
      // replace or remove
      free(keybinds[f].str);
      if (act == NULL || !act[0]) {
        // remove
        for (int c = f+1; c < keybinds_used; ++c) keybinds[c-1] = keybinds[c];
      } else {
        // replace
        if ((keybinds[f].str = strdup(act)) == NULL) k8t_die("out of memory");
      }
      return;
    }
  }
  //
  if (keybinds_used >= keybinds_size) {
    int newsize = keybinds_size+64;
    KeyInfoDef *n = realloc(keybinds, sizeof(KeyInfoDef)*newsize);
    //
    if (n == NULL) k8t_die("out of memory");
    keybinds_size = newsize;
    keybinds = n;
  }
  keybinds[keybinds_used].key = ks;
  keybinds[keybinds_used].mask = mask;
  keybinds[keybinds_used].kp = 0;
  if ((keybinds[keybinds_used].str = strdup(act)) == NULL) k8t_die("out of memory");
  ++keybinds_used;
}


////////////////////////////////////////////////////////////////////////////////
static void keymap_reset (void) {
  if (keymap) free(keymap);
  keymap = NULL;
  keymap_size = 0;
  keymap_used = 0;
}


static void keymap_add (const char *key, const char *act) {
  KeySym ks;
  uint32_t mask;
  int kp;
  //
  parsekeyname(key, &ks, &mask, &kp);
  //
  for (int f = 0; f < keymap_used; ++f) {
    if (keymap[f].key == ks && keymap[f].mask == mask && keymap[f].kp == kp) {
      // replace or remove
      free(keymap[f].str);
      if (act == NULL) {
        // remove
        for (int c = f+1; c < keymap_used; ++c) keymap[c-1] = keymap[c];
      } else {
        // replace
        if ((keymap[f].str = strdup(act)) == NULL) k8t_die("out of memory");
      }
      return;
    }
  }
  //
  if (keymap_used >= keymap_size) {
    int newsize = keymap_size+128;
    KeyInfoDef *n = realloc(keymap, sizeof(KeyInfoDef)*newsize);
    //
    if (n == NULL) k8t_die("out of memory");
    keymap_size = newsize;
    keymap = n;
  }
  keymap[keymap_used].key = ks;
  keymap[keymap_used].mask = mask;
  keymap[keymap_used].kp = kp;
  if ((keymap[keymap_used].str = strdup(act)) == NULL) k8t_die("out of memory");
  ++keymap_used;
}
