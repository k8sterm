#define XUDISBUF_SIZE  (1024u)
static XChar2b xDrawUtf8ImageStringBuf[XUDISBUF_SIZE];

static void xDrawUtf8ImageString (int x, int y, int fontset, Drawable dwb, const char *str, int bytelen) {
  if (bytelen < 0) {
    if (!str) return;
    bytelen = (int)strlen(str);
  }
  if (!bytelen) return;

  //y += dc.font[fontset].ascent;

#ifdef X11_USE_FUCKED_FONTSETS
  XFontSet xfontset = dc.font[fontset].set;
  Xutf8DrawImageString(xw.dpy, xw.pictab, xfontset, dc.gc, x, y, str, bytelen);
#else
  uint32_t utfcp = 0;
  uint32_t bpos = 0;
  XChar2b *c2bptr = xDrawUtf8ImageStringBuf;
  while (bytelen--) {
    utfcp = sxed_utf8d_consume(utfcp, *str++);
    if (sxed_utf8_valid_cp(utfcp)) {
      if (!sxed_utf8_printable_cp(utfcp)) utfcp = SXED_UTF8_REPLACEMENT_CP;
      c2bptr->byte1 = (uint8_t)(utfcp>>8);
      c2bptr->byte2 = (uint8_t)utfcp;
      ++c2bptr;
      if (++bpos == XUDISBUF_SIZE) {
        c2bptr = xDrawUtf8ImageStringBuf;
        if (dc.gcfid != dc.font[fontset].fid) {
          XSetFont(xw.dpy, dc.gc, dc.font[fontset].fid);
          dc.gcfid = dc.font[fontset].fid;
        }
        XDrawImageString16(xw.dpy, dwb, dc.gc, x, y, c2bptr, bpos);
        x += (int)bpos*dc.font[fontset].width;
        bpos = 0;
      }
    }
  }
  if (bpos) {
    c2bptr = xDrawUtf8ImageStringBuf;
    if (dc.gcfid != dc.font[fontset].fid) {
      XSetFont(xw.dpy, dc.gc, dc.font[fontset].fid);
      dc.gcfid = dc.font[fontset].fid;
    }
    XDrawImageString16(xw.dpy, dwb, dc.gc, x, y, c2bptr, bpos);
  }
#endif
}


static int xUtf8StringWidth (int fontset, const char *str, int bytelen) {
  if (bytelen < 0) {
    if (!str) return 0;
    bytelen = (int)strlen(str);
  }
  if (!bytelen) return 0;

#ifdef X11_USE_FUCKED_FONTSETS
  XFontSet xfontset = dc.font[fontset].set;
  XRectangle r;
  memset(&r, 0, sizeof(r));
  Xutf8TextExtents(xfontset, str, bytelen, &r, NULL);
  return r.width-r.x;
#else
  int wdt = 0;
  uint32_t utfcp = 0;
  uint32_t bpos = 0;
  XChar2b *c2bptr = xDrawUtf8ImageStringBuf;
  while (bytelen--) {
    utfcp = sxed_utf8d_consume(utfcp, *str++);
    if (sxed_utf8_valid_cp(utfcp)) {
      if (!sxed_utf8_printable_cp(utfcp)) utfcp = SXED_UTF8_REPLACEMENT_CP;
      c2bptr->byte1 = (uint8_t)(utfcp>>8);
      c2bptr->byte2 = (uint8_t)utfcp;
      ++c2bptr;
      if (++bpos == XUDISBUF_SIZE) {
        c2bptr = xDrawUtf8ImageStringBuf;
        wdt += XTextWidth16(dc.font[fontset].set, c2bptr, bpos);
        bpos = 0;
      }
    }
  }
  if (bpos) {
    c2bptr = xDrawUtf8ImageStringBuf;
    wdt += XTextWidth16(dc.font[fontset].set, c2bptr, bpos);
  }
  return wdt;
#endif
}
