/* Globals */
static uint16_t *unimap = NULL; // 0 or 65535 items; 127: special; high bit set: use alt(gfx) mode; 0: empty

static int opt_cmd_count = 0;
static char **opt_cmd  = NULL;
static char *opt_title = NULL;
static char *opt_embed = NULL;
static char *opt_class = NULL;
static char *opt_term = NULL;
static char *opt_fontnorm = NULL;
static char *opt_fontbold = NULL;
static char *opt_fonttab = NULL;
static char *opt_shell = NULL;
static char *opt_colornames[512];
static int opt_term_locked = 0;
static int opt_doubleclick_timeout = DOUBLECLICK_TIMEOUT;
static int opt_tripleclick_timeout = TRIPLECLICK_TIMEOUT;
static int opt_tabsize = TAB;
static int defaultFG = DEFAULT_FG;
static int defaultBG = DEFAULT_BG;
static int defaultCursorFG = 0;
static int defaultCursorBG = DEFAULT_CS;
static int defaultCursorFG1 = 0;
static int defaultCursorBG1 = -1;
static int defaultCursorInactiveFG = 0;
static int defaultCursorInactiveBG = DEFAULT_UCS;
static int defaultBoldFG = -1;
static int defaultUnderlineFG = -1;
static int normalTabFG = 258;
static int normalTabBG = 257;
static int activeTabFG = 258;
static int activeTabBG = 0;
static int opt_maxhistory = 512;
static int opt_ptrblank = 2000; // delay; 0: never
static int opt_tabcount = 6;
static int opt_tabposition = 0; // 0: bottom; 1: top
static int opt_drawtimeout = DRAW_TIMEOUT;
static int opt_disabletabs = 0;
static int opt_audiblebell = 1;
static int opt_urgentbell = 1;
static int opt_cursorBlink = 0;
static int opt_cursorBlinkInactive = 0;
static int opt_drawunderline = 1;
static int opt_ignoreclose = 0;
static int opt_maxdrawtimeout = 1500;
static int opt_fastredraw = 0;
static int opt_tabellipsis = 1; // center
static int ptrBlanked = 0;
static K8TTimeMSec ptrLastMove = 0;
static int globalBW = 0;
static int opt_scrollclear = 1; // default mode: clear to 1st char color


static K8Term **term_array = NULL;
static int term_count = 0;
static int term_array_size = 0;
static K8Term *curterm; // current terminal
static int termidx; // current terminal index; DON'T RELAY ON IT!
static int updateTabBar;
static char *lastSelStr = NULL;
//static int lastSelLength = 0;

static int firstVisibleTab = 0;

static int exitcode = 0;
static int closeRequestComes = 0;

static K8TXDC dc;
static K8TXWindow xw;

static Atom XA_VT_SELECTION;
static Atom XA_CLIPBOARD;
static Atom XA_UTF8;
static Atom XA_TARGETS;
static Atom XA_INCR;
static Atom XA_CSTRING;
static Atom XA_NETWM_NAME;
static Atom XA_WM_DELETE_WINDOW;
