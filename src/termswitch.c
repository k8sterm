#define k8stx_output(buf_,buflen_)  do { \
  if (!xw.paste_term || xw.paste_term->dead) { \
    xw.paste_term = NULL; \
    break; \
  } \
  if ((buflen_) <= 0) break; \
  if (K8T_DATA(xw.paste_term)->cmdline.cmdMode != K8T_CMDMODE_NONE) { \
    if (K8T_DATA(xw.paste_term)->cmdline.cmdMode == K8T_CMDMODE_INPUT) { \
      tcmdput(xw.paste_term, &K8T_DATA(xw.paste_term)->cmdline, (buf_), (buflen_)); \
    } \
  } else { \
    if (!xw.past_wasbrcp && K8T_ISSET(xw.paste_term, K8T_MODE_BRACPASTE)) { \
      xw.past_wasbrcp = 1; \
      k8t_ttyWriteStrNoEnc(xw.paste_term, "\x1b[200~"); \
    } \
    k8t_ttyWrite(xw.paste_term, (buf_), (buflen_)); \
  } \
} while (0)


#define k8stx_finish_paste()  do { \
  if (xw.paste_term && xw.past_wasbrcp && !xw.paste_term->dead) { \
    k8t_ttyWriteStrNoEnc(xw.paste_term, "\x1b[201~"); \
  } \
  xw.paste_term = NULL; \
} while (0)


////////////////////////////////////////////////////////////////////////////////
static const char *get_process_name (int pfd) {
  static char path[256], res[4097], *c;
  int fd;
  pid_t pgrp;
  if ((pgrp = tcgetpgrp(pfd)) == -1) return NULL;
  snprintf(path, sizeof(path), "/proc/%d/cmdline", pgrp);
  if ((fd = open(path, O_RDONLY|O_CLOEXEC)) < 0) return NULL;
  res[sizeof(res)-1] = 0;
  read(fd, res, sizeof(res)-1);
  close(fd);
  if ((c = strrchr(res, '/')) == NULL) c = res; else ++c;
  return c;
}


static const char *get_process_cwd (int pfd) {
  static char path[256], res[4097];
  pid_t pgrp;
  ssize_t n;
  if ((pgrp = tcgetpgrp(pfd)) == -1) return NULL;
  snprintf(path, sizeof(path), "/proc/%d/cwd", pgrp);
  n = readlink(path, res, sizeof(res)-1);
  if (n > 0) {
    res[n] = '\0';
    return res;
  }
  return NULL;
}


static void fixWindowTitle (const K8Term *t) {
  if (t != NULL) {
    const char *title = t->title;
    //
    if (title == NULL || !title[0]) {
      title = opt_title;
      if (title == NULL) title = "";
    }
    XStoreName(xw.dpy, xw.win, title);
    XChangeProperty(xw.dpy, xw.win, XA_NETWM_NAME, XA_UTF8, 8, PropModeReplace, (const unsigned char *)title, strlen(title));
  }
}


static void checkAndFixWindowTitle (K8Term *t, int forceupd) {
  if (t != NULL && t->cmdfd >= 0) {
    const char *pname, *ppath;
    int doupd = 0;
    if ((pname = get_process_name(t->cmdfd)) && strcmp(K8T_DATA(t)->lastpname, pname) != 0) {
      // new process
      int is_mc;
      doupd = 1;
      //fprintf(stderr, "new process: [%s]\n", pname);
      K8T_DATA(t)->titleset = 0;
      if (K8T_DATA(t)->lastpname) free(K8T_DATA(t)->lastpname);
      K8T_DATA(t)->lastpname = strdup(pname);
      is_mc = (strcmp(pname, "mc") == 0 || strcmp(pname, "mcedit") == 0 || strcmp(pname, "mcview") == 0);
      if (K8T_DATA(t)->mc_hack_active && !is_mc) {
        K8T_DATA(t)->mc_hack_active = 0;
        t->clearOnPartialScrollMode = K8T_DATA(t)->old_scroll_mode;
      } else if (!K8T_DATA(t)->mc_hack_active && is_mc) {
        K8T_DATA(t)->mc_hack_active = 1;
        t->clearOnPartialScrollMode = 2;
      }
    }
    if ((ppath = get_process_cwd(t->cmdfd)) != NULL && strcmp(K8T_DATA(t)->lastppath, ppath) != 0) {
      // new path
      if (K8T_DATA(t)->lastppath != NULL) free(K8T_DATA(t)->lastppath);
      K8T_DATA(t)->lastppath = strdup(ppath);
      doupd = 1;
    }
    if (doupd && !K8T_DATA(t)->titleset) {
      snprintf(t->title, sizeof(t->title), "%s [%s]", K8T_DATA(t)->lastpname, K8T_DATA(t)->lastppath);
    } else {
      doupd = 0;
    }
    if (doupd || forceupd) {
      if (t == curterm) fixWindowTitle(t);
      updateTabBar = 1;
    }
  }
}


// find latest active terminal (but not current %-)
static int findTermToSwitch (void) {
  int idx = -1;
  K8TTimeMSec maxlat = 0;
  for (int f = 0; f < term_count; ++f) {
    if (curterm != term_array[f] && term_array[f]->lastActiveTime > maxlat) {
      maxlat = term_array[f]->lastActiveTime;
      idx = f;
    }
  }
  if (idx < 0) {
    if (termidx == 0) idx = 0; else idx = termidx+1;
    if (idx > term_count) idx = term_count-1;
  }
  return idx;
}


static void fixFirstTab (void) {
  if (termidx < firstVisibleTab) firstVisibleTab = termidx;
  else if (termidx > firstVisibleTab+opt_tabcount-1) firstVisibleTab = termidx-opt_tabcount+1;
  if (firstVisibleTab < 0) firstVisibleTab = 0;
  updateTabBar = 1;
}


static void switchToTerm (int idx, int redraw) {
  if (idx >= 0 && idx < term_count && term_array[idx] != NULL && term_array[idx] != curterm) {
    K8TTimeMSec tt = mclock_ticks();
    //
    if (curterm != NULL) curterm->lastActiveTime = tt;
    termidx = idx;
    curterm = term_array[termidx];
    curterm->curbhidden = 0;
    curterm->lastBlinkTime = tt;
    if (xw.paste_term && xw.paste_term != curterm) k8stx_finish_paste();
    //
    fixFirstTab();
    //
    xseturgency(0);
    k8t_tmFullDirty(curterm);
    fixWindowTitle(curterm);
    updateTabBar = 1;
    XSetWindowBackground(xw.dpy, xw.win, getColor(curterm->defbg));
    xclearunused();
    if (redraw) {
      k8t_tmFullDirty(curterm);
      k8t_drawTerm(curterm, 1);
      if (!updateTabBar) updateTabBar = 1;
      xdrawTabBar();
    }
    //FIXME: optimize memory allocations
    if (curterm->sel.clip != NULL && curterm->sel.bx >= 0) {
      if (lastSelStr != NULL) {
        if (strlen(lastSelStr) >= strlen(curterm->sel.clip)) {
          strcpy(lastSelStr, curterm->sel.clip);
        } else {
          free(lastSelStr);
          lastSelStr = NULL;
        }
      }
      if (lastSelStr == NULL) lastSelStr = strdup(curterm->sel.clip);
      //fprintf(stderr, "newsel: [%s]\n", lastSelStr);
    }
    xfixsel();
    //fprintf(stderr, "curterm #%d\n", termidx);
    //fprintf(stderr, "needConv: %d\n", curterm->needConv);
  }
}


static K8Term *k8t_termalloc (void) {
  K8Term *t;
  //
  if (term_count >= term_array_size) {
    int newsz = ((term_count==0) ? 1 : term_array_size+64);
    K8Term **n = realloc(term_array, sizeof(K8Term *)*newsz);
    //
    if (n == NULL && term_count == 0) k8t_die("out of memory!");
    term_array = n;
    term_array_size = newsz;
  }
  if ((t = calloc(1, sizeof(K8Term))) == NULL) return NULL;
  //
  t->wrbufsize = K8T_WBUFSIZ;
  t->deffg = defaultFG;
  t->defbg = defaultBG;
  t->dead = 1;
  t->needConv = (needConversion ? 1 : 0);
  t->belltype = (opt_audiblebell?K8T_BELL_AUDIO:0)|(opt_urgentbell?K8T_BELL_URGENT:0);
  t->curblink = opt_cursorBlink;
  t->curblinkinactive = opt_cursorBlinkInactive;
  t->fastredraw = opt_fastredraw;
  t->clearOnPartialScrollMode = opt_scrollclear;
  //
  termSetCallbacks(t);
  termSetDefaults(t);
  //
  term_array[term_count++] = t;
  //
  return t;
}


// newer delete last terminal!
static void termfree (int idx) {
  if (idx >= 0 && idx < term_count && term_array[idx] != NULL) {
    K8Term *t = term_array[idx];
    K8TermData *td = K8T_DATA(t);
    // abort pasting, if there was any
    if (xw.paste_term == t) xw.paste_term = NULL;
    //
    if (K8T_DATA(t)->pid != 0) {
      kill(K8T_DATA(t)->pid, SIGKILL);
      return;
    }
    //
    if (t->cmdfd >= 0) {
      close(t->cmdfd);
      t->cmdfd = -1;
    }
    //
    exitcode = K8T_DATA(t)->exitcode;
    //
    if (K8T_DATA(t)->waitkeypress) return;
    //
    if (idx == termidx) {
      if (term_count > 1) {
        t->dead = 1;
        //switchToTerm((idx > 0) ? idx-1 : 1, 0);
        switchToTerm(findTermToSwitch(), 0);
        if (curterm == t) {
          fprintf(stderr, "FATAL: cannot find good terminal to switch onto.\n");
          curterm = NULL;
        }
      } else {
        curterm = NULL;
      }
    }
    //
    for (int y = 0; y < t->row; ++y) free(t->alt[y]);
    for (int y = 0; y < t->linecount; ++y) free(t->line[y]);
    free(t->dirty);
    free(t->alt);
    free(t->line);
    //
    if (td->lastpname != NULL) free(td->lastpname);
    if (td->lastppath != NULL) free(td->lastppath);
    if (td->execcmd != NULL) free(td->execcmd);
    // condense array
    if (termidx > idx) --termidx; // it's not current, and current is at the right
    for (int f = idx+1; f < term_count; ++f) term_array[f-1] = term_array[f];
    --term_count;
    //
    //if (td->picalloced) XFreePixmap(xw.dpy, td->picbuf);
    free(td);
    free(t);
  }
}


static void termcleanup (void) {
  int f = 0, needredraw = 0;

  while (f < term_count) {
    if (term_array[f]->dead) {
      //fprintf(stderr, "found dead term #%d of %d\n", f, term_count);
      termfree(f);
      //fprintf(stderr, "terms left: %d (curterm=%p)\n", term_count, curterm);
      needredraw = 1;
    } else {
      ++f;
    }
  }

  if (xw.paste_term) {
    K8TTimeMSec stt = mclock_ticks();
    if (stt < xw.paste_last_activity || stt-xw.paste_last_activity >= 13000) {
      //fprintf(stderr, "SELECTION PASTE ABORTED DUE TO TIMEOUT.\n");
      k8stx_finish_paste();
    }
  }

  if (needredraw) {
    updateTabBar = 1;
    k8t_drawTerm(curterm, 1);
  }
}
