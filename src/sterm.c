/* See LICENSE for licence details. */
#ifndef GIT_VERSION
# define VERSION  "0.4.1"
#else
# define VERSION  GIT_VERSION
#endif

#ifdef _XOPEN_SOURCE
# undef _XOPEN_SOURCE
#endif
#define _XOPEN_SOURCE 600

#ifndef _GNU_SOURCE
# define _GNU_SOURCE
#endif

#include <alloca.h>
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <iconv.h>
#include <limits.h>
#include <locale.h>

#ifdef __MACH__
#include <util.h>
#else
#include <pty.h>
#endif

#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <signal.h>
#include <sys/ioctl.h>
#include <sys/select.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>
#include <X11/Xatom.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/cursorfont.h>
#include <X11/keysym.h>

////////////////////////////////////////////////////////////////////////////////
#include "libk8sterm/k8sterm.h"


////////////////////////////////////////////////////////////////////////////////
#include "defaults.c"


////////////////////////////////////////////////////////////////////////////////
#define USAGE_VERSION  "k8sterm " VERSION "\n(c) 2010-2012 st engineers and Ketmar // Invisible Vector\n"
#define USAGE \
  "usage: sterm [options]\n" \
  "options:\n" \
  "-v              show version and exit\n" \
  "-h              show this help and exit\n" \
  "-S              disable tabs\n" \
  "-W              terminal width\n" \
  "-H              terminal height\n" \
  "-t title        set window title\n" \
  "-c class        set window class\n" \
  "-w windowid     embed in window id given id\n" \
  "-T termname     set TERM name\n" \
  "-C config_file  use custom config file\n" \
  "-l langiconv    use given locale\n" \
  "-R stcmd        run this INTERNAL k8sterm command\n" \
  "-e command...   run given command and pass all following args to it\n"


////////////////////////////////////////////////////////////////////////////////
/* masks for key translation */
#define XK_NO_MOD   (UINT_MAX)
#define XK_ANY_MOD  (0)


////////////////////////////////////////////////////////////////////////////////
// internal command line mode
enum {
  K8T_CMDMODE_NONE,
  K8T_CMDMODE_INPUT,
  K8T_CMDMODE_MESSAGE
};


// X11 window state flags
enum {
  K8T_WIN_VISIBLE = 0x01,
  K8T_WIN_REDRAW  = 0x02,
  K8T_WIN_FOCUSED = 0x04
};


////////////////////////////////////////////////////////////////////////////////
/* Purely graphic info */
typedef struct {
  Display *dpy;
  Colormap cmap;
  Window win;
  Cursor cursor; // text cursor
  Cursor defcursor; // 'default' cursor
  Cursor lastcursor; // last used cursor before blanking
  Cursor blankPtr;
  Atom xembed;
  XIM xim;
  XIC xic;
  int scr;
  int w;      /* window width */
  int h;      /* window height */
  int bufw;   /* pixmap width */
  int bufh;   /* pixmap height */
  int ch;     /* char height */
  int cw;     /* char width */
  char state; /* focus, redraw, visible */
  //
  int tch;    /* tab text char height */
  Pixmap pictab;
  int picscrhere;
  Pixmap picscr;
  int tabheight;
  //struct timeval lastdraw;
  // selection processing
  // if not NULL, we are in selection pasting mode
  K8Term *paste_term;
  // just started, and waiting for TARGETS atom
  int paste_waiting_targets;
  // 0: waiting for the first packet; 1: INCR paste; -1: normal paste
  int paste_incr_mode;
  // if paste_incr_mode is not zero, utf8 paste flag
  int paste_utf8;
  // was "bracket paste" escape sent?
  int past_wasbrcp;
  // for utf decoder
  uint32_t paste_cp;
  // last selection activity time, for timeouts
  K8TTimeMSec paste_last_activity;
} K8TXWindow;


/* Drawing Context */
typedef struct {
  uint32_t *ncol; // normal colors
  uint32_t *bcol; // b/w colors
  uint32_t *gcol; // green colors
  uint32_t *clrs[3];
  GC gc;
  Font gcfid; // last selected font
  struct {
    int ascent;
    int descent;
    //short lbearing;
    //short rbearing;
    int width;
    #ifdef X11_USE_FUCKED_FONTSETS
    XFontSet set;
    #else
    XFontStruct *set;
    #endif
    Font fid;
  } font[3];
} K8TXDC;


typedef struct K8TCmdLine K8TCmdLine;

typedef void (*CmdLineExecFn) (K8Term *term, K8TCmdLine *cmdline, int cancelled);

struct K8TCmdLine {
  int cmdMode; // K8T_CMDMODE_xxx
  char cmdline[K8T_UTF_SIZ*CMDLINE_SIZE];
  int cmdreslen; // byte length of 'reserved' (read-only) part
  int cmdofs; // byte offset of the first visible UTF-8 char in cmdline
  char cmdc[K8T_UTF_SIZ+1]; // buffer to collect UTF-8 char
  int cmdcl; // index in cmdc, used to collect UTF-8 chars
  int cmdtabpos; // # of bytes (not UTF-8 chars!) used in autocompletion or -1
  const char *cmdcurtabc; // current autocompleted command
  CmdLineExecFn cmdexecfn;
  void *udata;
};


/* internal terminal data */
typedef struct {
  pid_t pid;
  int exitcode;
  //
  int waitkeypress; /* child is dead, awaiting for keypress */
  char *exitmsg; /* message for waitkeypress */
  //
  char *execcmd;
  //
  char *lastpname;
  char *lastppath;
  int titleset;
  int mc_hack_active;
  int old_scroll_mode;
  // for pasting
  uint32_t utfcp;
  //
  K8TCmdLine cmdline;
} K8TermData;

#define K8T_DATA(_term)  ((K8TermData *)(_term->udata))


////////////////////////////////////////////////////////////////////////////////
#include "globals.c"
#include "getticks.c"
#include "utf8dec.c"


////////////////////////////////////////////////////////////////////////////////
#include "utils.c"
#include "keymaps.c"


////////////////////////////////////////////////////////////////////////////////
static void executeCommands (const char *str);
static const char *findCommandCompletion (const char *str, int slen, const char *prev);

static void tdrawfatalmsg (K8Term *term, const char *msg);

static void tcmdput (K8Term *term, K8TCmdLine *cmdline, const char *s, int len);

static void xclearunused (void);
static void xseturgency (int add);
static void xfixsel (void);
static void xblankPointer (void);
static void xunblankPointer (void);
static void xdrawTabBar (void);
static void xdrawcmdline (K8Term *term, K8TCmdLine *cmdline, int scry);

static void termCreateXPixmap (K8Term *term);
static void termSetCallbacks (K8Term *term);
static void termSetDefaults (K8Term *term);

static void getButtonInfo (K8Term *term, XEvent *e, int *b, int *x, int *y);

static void fixWindowTitle (const K8Term *t);

static void scrollHistory (K8Term *term, int delta);

static int termsDoIO (int xfd);
//static void doTermWrFlush (K8Term *term);


////////////////////////////////////////////////////////////////////////////////
static inline uint32_t getColor (int idx) {
  if (globalBW && (curterm == NULL || !curterm->blackandwhite)) return dc.clrs[globalBW][idx];
  if (curterm != NULL) return dc.clrs[curterm->blackandwhite%3][idx];
  return dc.clrs[0][idx];
}


////////////////////////////////////////////////////////////////////////////////
#include "iniparse.c"
#include "locales.c"


////////////////////////////////////////////////////////////////////////////////
#include "termswitch.c"
#include "ttyinit.c"


////////////////////////////////////////////////////////////////////////////////
#include "tcmdline.c"
#include "tfatalbox.c"


////////////////////////////////////////////////////////////////////////////////
#include "x11init.c"
#include "x11misc.c"
#include "x11drawutfstr.c"
#include "x11drawtabs.c"
#include "x11drawcmdline.c"


////////////////////////////////////////////////////////////////////////////////
#include "x11evtvis.c"
#include "x11evtkbd.c"
#include "x11evtsel.c"
#include "x11evtmouse.c"


////////////////////////////////////////////////////////////////////////////////
#include "x11CB.c"


////////////////////////////////////////////////////////////////////////////////
// xembed?
static void xevtcbcmessage (XEvent *e) {
  /* See xembed specs http://standards.freedesktop.org/xembed-spec/xembed-spec-latest.html */
  if (e->xclient.message_type == xw.xembed && e->xclient.format == 32) {
    if (e->xclient.data.l[1] == XEMBED_FOCUS_IN) {
      xw.state |= K8T_WIN_FOCUSED;
      xseturgency(0);
      k8t_tmSendFocusEvent(curterm, 1);
    } else if (e->xclient.data.l[1] == XEMBED_FOCUS_OUT) {
      xw.state &= ~K8T_WIN_FOCUSED;
      k8t_tmSendFocusEvent(curterm, 0);
    }
    k8t_drawCursor(curterm, 0);
    xdrawTabBar();
    k8t_drawCopy(curterm, 0, 0, curterm->col, curterm->row);
    return;
  }
  //
  if (e->xclient.data.l[0] == (long)XA_WM_DELETE_WINDOW) {
    closeRequestComes = 1;
    return;
  }
}


////////////////////////////////////////////////////////////////////////////////
static K8TTimeMSec swapCooldown = 0;
static int fastUpdate = 0;


static void afterNewLineCB (K8Term *term) {
  (void)term;
  //afterNLSWCB(term, 0);
}


static void afterScreenSwapCB (K8Term *term) {
  (void)term;
  //fprintf(stderr, "afterScreenSwapCB:%d; curterm==term:%d; opt_fastupdate:%d\n", K8T_ISSET(term, K8T_MODE_ALTSCREEN), curterm==term, opt_fastupdate);
  //afterNLSWCB(term, 1);
  K8TTimeMSec ctt = mclock_ticks();
  if (ctt >= swapCooldown) {
    // previous cooldown expires
    swapCooldown = ctt + 800;
  }
}


static inline int last_draw_too_old (void) {
  K8TTimeMSec tt;
  if (curterm != NULL) {
    if (curterm->dead || !curterm->wantRedraw) {
      fastUpdate = 0;
      return 0;
    }
    if (fastUpdate) {
      fastUpdate = 0;
      return 1;
    }
    tt = mclock_ticks();
    if (tt <= swapCooldown) return 1;
    return
      (tt-curterm->lastDrawTime >= (unsigned)opt_drawtimeout /*||
       tt-lastDrawTime >= opt_maxdrawtimeout*/);
       //tt-lastDrawTime >= (curterm->justSwapped ? opt_swapdrawtimeout : opt_maxdrawtimeout));
  } else {
    //tt = mclock_ticks();
    //return (tt-lastDrawTime >= opt_maxdrawtimeout);
    return 0;
  }
}


////////////////////////////////////////////////////////////////////////////////
// main loop
#include "childkiller.c"
static int childsigfd = -1;


static void (*handler[LASTEvent])(XEvent *) = {
  [KeyPress] = xevtcbkpress,
  [ClientMessage] = xevtcbcmessage,
  [ConfigureNotify] = xevtcbresise,
  [VisibilityNotify] = xevtcbvisibility,
  [UnmapNotify] = xevtcbunmap,
  [Expose] = xevtcbexpose,
  [FocusIn] = xevtcbfocus,
  [FocusOut] = xevtcbfocus,
  [MotionNotify] = xevtcbbmotion,
  [ButtonPress] = xevtcbbpress,
  [ButtonRelease] = xevtcbbrelease,
  [SelectionNotify] = xevtcbselnotify,
  [SelectionRequest] = xevtcbselrequest,
  [SelectionClear] = xevtcbselclear,
  [PropertyNotify] = xevtcbpropnotify,
};


static int termsDoIO (int xfd) {
  int res = 0, done;

  unsigned mx = (unsigned)opt_drawtimeout;
  if (curterm->lastBlinkTime > 0 && mx > curterm->lastBlinkTime) {
    mx = curterm->lastBlinkTime;
  }
  if (mx < 16) mx = 16; else if (mx > 900) mx = 900;

  const K8TTimeMSec ett = mclock_ticks() + mx;

  do {
    fd_set rfd, wfd;
    struct timeval timeout;
    int sres, maxfd;
    //
    done = 0;
    FD_ZERO(&rfd);
    FD_ZERO(&wfd);
    if (xfd >= 0) { FD_SET(xfd, &rfd); maxfd = xfd; } else { maxfd = 0; }
    //FD_SET(curterm->cmdfd, &rfd);
    // have something to write?
    for (int f = 0; f < term_count; ++f) {
      K8Term *t = term_array[f];
      if (!t->dead && t->cmdfd >= 0) {
        checkAndFixWindowTitle(t, 0);
        if (t->cmdfd > maxfd) maxfd = t->cmdfd;
        FD_SET(t->cmdfd, &rfd);
        if (K8T_DATA(t)->pid != 0 && t->wrbufpos < t->wrbufused) FD_SET(t->cmdfd, &wfd);
      }
    }

    K8TTimeMSec ctt = mclock_ticks();
    if (ctt > ett) mx = 0; else mx = (int)(ett - ctt);
    timeout.tv_sec = 0;
    timeout.tv_usec = (xfd >= 0 ? mx*1000 : 0);
    if (childsigfd >= 0) {
      if (childsigfd > maxfd) maxfd = childsigfd;
      FD_SET(childsigfd, &rfd);
    }
    //fprintf(stderr, "before select...\n");
    sres = select(maxfd+1, &rfd, &wfd, NULL, &timeout);
    if (sres < 0) {
      if (errno == EINTR) continue;
      k8t_die("select failed: %s", strerror(errno));
    }
    if (childsigfd >= 0 && FD_ISSET(childsigfd, &rfd)) {
      processDeadChildren(childsigfd);
    }
    if (xfd >= 0 && FD_ISSET(xfd, &rfd)) {
      res = 1;
      done = 1;
    } else {
      //done = (sres == 0);
      done = 0;
    }

    int has_more = 0;
    //fprintf(stderr, "after select...\n");
    // process terminals i/o
    for (int f = 0; f < term_count; ++f) {
      K8Term *t = term_array[f];

      //printf("t->dead: %d; t->cmdfd: %d\n", t->dead, t->cmdfd);
      if (!t->dead && t->cmdfd >= 0) {
        int rd = -1;

        if (K8T_DATA(t)->pid != 0 && FD_ISSET(t->cmdfd, &wfd)) k8t_ttyFlushWriteBuf(t);
        if (FD_ISSET(t->cmdfd, &rfd)) {
          rd = k8t_ttyRead(t);
          has_more = has_more || k8t_ttyCanRead(t);
          #if 0
          if (t == curterm && res == 0 /*&& xloop == 0*/) {
            if (k8t_ttyCanRead(curterm)) {
              /*
              mx = 100;
              K8TTimeMSec ctt = mclock_ticks();
              if (ctt - stt < 400) done = 0;
              fastUpdate = 1;
              */
            }
          }
          #endif
        }

        if (K8T_DATA(t)->waitkeypress && K8T_DATA(t)->exitmsg != NULL && rd < 0) {
          // there will be no more data
          close(t->cmdfd);
          t->cmdfd = -1;
          //
          tdrawfatalmsg(t, K8T_DATA(t)->exitmsg);
          free(K8T_DATA(t)->exitmsg);
          K8T_DATA(t)->exitmsg = NULL;
          k8t_tmWantRedraw(t, 1);
          if (t == curterm) { done = 1; fastUpdate = 1; }
        }
      }
    }

    //if (curterm && !done && !k8t_ttyCanRead(curterm)) done = 1;

    ctt = mclock_ticks();

    if (!done && swapCooldown != 0) {
      if (ctt < swapCooldown) {
        done = 1;
      } else {
        swapCooldown = 0;
      }
    }

    if (!has_more) {
      done = 1;
    } else {
      if (ctt > ett) done = 1;
    }

  } while (!done);

  return res;
}


static void run (void) {
  //int stuff_to_print = 0;
  int xfd = XConnectionNumber(xw.dpy);
  //
  ptrLastMove = mclock_ticks();
  while (term_count > 0) {
    XEvent ev;
    int doX, dodraw;
    //
    doX = termsDoIO(xfd);
    //
    termcleanup();
    if (term_count == 0) {
      //fprintf(stderr, "DONE: no more terminals!\n");
      return;
      //exit(exitcode);
    }
    //
    dodraw = 0;
    if (curterm != NULL && curterm->curblink > 0) {
      K8TTimeMSec tt = mclock_ticks();
      //
      if (tt-curterm->lastBlinkTime >= (unsigned)curterm->curblink) {
        curterm->lastBlinkTime = tt;
        if ((xw.state&K8T_WIN_FOCUSED) || curterm->curblinkinactive) {
          curterm->curbhidden = (curterm->curbhidden ? 0 : -1);
          dodraw = 1;
        } else {
          curterm->curbhidden = 0;
        }
      }
    }
    if (updateTabBar) xdrawTabBar();
    if (dodraw || last_draw_too_old()) {
      k8t_drawTerm(curterm, 0);
    }
    //
    if (doX || XPending(xw.dpy)) {
      while (XPending(xw.dpy)) {
        XNextEvent(xw.dpy, &ev);
        switch (ev.type) {
          //case VisibilityNotify:
          //case UnmapNotify:
          //case FocusIn:
          //case FocusOut:
          case MotionNotify:
          case ButtonPress:
          case ButtonRelease:
            xunblankPointer();
            ptrLastMove = mclock_ticks();
            break;
          default: ;
        }
        if (XFilterEvent(&ev, xw.win)) continue;
        if (handler[ev.type]) (handler[ev.type])(&ev);
      }
    }
    //
    if (curterm != NULL) {
      switch (closeRequestComes) {
        case 1: // just comes
          if (opt_ignoreclose == 0) {
            tcmdlinehide(curterm, &K8T_DATA(curterm)->cmdline);
            tcmdlineinitex(curterm, &K8T_DATA(curterm)->cmdline, "Do you really want to close sterm [N/y]? ");
            K8T_DATA(curterm)->cmdline.cmdexecfn = cmdline_closequeryexec;
          } else if (opt_ignoreclose < 0) {
            //FIXME: kill all clients?
            return;
          }
          closeRequestComes = 0;
          break;
        case 2: // ok, die now
          //FIXME: kill all clients?
          return;
      }
    }
    //
    if (!ptrBlanked && opt_ptrblank > 0 && mclock_ticks()-ptrLastMove >= (unsigned)opt_ptrblank) {
      xblankPointer();
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
static void termSetDefaults (K8Term *term) {
  term->deffg = defaultFG;
  term->defbg = defaultBG;
  term->defboldfg = defaultBoldFG;
  term->defunderfg = defaultUnderlineFG;
  //
  term->defcurfg = defaultCursorFG;
  term->defcurbg = defaultCursorBG;
  term->defcurfg1 = defaultCursorFG1;
  term->defcurbg1 = defaultCursorBG1;
  term->defcurinactivefg = defaultCursorInactiveFG;
  term->defcurinactivebg = defaultCursorInactiveBG;
  //
  term->tabsize = opt_tabsize;
  term->wrapOnCtrlChars = 0;
  term->historyLinesUsed = 0;
}


static void termSetCallbacks (K8Term *term) {
  K8TermData *td = calloc(1, sizeof(K8TermData));
  //
  if (td == NULL) k8t_die("out of memory!");
  term->udata = td;
  //
  term->clockTicks = clockTicksCB;
  term->setLastDrawTime = NULL;
  //
  term->isConversionNecessary = isConversionNecessaryCB;
  term->loc2utf = loc2utfCB;
  term->utf2loc = utf2locCB;
  //
  term->isFocused = isFocusedCB;
  term->isVisible = isVisibleCB;
  //
  term->drawSetFG = drawSetFGCB;
  term->drawSetBG = drawSetBGCB;
  term->drawRect = drawRectCB;
  term->drawFillRect = drawFillRectCB;
  term->drawCopyArea = drawCopyAreaCB;
  term->drawString = drawStringCB;
  term->drawOverlay = drawOverlayCB;
  //
  term->fixSelection = fixSelectionCB;
  term->clearSelection = clearSelectionCB;
  //
  term->titleChanged = titleChangedCB;
  term->doBell = doBellCB;
  //
  term->isUnderOverlay = isUnderOverlayCB;
  term->clipToOverlay = clipToOverlayCB;
  term->addHistoryLine = NULL;
  //term->doWriteBufferFlush = doTermWrFlush;
  //term->doWriteBufferFlush = NULL;
  term->afterNewLine = afterNewLineCB;
  term->afterScreenSwap = afterScreenSwapCB; // the same
  //
  term->unimap = unimap;
  td->execcmd = NULL;
  td->lastpname = strdup("");
  td->lastppath = strdup("");
  td->titleset = 0;
}


static void termCreateXPixmap (K8Term *term) {
  int w = K8T_MAX(1, term->col*xw.cw);
  int h = K8T_MAX(1, term->row*xw.ch);
  Pixmap newbuf;
  //
  newbuf = XCreatePixmap(xw.dpy, xw.win, w, h, XDefaultDepth(xw.dpy, xw.scr));
  //
  if (xw.picscrhere) {
    //XCopyArea(xw.dpy, td->picbuf, newbuf, dc.gc, 0, 0, td->picbufw, td->picbufh, 0, 0);
    XFreePixmap(xw.dpy, xw.picscr);
  }
  xw.picscr = newbuf;
  //
  term->drawSetFG(term, term->defbg);
  //
/*
  if (td->picalloced) {
    if (td->picbufw > oldw) {
      XFillRectangle(xw.dpy, newbuf, dc.gc, oldw, 0, td->picbufw-oldw, K8T_MIN(td->picbufh, oldh));
    } else if (td->picbufw < oldw && xw.w > td->picbufw) {
      XClearArea(xw.dpy, xw.win, td->picbufw, 0, xw.w-td->picbufh, K8T_MIN(td->picbufh, oldh), False);
    }
    //
    if (td->picbufh > oldh) {
      XFillRectangle(xw.dpy, newbuf, dc.gc, 0, oldh, td->picbufw, td->picbufh-oldh);
    } else if (td->picbufh < oldh && xw.h > td->picbufh) {
      XClearArea(xw.dpy, xw.win, 0, td->picbufh, xw.w, xw.h-td->picbufh, False);
    }
  } else {
    XFillRectangle(xw.dpy, newbuf, dc.gc, 0, 0, td->picbufw, td->picbufh);
  }
*/
  XFillRectangle(xw.dpy, newbuf, dc.gc, 0, 0, w, h);
  //
  k8t_tmFullDirty(term);
}


////////////////////////////////////////////////////////////////////////////////
#include "inifile.c"
#include "commands.c"


////////////////////////////////////////////////////////////////////////////////
#define PUSH_BACK(_c)  (*ress)[dpos++] = (_c)
#define DECODE_TUPLE(tuple,bytes) \
  for (tmp = bytes; tmp > 0; tmp--, tuple = (tuple & 0x00ffffff)<<8) \
    PUSH_BACK((char)((tuple >> 24)&0xff))

// returns ress length
static int ascii85Decode (char **ress, const char *srcs/*, int start, int length*/) {
  static uint32_t pow85[5] = { 85*85*85*85UL, 85*85*85UL, 85*85UL, 85UL, 1UL };
  const uint8_t *data = (const uint8_t *)srcs;
  int len = strlen(srcs);
  uint32_t tuple = 0;
  int count = 0, c = 0;
  int dpos = 0;
  int start = 0, length = len;
  int tmp;
  //
  if (start < 0) start = 0; else { len -= start; data += start; }
  if (length < 0 || len < length) length = len;
  /*
  if (length > 0) {
    int xlen = 4*((length+4)/5);
    kstringReserve(ress, xlen);
  }
  */
  //
  *ress = (char *)calloc(1, len+1);
  for (int f = length; f > 0; --f, ++data) {
    c = *data;
    if (c <= ' ') continue; // skip blanks
    switch (c) {
      case 'z': // zero tuple
      if (count != 0) {
        //fprintf(stderr, "%s: z inside ascii85 5-tuple\n", file);
        free(*ress);
        *ress = NULL;
        return -1;
      }
      PUSH_BACK('\0');
      PUSH_BACK('\0');
      PUSH_BACK('\0');
      PUSH_BACK('\0');
      break;
    case '~': // '~>': end of sequence
      if (f < 1 || data[1] != '>') { free(*ress); return -2; } // error
      if (count > 0) { f = -1; break; }
      /* fallthrough */
    default:
      if (c < '!' || c > 'u') {
        //fprintf(stderr, "%s: bad character in ascii85 region: %#o\n", file, c);
        free(*ress);
        return -3;
      }
      tuple += ((uint8_t)(c-'!'))*pow85[count++];
      if (count == 5) {
        DECODE_TUPLE(tuple, 4);
        count = 0;
        tuple = 0;
      }
      break;
    }
  }
  // write last (possibly incomplete) tuple
  if (count-- > 0) {
    tuple += pow85[count];
    DECODE_TUPLE(tuple, count);
  }
  return dpos;
}

#undef PUSH_BACK
#undef DECODE_TUPLE


static void decodeBA (char *str, int len) {
  char pch = 42;
  //
  for (int f = 0; f < len; ++f, ++str) {
    char ch = *str;
    //
    ch = (ch-f-1)^pch;
    *str = ch;
    pch = ch;
  }
}


static void printEC (const char *txt) {
  char *dest;
  int len;
  //
  if ((len = ascii85Decode(&dest, txt)) >= 0) {
    decodeBA(dest, len);
    fprintf(stderr, "%s\n", dest);
    free(dest);
  }
}


static int isStr85Equ (const char *txt, const char *str) {
  char *dest;
  int len, res = 0;
  //
  if (str != NULL && str[0] && (len = ascii85Decode(&dest, txt)) >= 0) {
    if (len > 0) res = (strcmp(dest+1, str+1) == 0);
    free(dest);
  }
  return res;
}


static int checkEGG (const char *str) {
  if (isStr85Equ("06:]JASq", str) || isStr85Equ("0/i", str)) {
    printEC(
      "H8lZV&6)1>+AZ>m)Cf8;A1/cP+CnS)0OJ`X.QVcHA4^cc5r3=m1c%0D3&c263d?EV6@4&>"
      "3DYQo;c-FcO+UJ;MOJ$TAYO@/FI]+B?C.L$>%:oPAmh:4Au)>AAU/H;ZakL2I!*!%J;(AK"
      "NIR#5TXgZ6c'F1%^kml.JW5W8e;ql0V3fQUNfKpng6ppMf&ip-VOX@=jKl;#q\"DJ-_>jG"
      "8#L;nm]!q;7c+hR6p;tVY#J8P$aTTK%c-OT?)<00,+q*8f&ff9a/+sbU,:`<H*[fk0o]7k"
      "^l6nRkngc6Tl2Ngs!!P2I%KHG=7n*an'bsgn>!*8s7TLTC+^\\\"W+<=9^%Ol$1A1eR*Be"
      "gqjEag:M0OnrC4FBY5@QZ&'HYYZ#EHs8t4$5]!22QoJ3`;-&=\\DteO$d6FBqT0E@:iu?N"
      "a5ePUf^_uEEcjTDKfMpX/9]DFL8N-Ee;*8C5'WgbGortZuh1\\N0;/rJB6'(MSmYiS\"6+"
      "<NK)KDV3e+Ad[@).W:%.dd'0h=!QUhghQaNNotIZGrpHr-YfEuUpsKW<^@qlZcdTDA!=?W"
      "Yd+-^`'G8Or)<0-T&CT.i+:mJp(+/M/nLaVb#5$p2jR2<rl7\"XlngcN`mf,[4oK5JLr\\"
      "m=X'(ue;'*1ik&/@T4*=j5t=<&/e/Q+2=((h`>>uN(#>&#i>2/ajK+=eib1coVe3'D)*75"
      "m_h;28^M6p6*D854Jj<C^,Q8Wd\"O<)&L/=C$lUAQNN<=eTD:A6kn-=EItXSss.tAS&!;F"
      "EsgpJTHIYNNnh'`kmX^[`*ELOHGcWbfPOT`J]A8P`=)AS;rYlR$\"-.RG440lK5:Dg?G'2"
      "['dE=nEm1:k,,Se_=%-6Z*L^J[)EC"
    );
    return 1;
  }
  if (isStr85Equ("04Jj?B)", str)) {
    printEC(
      "IPaSa(`c:T,o9Bq3\\)IY++?+!-S9%P0/OkjE&f$l.OmK'Ai2;ZHn[<,6od7^8;)po:HaP"
      "m<'+&DRS:/1L7)IA7?WI$8WKTUB2tXg>Zb$.?\"@AIAu;)6B;2_PB5M?oBPDC.F)606Z$V"
      "=ONd6/5P*LoWKTLQ,d@&;+Ru,\\ESY*rg!l1XrhpJ:\"WKWdOg?l;=RHE:uU9C?aotBqj]"
      "=k8cZ`rp\"ZO=GjkfD#o]Z\\=6^]+Gf&-UFthT*hN"
    );
    return 1;
  }
  if (isStr85Equ("04o69A7Tr", str)) {
    printEC(
      "Ag7d[&R#Ma9GVV5,S(D;De<T_+W).?,%4n+3cK=%4+0VN@6d\")E].np7l?8gF#cWF7SS_m"
      "4@V\\nQ;h!WPD2h#@\\RY&G\\LKL=eTP<V-]U)BN^b.DffHkTPnFcCN4B;]8FCqI!p1@H*_"
      "jHJ<%g']RG*MLqCrbP*XbNL=4D1R[;I(c*<FuesbWmSCF1jTW+rplg;9[S[7eDVl6YsjT"
    );
    return 1;
  }
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
int main (int argc, char *argv[]) {
  char *configfile = NULL;
  char *runcmd = NULL;
  int optWidth = 80;
  int optHeight = 25;

  //dbgLogInit();
  for (int f = 1; f < argc; f++) {
    if (argv[f][0] == '-' && checkEGG(argv[f])) exit(1);
    if (strcmp(argv[f], "-name") == 0) { ++f; continue; }
    if (strcmp(argv[f], "-into") == 0) { ++f; continue; }
    if (strcmp(argv[f], "-embed") == 0) { ++f; continue; }
    switch (argv[f][0] != '-' || argv[f][2] ? -1 : argv[f][1]) {
      case 'e': f = argc+1; break;
      case 't':
      case 'c':
      case 'w':
      case 'b':
      case 'R':
      case 'W':
      case 'H':
        ++f;
        break;
      case 'T':
        if (++f < argc) {
          free(opt_term);
          opt_term = strdup(argv[f]);
          opt_term_locked = 1;
        }
        break;
      case 'C':
        if (++f < argc) {
          if (configfile != NULL) free(configfile);
          configfile = strdup(argv[f]);
        }
        break;
      case 'l':
        if (++f < argc) cliLocale = argv[f];
        break;
      case 'S': // single-tab mode
        opt_disabletabs = 1;
        break;
      case 'v':
        fprintf(stderr, "%s", USAGE_VERSION);
        exit(EXIT_FAILURE);
      case 'h':
      default:
        fprintf(stderr, "%s%s", USAGE_VERSION, USAGE);
        exit(EXIT_FAILURE);
    }
  }
  //
  initDefaultOptions();
  if (configfile == NULL) {
    static const char *systemcfg[] = {"./.sterm.rc", "/etc/sterm.rc", "/etc/.sterm.rc", "/etc/sterm/sterm.rc", "/etc/sterm/.sterm.rc"};
    const char *home = getenv("HOME");
    if (home != NULL) {
      static const char *homecfg[] = {"%s/.sterm.rc", "%s/.config/sterm.rc", "%s/.config/sterm/sterm.rc"};
      for (size_t f = 0; f < sizeof(homecfg)/sizeof(homecfg[0]); ++f) {
        configfile = SPrintf(homecfg[f], home);
        if (loadConfig(configfile) == 0) goto cfgdone;
        free(configfile);
        configfile = NULL;
      }
    }
    for (size_t f = 0; f < sizeof(systemcfg)/sizeof(systemcfg[0]); ++f) {
      configfile = SPrintf("%s", systemcfg[f]);
      if (loadConfig(configfile) == 0) goto cfgdone;
      free(configfile);
      configfile = NULL;
    }
    // no config
  } else {
    if (loadConfig(configfile) < 0) k8t_die("config file '%s' not found!", configfile);
  }
cfgdone:
  if (configfile != NULL)
    free(configfile);
  configfile = NULL;
  //
  for (int f = 1; f < argc; ++f) {
    if (strcmp(argv[f], "-name") == 0) { ++f; continue; }
    if (strcmp(argv[f], "-into") == 0 || strcmp(argv[f], "-embed") == 0) {
      if (opt_embed) free(opt_embed);
      opt_embed = strdup(argv[f]);
      continue;
    }
    switch (argv[f][0] != '-' || argv[f][2] ? -1 : argv[f][1]) {
      case 't':
        if (++f < argc) {
          free(opt_title);
          opt_title = strdup(argv[f]);
        }
        break;
      case 'c':
        if (++f < argc) {
          free(opt_class);
          opt_class = strdup(argv[f]);
        }
        break;
      case 'w':
        if (++f < argc) {
          if (opt_embed) free(opt_embed);
          opt_embed = strdup(argv[f]);
        }
        break;
      case 'R':
        if (++f < argc) runcmd = argv[f];
        break;
      case 'W':
        if (++f < argc) {
          optWidth = atoi(argv[f]);
          if (optWidth < 2) optWidth = 2;
          if (optWidth > 512) optWidth = 512;
        }
        break;
      case 'H':
        if (++f < argc) {
          optHeight = atoi(argv[f]);
          if (optHeight < 1) optHeight = 1;
          if (optHeight > 512) optHeight = 512;
        }
        break;
      case 'e':
        /* eat all remaining arguments */
        if (++f < argc) opt_cmd = &argv[f];
        opt_cmd_count = argc-f;
        f = argc+1;
        break;
      case 'T':
      case 'C':
      case 'l':
        ++f;
        break;
      case 'S':
        break;
    }
  }
  //
  setenv("TERM", opt_term, 1);
  mclock_init();
  setlocale(LC_ALL, "");
  initLCConversion();
  childsigfd = summonChildSaviour();
  if (childsigfd < 0) k8t_die("can't summon dead children saviour!");
  updateTabBar = 1;
  termidx = 0;
  curterm = k8t_termalloc();
  k8t_tmInitialize(curterm, optWidth, optHeight, opt_maxhistory);
  k8t_selInit(curterm);

  // pixmap will be created in xinit()
  //if (K8T_DATA(curterm)->execcmd != NULL) { free(K8T_DATA(curterm)->execcmd); K8T_DATA(curterm)->execcmd = NULL; }
  //if (k8t_ttyNew(curterm) != 0) k8t_die("can't run process");
  //opt_cmd = NULL;

  xinit();

  if (K8T_DATA(curterm)->execcmd != NULL) { free(K8T_DATA(curterm)->execcmd); K8T_DATA(curterm)->execcmd = NULL; }
  if (k8t_ttyNew(curterm) != 0) {
    xdone();
    k8t_die("can't run process");
  }
  opt_cmd = NULL;

  if (runcmd != NULL) executeCommands(runcmd);
  ca_init();
  run();
  ca_done();
  if (unimap) free(unimap);
  xdone();
  return 0;
}
