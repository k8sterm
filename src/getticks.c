////////////////////////////////////////////////////////////////////////////////
// getticks
static struct timespec mclk_sttime; // starting time of monotonic clock

#ifdef __MACH__

#include <sys/time.h>

#define CLOCK_REALTIME			0
#define CLOCK_MONOTONIC			1
#define CLOCK_PROCESS_CPUTIME_ID	2
#define CLOCK_THREAD_CPUTIME_ID		3
#define CLOCK_MONOTONIC_RAW		4
#define CLOCK_REALTIME_COARSE		5
#define CLOCK_MONOTONIC_COARSE		6

//clock_gettime is not implemented on OSX
// This implementation is found on the internet. Not sure if it's 100% correct

int clock_gettime(int clk_id, struct timespec* t) {
    struct timeval now;
    int rv = gettimeofday(&now, NULL);
    if (rv) return rv;
    t->tv_sec  = now.tv_sec;
    t->tv_nsec = now.tv_usec * 1000;
    return 0;
}

#endif

static void mclock_init (void) {
  clock_gettime(CLOCK_MONOTONIC /*CLOCK_MONOTONIC_RAW*/, &mclk_sttime);
  if (mclk_sttime.tv_sec > 0) --mclk_sttime.tv_sec;
}


static K8TTimeMSec mclock_ticks (void) {
  struct timespec tp;
  clock_gettime(CLOCK_MONOTONIC /*CLOCK_MONOTONIC_RAW*/, &tp);
  tp.tv_sec -= mclk_sttime.tv_sec;
  return (K8TTimeMSec)tp.tv_sec*(K8TTimeMSec)1000+(K8TTimeMSec)(tp.tv_nsec/1000000);
}
