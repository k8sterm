// parse the argument list
// return error message or NULL
// format:
//   '*': skip
//   's': string (char *)
//   'i': integer (int *)
//   'b': boolean (int *)
//   '|': optional arguments follows
//   '.': stop parsing, ignore rest
//   'R': stop parsing, set rest ptr (char *)
//  string modifiers (also for 'R'):
//    '!' -- don't allow empty strings
//    '-' -- trim spaces
//  int modifiers:
//    {lo,hi}
//    {,hi}
//    {lo}
// WARNING! `line` will be modified!
// WARNING! string pointers will point INSIDE `line`, so don't discard it!
// UGLY! REWRITE!
const char *iniParseArguments (char *line, const char *fmt, ...) {
  va_list ap;
  int inOptional = 0;
  //
  if (line == NULL) return "alas";
  trimstr(line);
  va_start(ap, fmt);
  while (*fmt) {
    char spec = *fmt++, *args;
    //
    if (spec == '|') { inOptional = 1; continue; }
    if (spec == '.') { va_end(ap); return NULL; }
    //
    while (*line && isspace(*line)) ++line;
    if (*line == '#') *line = 0;
    //
    if (spec == 'R') {
      char **p = va_arg(ap, char **);
      int noempty = 0;
      //
      while (*fmt) {
        if (*fmt == '!') { ++fmt; noempty = 1; }
        else if (*fmt == '-') { ++fmt; trimstr(line); }
        else break;
      }
      va_end(ap);
      if (noempty && !line[0]) return "invalid empty arg";
      if (p != NULL) *p = line;
      return NULL;
    }
    //
    if (!line[0]) {
      // end of line, stop right here
      va_end(ap);
      if (!inOptional) return "out of args";
      return NULL;
    }
    //
    args = line;
    {
      char *dest = args, qch = '#';
      int n;
      //
      if (line[0] == '"' || line[0] == '\'') qch = *line++;
      //
      while (*line && *line != qch) {
        if (qch == '#' && isspace(*line)) break;
        //
        if (*line == '\\') {
          if (!line[1]) { va_end(ap); return "invalid escape"; }
          switch (*(++line)) {
            case 'n': *dest++ = '\n'; ++line; break;
            case 'r': *dest++ = '\r'; ++line; break;
            case 't': *dest++ = '\t'; ++line; break;
            case 'a': *dest++ = '\a'; ++line; break;
            case 'e': *dest++ = '\x1b'; ++line; break; // esc
            case 's': *dest++ = ' '; ++line; break;
            case 'x': // hex
              ++line;
              if (!isxdigit(*line)) { va_end(ap); return "invalid hex escape"; }
              n = toupper(*line)-'0'; if (n > 9) n -= 7;
              ++line;
              if (isxdigit(*line)) {
                int b = toupper(*line)-'0'; if (b > 9) b -= 7;
                //
                n = (n*16)+b;
                ++line;
              }
              *dest++ = n;
              break;
            case '0': // octal
              n = 0;
              for (int f = 0; f < 4; ++f) {
                if (*line < '0' || *line > '7') break;
                n = (n*8)+(line[0]-'0');
                if (n > 255) { va_end(ap); return "invalid oct escape"; }
                ++line;
              }
              if (n == 0) { va_end(ap); return "invalid oct escape"; }
              *dest++ = n;
              break;
            case '1'...'9': // decimal
              n = 0;
              for (int f = 0; f < 3; ++f) {
                if (*line < '0' || *line > '9') break;
                n = (n*8)+(line[0]-'0');
                if (n > 255) { va_end(ap); return "invalid dec escape"; }
                ++line;
              }
              if (n == 0) { va_end(ap); return "invalid oct escape"; }
              *dest++ = n;
              break;
            default:
              *dest++ = *line++;
              break;
          }
        } else {
          *dest++ = *line++;
        }
      }
      if (qch != '#') {
        if (*line != qch) return "unfinished string";
        if (*line) ++line;
      } else if (*line && *line != '#') ++line;
      *dest = 0;
    }
    // now process and convert argument
    switch (spec) {
      case '*': /* skip */
        break;
      case 's': { /* string */
        int noempty = 0, trim = 0;
        char **p;
        //
        for (;;) {
          if (*fmt == '!') { noempty = 1; ++fmt; }
          else if (*fmt == '-') { trim = 1; ++fmt; }
          else break;
        }
        //
        if (trim) trimstr(args);
        //
        if (noempty && !args[0]) { va_end(ap); return "invalid empty string"; }
        p = va_arg(ap, char **);
        if (p != NULL) *p = args;
        } break;
      case 'i': /* int */
        if (!args[0]) {
          va_end(ap);
          return "invalid integer";
        } else {
          int *p = va_arg(ap, int *);
          long int n;
          char *eptr;
          //
          trimstr(args);
          n = strtol(args, &eptr, 0);
          if (*eptr) { va_end(ap); return "invalid integer"; }
          //
          if (*fmt == '{') {
            // check min/max
            int minmax[2], haveminmax[2];
            //
            haveminmax[0] = haveminmax[1] = 0;
            minmax[0] = minmax[1] = 0;
            ++fmt;
            for (int f = 0; f < 2; ++f) {
              if (isdigit(*fmt) || *fmt == '-' || *fmt == '+') {
                int neg = 0;
                haveminmax[f] = 1;
                if (*fmt == '-') neg = 1;
                if (!isdigit(*fmt)) {
                  ++fmt;
                  if (!isdigit(*fmt)) { va_end(ap); return "invalid integer bounds"; }
                }
                while (isdigit(*fmt)) {
                  minmax[f] = minmax[f]*10+(fmt[0]-'0');
                  ++fmt;
                }
                if (neg) minmax[f] = -minmax[f];
                //fprintf(stderr, "got: %d\n", minmax[f]);
              }
              if (*fmt == ',') {
                if (f == 1) { va_end(ap); return "invalid integer bounds: extra comma"; }
                // do nothing, we are happy
                ++fmt;
              } else if (*fmt == '}') {
                // ok, done
                break;
              } else { va_end(ap); return "invalid integer bounds"; }
            }
            if (*fmt != '}') { va_end(ap); return "invalid integer bounds"; }
            ++fmt;
            //
            //fprintf(stderr, "b: (%d,%d) (%d,%d)\n", haveminmax[0], minmax[0], haveminmax[1], minmax[1]);
            if ((haveminmax[0] && n < minmax[0]) || (haveminmax[1] && n > minmax[1])) { va_end(ap); return "integer out of bounds"; }
          }
          //
          if (p) *p = n;
        }
        break;
      case 'b': { /* bool */
        int *p = va_arg(ap, int *);
        //
        trimstr(args);
        if (!args[0]) { va_end(ap); return "invalid boolean"; }
        if (strcasecmp(args, "true") == 0 || strcasecmp(args, "on") == 0 ||
            strcasecmp(args, "tan") == 0 || strcasecmp(args, "1") == 0 ||
            strcasecmp(args, "yes") == 0) {
          if (p) *p = 1;
        } else if (strcasecmp(args, "false") == 0 || strcasecmp(args, "off") == 0 ||
                   strcasecmp(args, "ona") == 0 || strcasecmp(args, "0") == 0 ||
                   strcasecmp(args, "no") == 0) {
          if (p) *p = 0;
        } else {
          va_end(ap);
          return "invalid boolean";
        }
        } break;
      default:
        va_end(ap);
        return "invalid format specifier";
    }
  }
  va_end(ap);
  while (*line && isspace(*line)) ++line;
  if (!line[0] || line[0] == '#') return NULL;
  return "extra args";
}
