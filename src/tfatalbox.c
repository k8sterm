////////////////////////////////////////////////////////////////////////////////
//SHITCODE!
static void tDrawFatalBox (K8Term *term, const char *msg) {
  if (term != NULL) {
    static const char *title = " GURU MEDITAION ";
    static const char *clr = "\x1b[0;1;33;41m";
    static const char *clrdef = "\x1b[0m";
    int mlen, tlen = strlen(title), wdt = tlen+4;
    //
    if (msg == NULL) msg = "";
    if ((mlen = strlen(msg))+4 > wdt) wdt = mlen+4;
    //
    k8t_ttyPutStr(term, clr);
    //
    if (term->col <= mlen+4) {
      k8t_ttyPutStr(term, msg);
      return;
    }
    //
    term->charset = K8T_MODE_GFX1;
    term->mode &= ~K8T_MODE_GFX0;
    term->mode |= K8T_MODE_GFX1;
    //term->c.attr.attr |= K8T_ATTR_REVERSE;
    if (wdt == tlen+4) {
      // just title
      int left = (wdt-2-mlen)/2, cnt = left+1+mlen;
      //
      k8t_ttyPutStr(term, "lq");
      k8t_ttyPutStr(term, "\x1b[37m");
      k8t_ttyPutStr(term, title);
      k8t_ttyPutStr(term, "\x1b[33m");
      k8t_ttyPutStr(term, "qk");
      //
      k8t_ttyPutStr(term, clrdef);
      k8t_ttyPutStr(term, "\r\n");
      k8t_ttyPutStr(term, clr);
      //
      k8t_tmPutC(term, "x");
      while (left-- > 0) k8t_tmPutC(term, " ");
      term->charset = K8T_MODE_GFX0;
      k8t_ttyPutStr(term, msg);
      term->charset = K8T_MODE_GFX1;
      while (cnt++ < wdt-1) k8t_tmPutC(term, " ");
      k8t_ttyPutStr(term, "x");
      //
      k8t_ttyPutStr(term, clrdef);
      k8t_ttyPutStr(term, "\r\n");
      k8t_ttyPutStr(term, clr);
      //
      k8t_tmPutC(term, "m");
      for (int f = 2; f < wdt; ++f) k8t_tmPutC(term, "q");
      k8t_tmPutC(term, "j");
    } else {
      int left = (wdt-2-tlen)/2, cnt = left+1+tlen;
      //
      k8t_tmPutC(term, "l");
      while (left-- > 0) k8t_tmPutC(term, "q");
      k8t_ttyPutStr(term, "\x1b[37m");
      k8t_ttyPutStr(term, title);
      k8t_ttyPutStr(term, "\x1b[33m");
      while (cnt++ < wdt-1) k8t_tmPutC(term, "q");
      k8t_tmPutC(term, "k");
      //
      k8t_ttyPutStr(term, clrdef);
      k8t_ttyPutStr(term, "\r\n");
      k8t_ttyPutStr(term, clr);
      //
      k8t_ttyPutStr(term, "x ");
      term->charset = K8T_MODE_GFX0;
      k8t_ttyPutStr(term, msg);
      term->charset = K8T_MODE_GFX1;
      k8t_ttyPutStr(term, " x");
      //
      k8t_ttyPutStr(term, clrdef);
      k8t_ttyPutStr(term, "\r\n");
      k8t_ttyPutStr(term, clr);
      //
      k8t_tmPutC(term, "m");
      for (int f = 2; f < wdt; ++f) k8t_tmPutC(term, "q");
      k8t_tmPutC(term, "j");
    }
    //
    term->charset = K8T_MODE_GFX0;
    //term->c.attr.attr &= ~K8T_ATTR_REVERSE;
    //
    //k8t_ttyPutStr(msg);
  }
  k8t_ttyPutStr(term, "\x1b[0m");
}


static void tdrawfatalmsg (K8Term *term, const char *msg) {
  if (term != NULL && msg != NULL) {
    k8t_tmResetMode(term);
    term->c.state |= K8T_CURSOR_HIDE;
    //k8t_tmMoveTo(0, term->c.y);
    if (term->c.x != 0) k8t_ttyPutStr(term, "\x1b[0m\r\n");
    //
    tDrawFatalBox(term, msg);
    //
    k8t_tmWantRedraw(term, 1);
    k8t_tmFullDirty(term);
  }
}
