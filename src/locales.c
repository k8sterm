////////////////////////////////////////////////////////////////////////////////
// locale conversions
static iconv_t icFromLoc;
static iconv_t icToLoc;
static int needConversion = 0;
static const char *cliLocale = NULL;


static void initLCConversion (void) {
  const char *lct = setlocale(LC_CTYPE, NULL);
  char *cstr;
  //
  needConversion = 0;
  if (cliLocale == NULL) {
    if (strrchr(lct, '.') != NULL) lct = strrchr(lct, '.')+1;
  } else {
    lct = cliLocale;
  }
  if (strcasecmp(lct, "utf8") == 0 || strcasecmp(lct, "utf-8") == 0) return;
  //fprintf(stderr, "locale: [%s]\n", lct);
  icFromLoc = iconv_open("UTF-8", lct);
  if (icFromLoc == (iconv_t)-1) k8t_die("can't initialize locale conversion");
  cstr = SPrintf("%s//TRANSLIT", lct);
  icToLoc = iconv_open(cstr, "UTF-8");
  free(cstr);
  if (icToLoc == (iconv_t)-1) k8t_die("can't initialize locale conversion");
  needConversion = 1;
}


static int loc2utf (char *dest, const char *src, int len) {
  if (needConversion) {
    char *ibuf, *obuf;
    size_t il, ol, ool;
    //
    ibuf = (char *)src;
    obuf = dest;
    il = len;
    ool = ol = il*4;
    il = iconv(icFromLoc, &ibuf, &il, &obuf, &ol);
    if (il == (size_t)-1) return 0;
    return ool-ol;
  } else {
    if (len > 0) memmove(dest, src, len);
    return len;
  }
}


static int utf2loc (char *dest, const char *src, int len) {
  if (needConversion) {
    char *ibuf, *obuf;
    size_t il, ol, ool;
    //
    ibuf = (char *)src;
    obuf = dest;
    il = len;
    ool = ol = il*4;
    il = iconv(icToLoc, &ibuf, &il, &obuf, &ol);
    if (il == (size_t)-1) return 0;
    return ool-ol;
  } else {
    if (len > 0) memmove(dest, src, len);
    return len;
  }
}


static int loc2utfCB (K8Term *term, char *dest, const char *src, int len) {
  (void)term;
  return loc2utf(dest, src, len);
}


static int utf2locCB (K8Term *term, char *dest, const char *src, int len) {
  (void)term;
  return utf2loc(dest, src, len);
}


static int isConversionNecessaryCB (K8Term *term) {
  (void)term;
  return needConversion;
}
