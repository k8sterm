////////////////////////////////////////////////////////////////////////////////
// x11 drawing and utils
/*
static void xcreatebw (void) {
  if ((dc.bcol = calloc(MAX_COLOR+1, sizeof(dc.bcol[0]))) == NULL) k8t_die("out of memory");
  //
  for (int f = 0; f <= MAX_COLOR; ++f) {
    XColor nclr;
    //
    nclr = dc.ncol[f].pixel;
    XQueryColor(xw.dpy, xw.cmap, &nclr);
    fprintf(stderr, "%d: r=%u; g=%u; b=%u\n", f, nclr.red, nclr.green, nclr.blue);
  }
}
*/


static void xallocbwclr (int idx, XColor *color) {
  double lumi;
  //
  XQueryColor(xw.dpy, xw.cmap, color);
  //fprintf(stderr, "%d: r=%u; g=%u; b=%u\n", idx, color->red, color->green, color->blue);
  //
  lumi = 0.3*((double)color->red/65535.0)+0.59*((double)color->green/65535.0)+0.11*((double)color->blue/65535.0);
  color->red = color->green = color->blue = (int)(lumi*65535.0);
  if (!XAllocColor(xw.dpy, xw.cmap, color)) {
    fprintf(stderr, "WARNING: could not allocate b/w color #%d\n", idx);
    return;
  }
  dc.bcol[idx] = color->pixel;
  color->red = color->blue = 0;
  if (!XAllocColor(xw.dpy, xw.cmap, color)) {
    fprintf(stderr, "WARNING: could not allocate b/w color #%d\n", idx);
    return;
  }
  dc.gcol[idx] = color->pixel;
}


static void xallocnamedclr (int idx, const char *cname) {
  XColor color;
  //
  if (!XAllocNamedColor(xw.dpy, xw.cmap, cname, &color, &color)) {
    fprintf(stderr, "WARNING: could not allocate color #%d: '%s'\n", idx, cname);
    return;
  }
  dc.ncol[idx] = color.pixel;
  xallocbwclr(idx, &color);
}


static void xloadcols (void) {
  int f, r, g, b;
  XColor color;
  uint32_t white = WhitePixel(xw.dpy, xw.scr);
  //
  if ((dc.clrs[0] = dc.ncol = calloc(MAX_COLOR+1, sizeof(dc.ncol[0]))) == NULL) k8t_die("out of memory");
  if ((dc.clrs[1] = dc.bcol = calloc(MAX_COLOR+1, sizeof(dc.bcol[0]))) == NULL) k8t_die("out of memory");
  if ((dc.clrs[2] = dc.gcol = calloc(MAX_COLOR+1, sizeof(dc.gcol[0]))) == NULL) k8t_die("out of memory");
  //
  for (f = 0; f <= MAX_COLOR; ++f) dc.ncol[f] = dc.bcol[f] = white;
  /* load colors [0-15] */
  for (f = 0; f <= 15; ++f) {
    const char *cname = opt_colornames[f]!=NULL?opt_colornames[f]:defcolornames[f];
    //
    xallocnamedclr(f, cname);
  }
  /* load colors [256-...] */
  for (f = 256; f <= MAX_COLOR; ++f) {
    const char *cname = opt_colornames[f];
    //
    if (cname == NULL) {
      if (K8T_ARRLEN(defextcolornames) <= (unsigned)f-256) continue;
      cname = defextcolornames[f-256];
    }
    if (cname == NULL) continue;
    xallocnamedclr(f, cname);
  }
  /* load colors [16-255] ; same colors as xterm */
  for (f = 16, r = 0; r < 6; ++r) {
    for (g = 0; g < 6; ++g) {
      for (b = 0; b < 6; ++b) {
        if (opt_colornames[f] != NULL) {
          xallocnamedclr(f, opt_colornames[f]);
        } else {
          color.red = r == 0 ? 0 : 0x3737+0x2828*r;
          color.green = g == 0 ? 0 : 0x3737+0x2828*g;
          color.blue = b == 0 ? 0 : 0x3737+0x2828*b;
          if (!XAllocColor(xw.dpy, xw.cmap, &color)) {
            fprintf(stderr, "WARNING: could not allocate color #%d\n", f);
          } else {
            dc.ncol[f] = color.pixel;
            xallocbwclr(f, &color);
          }
        }
        ++f;
      }
    }
  }
  for (r = 0; r < 24; ++r, ++f) {
    if (opt_colornames[f] != NULL) {
      xallocnamedclr(f, opt_colornames[f]);
    } else {
      color.red = color.green = color.blue = 0x0808+0x0a0a*r;
      if (!XAllocColor(xw.dpy, xw.cmap, &color)) {
        fprintf(stderr, "WARNING: could not allocate color #%d\n", f);
      } else {
        dc.ncol[f] = color.pixel;
        xallocbwclr(f, &color);
      }
    }
  }
  //
  for (size_t f = 0; f < K8T_ARRLEN(opt_colornames); ++f) {
    if (opt_colornames[f]) free(opt_colornames[f]);
  }
}


static void xhints (void) {
  XClassHint class = {opt_class, opt_title};
  XWMHints wm = {.flags = InputHint, .input = 1};
  XSizeHints size = {
    .flags = PSize|PResizeInc|PBaseSize,
    .height = xw.h,
    .width = xw.w,
    .height_inc = xw.ch,
    .width_inc = xw.cw,
    .base_height = xw.h/*xw.tabheight*/,
    .base_width = xw.w,
  };
  //XSetWMNormalHints(xw.dpy, xw.win, &size);
  XSetWMProperties(xw.dpy, xw.win, NULL, NULL, NULL, 0, &size, &wm, &class);
  XSetWMProtocols(xw.dpy, xw.win, &XA_WM_DELETE_WINDOW, 1);
}


#ifdef X11_USE_FUCKED_FONTSETS
static XFontSet xinitfont (const char *fontstr) {
  XFontSet set;
  char *def, **missing;
  int n;
  //
  missing = NULL;
  set = XCreateFontSet(xw.dpy, fontstr, &missing, &n, &def);
  if (missing) {
    while (n--) fprintf(stderr, "sterm: missing fontset: %s\n", missing[n]);
    XFreeStringList(missing);
  }
  return set;
}
#else
static XFontStruct *xinitfont (const char *fontstr) {
  XFontStruct *fst = NULL;
  int fcount = 0;
  char **flist = XListFonts(xw.dpy, (char *)fontstr, 32700/*maxnames*/, &fcount);
  if (flist) {
    //for (int f = 0; f < fcount; ++f) fprintf(stderr, "F#%d: <%s>\n", f, flist[f]);
    for (int f = 0; f < fcount; ++f) {
      if (strstr(flist[f], "-iso10646-1")) {
        fst = XLoadQueryFont(xw.dpy, flist[f]);
        if (fst) {
          int dir, ascent, descent;
          XCharStruct cs;
          XQueryTextExtents(xw.dpy, fst->fid, "W", 1, &dir, &ascent, &descent, &cs);
          if (ascent+descent > 0 && cs.rbearing-cs.lbearing > 0 && cs.width > 0) {
            fprintf(stderr, "X11: loaded font '%s' (hgt=%d; wdt=%d)\n", flist[f], (int)(ascent+descent), (int)cs.width);
            break;
          } else {
            fprintf(stderr, "X11: rejected font '%s'\n", flist[f]);
            XFreeFont(xw.dpy, fst);
          }
        }
      }
    }
    XFreeFontNames(flist);
  }
  if (!fst) {
    fst = XLoadQueryFont(xw.dpy, (char *)fontstr);
  }
  return fst;
}
#endif


#ifdef X11_USE_FUCKED_FONTSETS
static void xgetfontinfo (XFontSet set, int *ascent, int *descent, int *width, Font *fid, int monocheck) {
  XFontStruct **xfonts;
  char **font_names;
  int n;
  //
  *ascent = *descent = 0;
  int lbearing = 0, rbearing = 0;
  n = XFontsOfFontSet(set, &xfonts, &font_names);
  for (int f = 0; f < n; ++f, ++xfonts) {
    //fprintf(stderr, "--%d-- [%s] a=%d; d=%d\n", f, font_names[f], (*xfonts)->ascent, (*xfonts)->descent);
    if (f == 0) *fid = (*xfonts)->fid;
    //if (*rbearing != 0 && (*xfonts)->max_bounds.rbearing > (*rbearing)*2-(*rbearing)/2) continue; /*HACK: skip too wide fonts*/
    *ascent = K8T_MAX(*ascent, (*xfonts)->ascent);
    *descent = K8T_MAX(*descent, (*xfonts)->descent);
    #if 1
    /*
    fprintf(stderr, "--%d-- [%s]\n", f, font_names[f]);
    fprintf(stderr, " ib: l=%d; r=%d\n", (*xfonts)->min_bounds.lbearing, (*xfonts)->min_bounds.rbearing);
    fprintf(stderr, " ab: l=%d; r=%d\n", (*xfonts)->max_bounds.lbearing, (*xfonts)->max_bounds.rbearing);
    */
    lbearing = K8T_MAX(lbearing, (*xfonts)->min_bounds.lbearing);
    rbearing = K8T_MAX(rbearing, (*xfonts)->max_bounds.rbearing);
    #else
    XRectangle r;
    memset(&r, 0, sizeof(r));
    XmbTextExtents(set, "W", 1, &r, NULL);
    fprintf(stderr, "x=%d; w=%d\n", r.x, r.width);
    fprintf(stderr, " ib: l=%d; r=%d\n", (*xfonts)->min_bounds.lbearing, (*xfonts)->min_bounds.rbearing);
    fprintf(stderr, " ab: l=%d; r=%d\n", (*xfonts)->max_bounds.lbearing, (*xfonts)->max_bounds.rbearing);
    lbearing = K8T_MAX(lbearing, r.x);
    rbearing = K8T_MAX(rbearing, r.width);
    #endif
    *width = rbearing-lbearing;
    break; /*HACK: use first font in set*/
  }
}
#else
static void xgetfontinfo (XFontStruct *set, int *ascent, int *descent, int *width, Font *fid, int monocheck) {
  int dir;
  XCharStruct cs;
  *fid = set->fid;
  XQueryTextExtents(xw.dpy, set->fid, "W", 1, &dir, ascent, descent, &cs);
  *width = cs.width;
  if (!(cs.width > 0 && *ascent+*descent > 0)) {
    k8t_die("X11: specified X font has invalid dimensions");
    return;
  }
  //fprintf(stderr, "ASCENT: %d; DESCENT: %d; lb=%d; rb=%d\n", font->ascent, font->descent, font->lbearing, font->rbearing);

  if (monocheck) {
    // rough check for monospaced font
    const char *wcs[9] = {"A", "W", "I", "i", "0", "1", "-", "g", "b"};
    int wdt[9] = {0};

    for (unsigned f = 0; f < 9; ++f) {
      wdt[f] = XTextWidth(set, wcs[f], 1);
    }

    for (unsigned f = 0; f < 9; ++f) {
      for (unsigned c = 0; c < 9; ++c) {
        if (wdt[f] != wdt[c]) {
          k8t_die("X11: specified X font is not monospaced");
          return;
        }
      }
    }
  }
}
#endif


/*
static int isUTF8LocaleName (const char *lang) {
  if (!lang) return 0;
  while (*lang) {
    if ((lang[0]|0x20) == 'u' && (lang[1]|0x20) == 't' && (lang[2]|0x20) == 'f') return 1;
    ++lang;
  }
  return 0;
}
*/


static void initfonts (const char *fontstr, const char *bfontstr, const char *tabfont) {
#ifdef X11_USE_FUCKED_FONTSETS
  char *olocale;
  /* X Core Fonts fix */
  /* sorry, we HAVE to do this shit here!
   * braindamaged X11 will not work with utf-8 in Xmb*() correctly if we have, for example,
   * koi8 as system locale unless we first create font set in system locale. don't even ask
   * me why X.Org is so fuckin' broken. */
  XFontSet tmp_fs;
  char **missing;
  int missing_count;
  olocale = strdup(setlocale(LC_ALL, NULL)); /*FIXME: this can fail if we have no memory; fuck it*/
  setlocale(LC_ALL, "");
  tmp_fs = XCreateFontSet(xw.dpy, "-*-*-*-*-*-*-*-*-*-*-*-*-*", &missing, &missing_count, NULL);
  if (!tmp_fs) {
    fprintf(stderr, "FATAL: can't apply workarount for X Core FontSets!\n");
    exit(1);
  }
  if (missing) XFreeStringList(missing);
  /* throw out unused fontset */
  XFreeFontSet(xw.dpy, tmp_fs);
  //setlocale(LC_ALL, "ru_RU.utf-8");
  /*TODO: find suitable utf-8 encoding */
  if (!isUTF8LocaleName(setlocale(LC_ALL, NULL)) {
    //setlocale(LC_ALL, "en_US.UTF-8");
    if (!setlocale(LC_ALL, "en_US.UTF-8") && !setlocale(LC_ALL, "en_US.UTF8") && !setlocale(LC_ALL, "en_US.utf8")) {
      fprintf(stderr, "FATAL: can't set UTF locale!\n");
      exit(1);
    }
  }
  /* create fonts for utf-8 locale */
#endif
  if ((dc.font[0].set = xinitfont(fontstr)) == NULL) {
    if ((dc.font[0].set = xinitfont(FONT)) == NULL) k8t_die("can't load font '%s'", fontstr);
  }
  xgetfontinfo(dc.font[0].set, &dc.font[0].ascent, &dc.font[0].descent, &dc.font[0].width, &dc.font[0].fid, 1);
  if ((dc.font[1].set = xinitfont(bfontstr)) == NULL) {
    if ((dc.font[1].set = xinitfont(FONTBOLD)) == NULL) k8t_die("can't load font '%s'", bfontstr);
  }
  xgetfontinfo(dc.font[1].set, &dc.font[1].ascent, &dc.font[1].descent, &dc.font[1].width, &dc.font[1].fid, 1);
  if ((dc.font[2].set = xinitfont(tabfont)) == NULL) {
    if ((dc.font[2].set = xinitfont(FONTTAB)) == NULL) k8t_die("can't load font '%s'", tabfont);
  }
  xgetfontinfo(dc.font[2].set, &dc.font[2].ascent, &dc.font[2].descent, &dc.font[2].width, &dc.font[2].fid, 0);
  /* restore locale */
#ifdef X11_USE_FUCKED_FONTSETS
  setlocale(LC_ALL, olocale);
  free(olocale);
#endif
}


static void xinit (void) {
  XSetWindowAttributes attrs;
  Window parent;
  XColor blackcolor = { 0, 0, 0, 0, 0, 0 };
  int wwdt, whgt;
  //
  if (!(xw.dpy = XOpenDisplay(NULL))) k8t_die("can't open display");
  //
  XA_VT_SELECTION = XInternAtom(xw.dpy, "_K8STERM_SELECTION_PROP_", 0);
  XA_CLIPBOARD = XInternAtom(xw.dpy, "CLIPBOARD", 0);
  XA_UTF8 = XInternAtom(xw.dpy, "UTF8_STRING", 0);
  XA_NETWM_NAME = XInternAtom(xw.dpy, "_NET_WM_NAME", 0);
  XA_TARGETS = XInternAtom(xw.dpy, "TARGETS", 0);
  XA_WM_DELETE_WINDOW = XInternAtom(xw.dpy, "WM_DELETE_WINDOW", 0);
  XA_INCR = XInternAtom(xw.dpy, "INCR", 0);
  XA_CSTRING = XInternAtom(xw.dpy, "C_STRING", 0);
  xw.xembed = XInternAtom(xw.dpy, "_XEMBED", False);
  //
  xw.paste_term = NULL;
  //
  xw.scr = XDefaultScreen(xw.dpy);
  /* font */
  initfonts(opt_fontnorm, opt_fontbold, opt_fonttab);
  /* XXX: Assuming same size for bold font */
  xw.cw = dc.font[0].width;
  xw.ch = dc.font[0].ascent+dc.font[0].descent;
  xw.tch = dc.font[2].ascent+dc.font[2].descent;
  xw.tabheight = opt_disabletabs ? 0 : xw.tch+2;
  //xw.tabheight = 0;
  /* colors */
  xw.cmap = XDefaultColormap(xw.dpy, xw.scr);
  xloadcols();
  /* window - default size */
  /*
  K8T_DATA(curterm)->picbufh = curterm->row*xw.ch;
  K8T_DATA(curterm)->picbufw = curterm->col*xw.cw;
  */
  wwdt = curterm->col*xw.cw;
  whgt = curterm->row*xw.ch;
  //
  xw.h = whgt+xw.tabheight;
  xw.w = wwdt;
  //
  attrs.background_pixel = getColor(defaultBG);
  attrs.border_pixel = getColor(defaultBG);
  attrs.bit_gravity = NorthWestGravity;
  attrs.event_mask =
    FocusChangeMask|KeyPressMask|
    ExposureMask|VisibilityChangeMask|StructureNotifyMask|
    /*ButtonMotionMask|*/PointerMotionMask|ButtonPressMask|ButtonReleaseMask|
    EnterWindowMask|LeaveWindowMask|
    PropertyChangeMask; // for selection pasting
  attrs.colormap = xw.cmap;
  //fprintf(stderr, "oe: [%s]\n", opt_embed);
  parent = opt_embed ? (Window)strtol(opt_embed, NULL, 0) : XRootWindow(xw.dpy, xw.scr);
  xw.win = XCreateWindow(xw.dpy, parent, 0, 0,
      xw.w, xw.h, 0, XDefaultDepth(xw.dpy, xw.scr), InputOutput,
      XDefaultVisual(xw.dpy, xw.scr),
      CWBackPixel|CWBorderPixel|CWBitGravity|CWEventMask|CWColormap,
      &attrs);
  xhints();
  //
  xw.pictab = XCreatePixmap(xw.dpy, xw.win, xw.w, xw.tabheight>0?xw.tabheight:1, XDefaultDepth(xw.dpy, xw.scr));
  /* input methods */
  if ((xw.xim = XOpenIM(xw.dpy, NULL, NULL, NULL)) == NULL) k8t_die("XOpenIM() failed");
  xw.xic = XCreateIC(xw.xim, XNInputStyle, XIMPreeditNothing|XIMStatusNothing, XNClientWindow, xw.win, XNFocusWindow, xw.win, NULL);
  /* gc */
  dc.gc = XCreateGC(xw.dpy, xw.win, 0, NULL);
#ifdef X11_USE_FUCKED_FONTSETS
  // nothing
#else
  XSetFont(xw.dpy, dc.gc, dc.font[0].fid);
  dc.gcfid = dc.font[0].fid;
#endif
  //
  xw.lastcursor = xw.cursor = XCreateFontCursor(xw.dpy, XC_xterm);
  //xw.cursor = XCreateFontCursor(xw.dpy, XC_arrow);
  /* green cursor, black outline */
  XRecolorCursor(xw.dpy, xw.cursor,
    &(XColor){.red = 0x0000, .green = 0xffff, .blue = 0x0000},
    &(XColor){.red = 0x0000, .green = 0x0000, .blue = 0x0000});
  //
  xw.defcursor = XCreateFontCursor(xw.dpy, /*XC_X_cursor*/XC_left_ptr);
  XRecolorCursor(xw.dpy, xw.defcursor,
    &(XColor){.red = 0x0000, .green = 0xffff, .blue = 0x0000},
    &(XColor){.red = 0x0000, .green = 0x0000, .blue = 0x0000});
  //
  XDefineCursor(xw.dpy, xw.win, xw.lastcursor);
  fixWindowTitle(curterm);
  //XStoreName(xw.dpy, xw.win, opt_title);
  //
  XSetForeground(xw.dpy, dc.gc, 0);
  if (xw.tabheight > 0) XFillRectangle(xw.dpy, xw.pictab, dc.gc, 0, 0, xw.w, xw.tabheight);
  //
  xw.picscrhere = 0;
  termCreateXPixmap(curterm);
  //
  XMapWindow(xw.dpy, xw.win);
  //
#if BLANKPTR_USE_GLYPH_CURSOR
  xw.blankPtr = XCreateGlyphCursor(xw.dpy, dc.font[0].fid, dc.font[0].fid, ' ', ' ', &blackcolor, &blackcolor);
#else
  static const char cmbmp[1] = {0};
  Pixmap pm;
  //
  pm = XCreateBitmapFromData(xw.dpy, xw.win, cmbmp, 1, 1);
  xw.blankPtr = XCreatePixmapCursor(xw.dpy, pm, pm, &blackcolor, &blackcolor, 0, 0);
  XFreePixmap(xw.dpy, pm);
#endif
  //
  XSync(xw.dpy, 0);
}


void xdone (void) {
  if (xw.xim) XCloseIM(xw.xim);
  if (xw.dpy) XCloseDisplay(xw.dpy);
}
