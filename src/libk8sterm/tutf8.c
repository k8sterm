#include "k8sterm.h"


////////////////////////////////////////////////////////////////////////////////
// UTF-8

static const unsigned char utf8Length[256] = {
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, //0x00-0x0f
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, //0x10-0x1f
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, //0x20-0x2f
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, //0x30-0x3f
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, //0x40-0x4f
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, //0x50-0x5f
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, //0x60-0x6f
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, //0x70-0x7f
  9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9, //0x80-0x8f
  9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9, //0x90-0x9f
  9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9, //0xa0-0xaf
  9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9, //0xb0-0xbf
  2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2, //0xc0-0xcf  c0-c1: overlong encoding: start of a 2-byte sequence, but code point <= 127
  2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2, //0xd0-0xdf
  3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3, //0xe0-0xef
  4,4,4,4,4,8,8,8,8,8,8,8,8,8,8,8  //0xf0-0xff
};


// decode one utf-8 char from *buf to *u, return char length; return '?' on error
K8TERM_API int k8t_UTF8Decode (uint32_t *u, const void *buf) {
  const unsigned char *data = (const unsigned char *)buf;
  unsigned char len = utf8Length[*data];
  uint32_t uc;
  //
  switch (len) {
    case 0: // ascii
      *u = *data;
      return 1;
    case 8: case 9: // invalid
      *u = '?';
      return 1;
  }
  // utf-8
  uc = (*data++)&(0x7c>>len);
  while (--len) {
    if (utf8Length[*data] != 9) { uc = 0xffff; break; }
    uc = (uc<<6)|((*data++)&0x3f);
  }
  if (uc > 0x10ffff) uc &= 0x1fffff;
  if ((uc >= 0xd800 && uc <= 0xdfff) || // utf16/utf32 surrogates
      (uc >= 0xfdd0 && uc <= 0xfdef) || // just for fun
      (uc >= 0xfffe && uc <= 0xffff)) uc = '?'; // bad unicode
  *u = uc;
  return data-((const unsigned char *)buf);
}


// encode one utf-8 char from u to *buf, return char length
K8TERM_API int k8t_UTF8Encode (void *buf, uint32_t uc) {
  uint8_t *sp = (uint8_t *)buf;
  int n;
  //
  if (uc < 0x80) { *sp = uc; return 1; } /* 0xxxxxxx */
  uc &= 0x1fffff;
  if (uc < 0x800) {
    /* 110xxxxx */
    *sp++ = (uc>>6)|0xc0;
    n = 1;
  } else if (uc < 0x10000) {
    /* 1110xxxx */
    *sp++ = (uc>>12)|0xe0;
    n = 2;
  } else if (uc <= 0x10FFFF) {
    /* 11110xxx */
    *sp++ = (uc>>18)|0xf0;
    n = 3;
  } else {
    /* U+FFFD */
    memcpy(sp, "\xEF\xBF\xBD", 3);
    return 3;
  }
  for (int f = n; f > 0; --f) *sp++ = ((uc>>(6*(f-1)))&0x3f)|0x80; /* 10xxxxxx */
  return n+1;
}


/* use this if your buffer is less than K8T_UTF_SIZ, it returns 1 if you can decode
   UTF-8 otherwise return 0 */
K8TERM_API int k8t_UTF8IsFull (const void *buf, int buflen) {
  if (buflen > 0) {
    const unsigned char *data = (const unsigned char *)buf;
    unsigned char len = utf8Length[*data++];
    int res;
    //
    switch (len) {
      case 0: case 8: case 9: return 1;
    }
    if ((res = (buflen >= len))) buflen = len;
    for (int f = buflen-1; f > 0; --f) if (((*data++)&0xc0) != 0x80) return 1;
    return res;
  }
  return 0;
}


K8TERM_API int k8t_UTF8Size (const void *buf) {
  const unsigned char *data = (const unsigned char *)buf;
  unsigned char len = utf8Length[*data];
  //
  switch (len) {
    case 0: return 1;
    case 8: case 9: return 0;
  }
  return len;
}


K8TERM_API int k8t_UTF8strlen (const char *s) {
  int len = 0;
  //
  while (*s) {
    if (((unsigned char)(s[0])&0xc0) == 0xc0 || ((unsigned char)(s[0])&0x80) == 0) ++len;
    ++s;
  }
  return len;
}


K8TERM_API void k8t_UTF8ChopLast (char *s) {
  int lastpos = 0;
  //
  for (char *t = s; *t; ++t) {
    if (((unsigned char)(t[0])&0xc0) == 0xc0 || ((unsigned char)(t[0])&0x80) == 0) lastpos = (int)(t-s);
  }
  s[lastpos] = 0;
}
