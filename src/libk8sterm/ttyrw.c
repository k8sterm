#include "k8sterm.h"


////////////////////////////////////////////////////////////////////////////////
// tty r/w
static int k8t_ttyCanReadEx (K8Term *term, int msecto) {
  for (;;) {
    fd_set rfd;
    struct timeval timeout = {0};
    //
    if (term->dead || term->cmdfd < 0) return 0;
    FD_ZERO(&rfd);
    FD_SET(term->cmdfd, &rfd);
    if (msecto > 0) timeout.tv_usec = msecto*1000;
    if (select(term->cmdfd+1, &rfd, NULL, NULL, (msecto >= 0 ? &timeout : NULL)) < 0) {
      if (errno == EINTR) continue;
      k8t_die("select failed: %s", strerror(errno));
    }
    if (FD_ISSET(term->cmdfd, &rfd)) return 1;
    break;
  }
  return 0;
}


K8TERM_API int k8t_ttyCanRead (K8Term *term) {
  return k8t_ttyCanReadEx(term, 0);
}


K8TERM_API int k8t_ttyCanWrite (K8Term *term) {
  for (;;) {
    fd_set wfd;
    struct timeval timeout = {0};
    //
    if (term->dead || term->cmdfd < 0) return 0;
    FD_ZERO(&wfd);
    FD_SET(term->cmdfd, &wfd);
    if (select(term->cmdfd+1, NULL, &wfd, NULL, &timeout) < 0) {
      if (errno == EINTR) continue;
      k8t_die("select failed: %s", strerror(errno));
    }
    if (FD_ISSET(term->cmdfd, &wfd)) return 1;
    break;
  }
  return 0;
}


K8TERM_API void k8t_tmFeed (K8Term *term, char *buffer, int bufLen) {
  char *ptr;
  int left;
  int res = 0;
  //
  /* append read bytes to unprocessed bytes */
  if (term == NULL || term->dead || term->cmdfd < 0) return;
  while (bufLen>0) {
    left = K8T_OBUFSIZ-term->obuflen;
    while (left > 0) {
      int ret;
      //
      //if ((ret = read(term->cmdfd, term->obuf+term->obuflen, 1)) < 0) k8t_die("Couldn't read from shell: %s", strerror(errno));
      ret = (bufLen > left) ? left : bufLen;
      memcpy(term->obuf+term->obuflen, buffer, ret);
      buffer += ret;
      bufLen -= ret;
      //if ((ret = read(term->cmdfd, term->obuf+term->obuflen, left)) < 0) {
      //
      //    //fprintf(stderr, "Warning: couldn't read from tty: %s\n", strerror(errno));
      //    if (errno == EINTR) continue;
      //    res = -1;
      //    break;
      //}
      //
      if (ret == 0) break;
      //fprintf(stderr, "k8t_ttyRead: got %d bytes\n", ret);
      term->obuflen += ret;
      left -= ret;
    }
    //fprintf(stderr, "1: k8t_ttyRead after: free=%d, used=%d\n", left, term->obuflen);
    if (term->obuflen == 0) return;
    /* process every complete utf8 char */
    ptr = term->obuf;
    if (term->needConv) {
      // need conversion from locale to utf-8
      //fprintf(stderr, "buf: %d bytes\n", term->obuflen);
      while (term->obuflen > 0) {
        char obuf[K8T_UTF_SIZ+1];
        int len;
        //
        //fprintf(stderr, "'%c' (%d)\n", (*ptr >= ' ' && *ptr != 127 ? *ptr : '.'), *ptr);
        len = term->loc2utf(term, obuf, ptr, 1);
        if (len > 0) {
          obuf[len] = 0;
          k8t_tmPutC(term, obuf);
        }
        ++ptr;
        --term->obuflen;
      }
      term->obuflen = 0;
    } else {
      // don't do any conversion
      while (term->obuflen >= K8T_UTF_SIZ || k8t_UTF8IsFull(ptr, term->obuflen)) {
        uint32_t utf8c;
        char s[K8T_UTF_SIZ+1];
        int charsize = k8t_UTF8Decode(&utf8c, ptr);
        int len;
        //
        len = k8t_UTF8Encode(s, utf8c);
        if (len > 0) {
          s[len] = 0;
          k8t_tmPutC(term, s);
        }
        ptr += charsize;
        term->obuflen -= charsize;
      }
      if (res < 0) term->obuflen = 0; // there was read error, we should not expect more bytes
      //dlogf("2: k8t_ttyRead afterproc: used=%d", term->obuflen);
    }
    /* keep any uncomplete utf8 char for the next call */
    if (term->obuflen > 0) {
      res = 0;
      memmove(term->obuf, ptr, term->obuflen);
    }
  }
}


// return -1 if there was read error, 0 otherwise
K8TERM_API int k8t_ttyRead (K8Term *term) {
  char *ptr;
  int left;
  int res = 0;
  //
  /* append read bytes to unprocessed bytes */
  if (term == NULL || term->dead || term->cmdfd < 0) return -1;
  left = K8T_OBUFSIZ-term->obuflen;
  //fprintf(stderr, "0: k8t_ttyRead before: free=%d, used=%d\n", left, term->obuflen);
  while (left > 0 && k8t_ttyCanRead(term)) {
    int ret;
    //
    //if ((ret = read(term->cmdfd, term->obuf+term->obuflen, 1)) < 0) k8t_die("Couldn't read from shell: %s", strerror(errno));
    if ((ret = read(term->cmdfd, term->obuf+term->obuflen, left)) < 0) {
      //fprintf(stderr, "Warning: couldn't read from tty: %s\n", strerror(errno));
      if (errno == EINTR) continue;
      res = -1;
      break;
    }
    //
    if (ret == 0) break;
    //fprintf(stderr, "k8t_ttyRead: got %d bytes\n", ret);
    term->obuflen += ret;
    left -= ret;
  }
  //fprintf(stderr, "1: k8t_ttyRead after: free=%d, used=%d\n", left, term->obuflen);
  if (term->obuflen == 0) return res;
  /* process every complete utf8 char */
  ptr = term->obuf;
  if (term->needConv) {
    // need conversion from locale to utf-8
    //fprintf(stderr, "buf: %d bytes\n", term->obuflen);
    while (term->obuflen > 0) {
      char obuf[K8T_UTF_SIZ+1];
      int len;
      //
      //fprintf(stderr, "'%c' (%d)\n", (*ptr >= ' ' && *ptr != 127 ? *ptr : '.'), *ptr);
      len = term->loc2utf(term, obuf, ptr, 1);
      if (len > 0) {
        obuf[len] = 0;
        k8t_tmPutC(term, obuf);
      }
      ++ptr;
      --term->obuflen;
    }
    term->obuflen = 0;
  } else {
    // don't do any conversion
    while (term->obuflen >= K8T_UTF_SIZ || k8t_UTF8IsFull(ptr, term->obuflen)) {
      uint32_t utf8c;
      char s[K8T_UTF_SIZ+1];
      int charsize = k8t_UTF8Decode(&utf8c, ptr);
      int len;
      //
      len = k8t_UTF8Encode(s, utf8c);
      if (len > 0) {
        s[len] = 0;
        k8t_tmPutC(term, s);
      }
      ptr += charsize;
      term->obuflen -= charsize;
    }
    if (res < 0) term->obuflen = 0; // there was read error, we should not expect more bytes
    //dlogf("2: k8t_ttyRead afterproc: used=%d", term->obuflen);
  }
  /* keep any uncomplete utf8 char for the next call */
  if (term->obuflen > 0) {
    res = 0;
    memmove(term->obuf, ptr, term->obuflen);
  }
  //
  return res;
}


//TODO: remove idiotic copypaste, merge k8t_ttySendWriteBuf() and k8t_ttyFlushWriteBuf()
K8TERM_API int k8t_ttySendWriteBuf (K8Term *term) {
  int sent = 0;
  //
  if (term == NULL || term->dead || term->cmdfd < 0) return -1;
  if (term->wrbufpos >= term->wrbufused) {
    term->wrbufpos = term->wrbufused = 0;
    return 0;
  }
  //
  //dlogf("0: k8t_ttyFlushWriteBuf before: towrite=%d", term->wrbufused-term->wrbufpos);
  while (term->wrbufpos < term->wrbufused && k8t_ttyCanWrite(term)) {
    int ret;
    //
    if ((ret = write(term->cmdfd, term->wrbuf+term->wrbufpos, term->wrbufused-term->wrbufpos)) < 0) {
      //fprintf(stderr, "Warning: write error on tty #%d: %s\n", termidx, strerror(errno));
      if (errno == EINTR) continue;
      break;
    }
    if (ret == 0) break;
    sent += ret;
    term->wrbufpos += ret;
  }
  //
  if (term->wrbufpos > 0) {
    int left = term->wrbufused-term->wrbufpos;
    //
    if (left < 1) {
      // write buffer is empty
      term->wrbufpos = term->wrbufused = 0;
    } else {
      memmove(term->wrbuf, term->wrbuf+term->wrbufpos, left);
      term->wrbufpos = 0;
      term->wrbufused = left;
    }
  }
  //dlogf("1: k8t_ttyFlushWriteBuf after: towrite=%d", term->wrbufused-term->wrbufpos);
  //
  return sent;
}


K8TERM_API void k8t_ttyFlushWriteBuf (K8Term *term) {
  if (term == NULL || term->dead || term->cmdfd < 0) return;
  if (term->wrbufpos >= term->wrbufused) {
    term->wrbufpos = term->wrbufused = 0;
    return;
  }
  //
/*
  if (term->wrbufpos < term->wrbufused && term->doWriteBufferFlush != NULL) {
    while (term->wrbufpos < term->wrbufused) {
      int wrt = term->doWriteBufferFlush(term, term->wrbuf+term->wrbufpos, term->wrbufused-term->wrbufpos);
      //
      if (wrt < 0) goto done;
      if (wrt == 0) break;
      term->wrbufpos += ret;
    }
  }
*/
  //dlogf("0: k8t_ttyFlushWriteBuf before: towrite=%d", term->wrbufused-term->wrbufpos);
  while (term->wrbufpos < term->wrbufused) {
    int ret;
    //
    if (!k8t_ttyCanWrite(term)) {
      if (term->wrbufused >= term->wrbufsize-8) ret = 0; else break;
    } else {
      if ((ret = write(term->cmdfd, term->wrbuf+term->wrbufpos, term->wrbufused-term->wrbufpos)) < 0) {
        //fprintf(stderr, "Warning: write error on tty #%d: %s\n", termidx, strerror(errno));
        if (errno == EINTR) continue;
        break;
      }
    }
    //
    if (ret == 0) {
      int cr = k8t_ttyCanReadEx(term, 100);
      //
      //fprintf(stderr, "k8t_ttyCanReadEx: %d\n", cr);
      if (cr < 0) break;
      if (cr > 0) k8t_ttyRead(term);
      continue;
    }
    //
    term->wrbufpos += ret;
  }
  //
//done:
  if (term->wrbufpos > 0) {
    int left = term->wrbufused-term->wrbufpos;
    //
    if (left < 1) {
      // write buffer is empty
      term->wrbufpos = term->wrbufused = 0;
    } else {
      memmove(term->wrbuf, term->wrbuf+term->wrbufpos, left);
      term->wrbufpos = 0;
      term->wrbufused = left;
    }
  }
  //dlogf("1: k8t_ttyFlushWriteBuf after: towrite=%d", term->wrbufused-term->wrbufpos);
}


// convert char to locale and write it
// return<0: error; ==0: ok
K8TERM_API int k8t_ttyWriteRawChar (K8Term *term, const char *s, int len, int noenc) {
  char loc[16];
  int clen;
  int rdtries = 100;
  //
  if (s == NULL || len < 1) return 0;
  if (!noenc) {
    if (term->needConv) {
      if ((clen = term->utf2loc(term, loc, s, len)) < 1) return 0;
    } else {
      if ((clen = k8t_UTF8Size(s)) < 1) return 0;
      memmove(loc, s, clen);
    }
  } else {
    memmove(loc, s, (clen = len));
  }
  //
  while (term->wrbufused+clen >= term->wrbufsize) {
    //FIXME: make write buffer dynamic?
    int res;
    //
    // force write at least one char
    //dlogf("k8t_ttyWrite: forced write");
    if ((res = write(term->cmdfd, term->wrbuf+term->wrbufpos, 1)) < 0) {
      //fprintf(stderr, "Warning: write error on tty #%d: %s\n", termidx, strerror(errno));
      if (errno == EINTR) continue;
      return -1; //SHIT!
    }
    if (res == 0) {
      if (--rdtries > 0) {
        int cr = k8t_ttyCanReadEx(term, 100);
        //
        //fprintf(stderr, "(%d) k8t_ttyCanReadEx: %d\n", rdtries, cr);
        if (cr > 0) k8t_ttyRead(term);
        if (cr >= 0) continue;
      }
      //
      return -1; //SHIT!
    }
    ++term->wrbufpos;
    k8t_ttyFlushWriteBuf(term); // make room for char
  }
  memcpy(term->wrbuf+term->wrbufused, loc, clen);
  term->wrbufused += clen;
  //
  return 0;
}


K8TERM_API void k8t_ttyWriteNoEnc (K8Term *term, const char *s, size_t n) {
  if (n > 0) {
    term->ubufpos = 0; // discard possible utf-8 char
    while (n-- > 0) k8t_ttyWriteRawChar(term, s++, 1, 1);
  }
  k8t_ttyFlushWriteBuf(term);
}


K8TERM_API void k8t_ttyWrite (K8Term *term, const char *s, size_t n) {
  if (term == NULL || term->dead || term->cmdfd < 0) return;
  //k8t_ttyFlushWriteBuf();
  if (s != NULL && n > 0) {
    while (n > 0) {
      unsigned char c = (unsigned char)(s[0]);
      //
      if (term->ubufpos > 0 && k8t_UTF8IsFull(term->ubuf, term->ubufpos)) {
        // have complete char
        k8t_ttyWriteRawChar(term, term->ubuf, term->ubufpos, 0);
        term->ubufpos = 0;
        continue;
      }
      //
      if (term->ubufpos == 0) {
        // new char
        if (c < 128) {
          k8t_ttyWriteRawChar(term, s, 1, 0);
        } else if ((c&0xc0) == 0xc0) {
          // new utf-8 char
          term->ubuf[term->ubufpos++] = *s;
        } else {
          // ignore unsynced utf-8
        }
        ++s;
        --n;
        continue;
      }
      // char continues
      if (c < 128 || term->ubufpos >= K8T_UTF_SIZ || (c&0xc0) == 0xc0) {
        // discard previous utf-8, it's bad
        term->ubufpos = 0;
        continue;
      }
      // collect
      term->ubuf[term->ubufpos++] = *s;
      ++s;
      --n;
      if (k8t_UTF8IsFull(term->ubuf, term->ubufpos)) {
        // have complete char
        k8t_ttyWriteRawChar(term, term->ubuf, term->ubufpos, 0);
        term->ubufpos = 0;
      }
    }
  }
  k8t_ttyFlushWriteBuf(term);
}
