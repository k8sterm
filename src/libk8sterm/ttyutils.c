#include "k8sterm.h"


////////////////////////////////////////////////////////////////////////////////
K8TERM_API void k8t_tmWantRedraw (K8Term *term, int forceFast) {
  if (term != NULL) {
    term->wantRedraw = 1;
    if (forceFast) term->lastDrawTime = 0;
    else if (!term->fastredraw) term->lastDrawTime = term->clockTicks(term); //FIXME: avoid excess redraw?
  }
}


K8TERM_API void k8t_tmDirtyMark (K8Term *term, int lineno, int flag) {
  if (term != NULL && lineno >= 0 && lineno < term->row) {
    term->dirty[lineno] |= flag;
    k8t_tmWantRedraw(term, 0);
  }
}


K8TERM_API void k8t_tmDirty (K8Term *term, int top, int bot) {
  K8T_LIMIT(top, 0, term->row-1);
  K8T_LIMIT(bot, 0, term->row-1);
  for (int y = top; y <= bot; ++y) k8t_tmDirtyMark(term, y, 2);
}


K8TERM_API void k8t_tmFullDirty (K8Term *term) {
  k8t_tmDirty(term, 0, term->row-1);
}


K8TERM_API void k8t_tmMoveTo (K8Term *term, int x, int y) {
  K8T_LIMIT(x, 0, term->col-1);
  K8T_LIMIT(y, 0, term->row-1);
  term->c.state &= ~K8T_CURSOR_WRAPNEXT;
  if (term->c.x != x || term->c.y != y) {
    term->c.x = x;
    term->c.y = y;
    k8t_tmWantRedraw(term, 0);
  }
}


// 0: only chars
// 1: to default colors
// 2: to first char color
K8TERM_API void k8t_tmClearRegionEx (K8Term *term, int x1, int y1, int x2, int y2) {
  if (term->clearOnPartialScrollMode >= 0) {
    int temp;
    //fprintf(stderr, "k8t_tmClearRegion: (%d,%d)-(%d,%d)\n", x1, y1, x2, y2);
    if (x1 > x2) { temp = x1; x1 = x2; x2 = temp; }
    if (y1 > y2) { temp = y1; y1 = y2; y2 = temp; }
    K8T_LIMIT(x1, 0, term->col-1);
    K8T_LIMIT(x2, 0, term->col-1);
    K8T_LIMIT(y1, 0, term->row-1);
    K8T_LIMIT(y2, 0, term->row-1);
    if (x1 > x2) return; // unnecessary
    //fprintf(stderr, " k8t_tmClearRegion: (%d,%d)-(%d,%d)\n", x1, y1, x2, y2);
    for (int y = y1; y <= y2; ++y) {
      K8TLine l = term->line[y];
      K8TGlyph g = (term->clearOnPartialScrollMode == 1 ? term->c.attr : l[x1]);
      k8t_tmDirtyMark(term, y, (x1 <= 0 && x2 >= term->col-1) ? 2 : 1);
      for (int x = x1; x <= x2; ++x) {
        if (term->clearOnPartialScrollMode > 0) {
          l[x].fg = g.fg;
          l[x].bg = g.bg;
          l[x].attr = K8T_ATTR_NORMAL|(g.attr&(K8T_ATTR_FGSET|K8T_ATTR_BGSET));
        } else {
          l[x].attr = K8T_ATTR_NORMAL|(l[x].attr&(K8T_ATTR_FGSET|K8T_ATTR_BGSET));
        }
        l[x].state = K8T_GLYPH_DIRTY;
        l[x].c[0] = ' ';
        if (term->sel.bx != -1 && k8t_isSelected(term, x, y)) k8t_selHide(term);
      }
      l[term->col-1].state &= ~K8T_GLYPH_WRAP;
    }
  }
}


K8TERM_API void k8t_tmClearRegion (K8Term *term, int x1, int y1, int x2, int y2) {
  int ocl = term->clearOnPartialScrollMode;
  term->clearOnPartialScrollMode = 1;
  k8t_tmClearRegionEx(term, x1, y1, x2, y2);
  term->clearOnPartialScrollMode = ocl;
}


K8TERM_API void k8t_tmCursor (K8Term *term, int mode) {
  if (mode == K8T_CURSOR_SAVE) {
    term->csaved = term->c;
  } else if (mode == K8T_CURSOR_LOAD) {
    term->c = term->csaved;
    k8t_tmMoveTo(term, term->c.x, term->c.y);
    k8t_tmWantRedraw(term, 0);
  }
}


K8TERM_API void k8t_tmAdjMaxHistory (K8Term *term, int maxh) {
  if (term != NULL) {
    K8T_LIMIT(maxh, 0, 65535);
    if (term->maxhistory < maxh) {
      K8TLine *nl;
      int newlc = term->linecount+(maxh-term->maxhistory);
      //
      // add history lines
      if ((nl = realloc(term->line, sizeof(K8TLine)*newlc)) != NULL) {
        K8TGlyph g;
        //
        term->topline = 0;
        term->line = nl;
        g.state = K8T_GLYPH_DIRTY;
        g.attr = K8T_ATTR_NORMAL;
        g.fg = term->deffg;
        g.bg = term->defbg;
        g.c[0] = ' ';
        g.c[1] = 0;
        for (int y = term->linecount; y < newlc; ++y) {
          term->line[y] = calloc(term->col, sizeof(K8TGlyph));
          for (int x = 0; x < term->col; ++x) term->line[y][x] = g;
        }
        term->maxhistory = maxh;
        term->linecount = newlc;
      }
    } else if (term->maxhistory > maxh) {
      K8TLine *nl;
      //
      term->topline = 0;
      // remove history lines
      while (term->linecount > term->row+maxh) free(term->line[--term->linecount]);
      if ((nl = realloc(term->line, sizeof(K8TLine)*term->linecount)) != NULL) term->line = nl;
      term->maxhistory = maxh;
    }
    if (term->historyLinesUsed > term->linecount-term->row) term->historyLinesUsed = term->linecount-term->row;
  }
}


K8TERM_API void k8t_tmSwapScreen (K8Term *term) {
  k8t_selHide(term);
  k8t_tmDoWrap(term);
  for (int f = 0; f < term->row; ++f) {
    K8TLine t = term->line[f];
    term->line[f] = term->alt[f];
    term->alt[f] = t;
  }
  term->mode ^= K8T_MODE_ALTSCREEN;
  k8t_tmFullDirty(term);
  //term->justSwapped = 1;
  k8t_tmWantRedraw(term, 0); //FIXME: we need new option for 'instant redraw'
  if (term->afterScreenSwap != NULL) term->afterScreenSwap(term);
}


//FIXME: works bad with history
//FIXME: ugly code
K8TERM_API void k8t_tmScrollSelection (K8Term *term, int orig, int n, int tohistory) {
  int docopy = 0;
  //
  if (term->sel.bx == -1) return;
  //
  k8t_tmFullDirty(term); // just in case
  if (!tohistory) {
    if (K8T_BETWEEN(term->sel.by, orig, term->bot) || K8T_BETWEEN(term->sel.ey, orig, term->bot)) {
      if ((term->sel.by += n) > term->bot || (term->sel.ey += n) < term->top) {
        k8t_selClear(term);
        return;
      }
      if (term->sel.by < term->top) {
        term->sel.by = term->top;
        term->sel.bx = 0;
        docopy = 1;
      }
      if (term->sel.ey > term->bot) {
        term->sel.ey = term->bot;
        term->sel.ex = term->col;
        docopy = 1;
      }
      term->sel.b.x = term->sel.bx;
      term->sel.b.y = term->sel.by;
      term->sel.e.x = term->sel.ex;
      term->sel.e.y = term->sel.ey;
    }
  } else {
    // tohistory!=0; always scrolls full screen up (n == -1)
    //fprintf(stderr, "k8t_tmScrollSelection to history\n");
    term->sel.by += n;
    term->sel.ey += n;
    //fprintf(stderr, " by=%d; ey=%d; maxhistory=%d\n", term->sel.by, term->sel.ey, term->maxhistory);
    if (term->sel.ey < 0 && -(term->sel.ey) > term->maxhistory) {
      // out of screen completely
      k8t_selClear(term);
      return;
    }
    if (term->sel.by < 0 && -(term->sel.by) > term->maxhistory) {
      term->sel.by = -term->maxhistory;
      term->sel.bx = 0;
      docopy = 1;
    }
    term->sel.b.x = term->sel.bx;
    term->sel.b.y = term->sel.by;
    term->sel.e.x = term->sel.ex;
    term->sel.e.y = term->sel.ey;
  }
  //
  if (docopy) k8t_selCopy(term);
}


K8TERM_API void k8t_tmScrollDown (K8Term *term, int orig, int n) {
  K8T_LIMIT(n, 0, term->bot-orig+1);
  if (n < 1) return;
  k8t_tmScrollSelection(term, orig, n, 0);
  //fprintf(stderr, "k8t_tmScrollDown(%d, %d)\n", orig, n);
  // clear bottom lines (they will be copied up)
  k8t_tmClearRegionEx(term, 0, term->bot-n+1, term->col-1, term->bot);
  for (int f = term->bot; f >= orig+n; --f) {
    K8TLine temp = term->line[f];
    term->line[f] = term->line[f-n];
    term->line[f-n] = temp;
    //k8t_tmDirtyMark(term, f, 2);
    //k8t_tmDirtyMark(term, f-n, 2);
  }
  //k8t_tmClearRegionEx(term, 0, orig, term->col-1, orig+n-1);
  k8t_tmDirty(term, orig, term->bot);
  //k8t_tmClearRegionEx(term, 0, orig, term->col-1, orig+n-1);
}


K8TERM_API void k8t_tmScrollUp (K8Term *term, int orig, int n, int tohistory) {
  if (term == NULL) return;
  K8T_LIMIT(n, 0, term->bot-orig+1);
  if (n < 1) return;
  //fprintf(stderr, "k8t_tmScrollUp(%d, %d)\n", orig, n);
  if (tohistory && !K8T_ISSET(term, K8T_MODE_ALTSCREEN) && term->maxhistory > 0) {
    K8TLine l = term->line[term->linecount-1];
    //
    for (int f = term->linecount-1; f > term->row; --f) term->line[f] = term->line[f-1];
    term->line[term->row] = l;
    for (int x = 0; x < term->col; ++x) l[x] = term->line[0][x];
  } else {
    tohistory = 0;
  }
  //
  k8t_tmScrollSelection(term, orig, -n, tohistory);
  //k8t_tmClearRegion(0, orig, term->col-1, orig+n-1);
  for (int f = orig; f <= term->bot-n; ++f) {
    K8TLine temp = term->line[f];
    term->line[f] = term->line[f+n];
    term->line[f+n] = temp;
    //k8t_tmDirtyMark(term, f, 2);
    //k8t_tmDirtyMark(term, f+n, 2);
  }
  k8t_tmClearRegionEx(term, 0, term->bot-n+1, term->col-1, term->bot);
  k8t_tmDirty(term, orig, term->bot);
  //FIXME: we need new option for 'instant redraw'
  //k8t_tmWantRedraw(term, 0); //redraw it NOW, so terminal will not seat silent on fast output
}


K8TERM_API void k8t_tmCharWrap (K8Term *term, int y, int wrap) {
  if (y >= 0 && y < term->row) {
    if (wrap) term->line[y][term->col-1].state |= K8T_GLYPH_WRAP;
    else term->line[y][term->col-1].state &= ~K8T_GLYPH_WRAP;
  }
}


K8TERM_API void k8t_tmNewLine (K8Term *term, int first_col) {
  int y = term->c.y;
  //
  k8t_tmCharWrap(term, y, (first_col == 2)); // 2: wrapping
  if (y == term->bot) {
    if (!K8T_ISSET(term, K8T_MODE_ALTSCREEN)) {
      if (++term->historyLinesUsed > term->linecount-term->row) term->historyLinesUsed = term->linecount-term->row;
      if (term->addHistoryLine != NULL) term->addHistoryLine(term); // have callback and not alternate screen
    }
    k8t_tmScrollUp(term, term->top, 1, 1);
  } else {
    ++y;
  }
  k8t_tmMoveTo(term, (first_col ? 0 : term->c.x), y);
  if (term->afterNewLine != NULL) term->afterNewLine(term);
}


K8TERM_API void k8t_tmSetChar (K8Term *term, const char *c) {
  char ub[K8T_UTF_SIZ];
  int rev = 0, gfx = 0;
  int x = term->c.x, y = term->c.y;
  //
  if (x < 0 || x >= term->col || y < 0 || y >= term->row) return;
  //
  if (!term->needConv && term->unimap != NULL && (unsigned char)c[0] >= 0x80) {
    uint32_t cc;
    //
    k8t_UTF8Decode(&cc, c);
    if (cc <= 65535) {
      uint16_t uc = term->unimap[cc];
      //
      //fprintf(stderr, "unimap: %d [%02X] 0x%04x -> 0x%04x\n", (unsigned char)c[0], cc, uc);
      if (uc) {
        if (uc == 127) {
          // inversed space
          rev = 1;
          ub[0] = ' ';
        } else {
          if (uc&0x8000) {
            ub[0] = (uc&0x7f);
            gfx = 1;
          } else {
            k8t_UTF8Encode(ub, uc);
          }
        }
        c = ub;
      }
    }
  }
  k8t_tmDirtyMark(term, y, 1);
  //
  term->line[y][x] = term->c.attr;
  if (rev) term->line[y][x].attr ^= K8T_ATTR_REVERSE;
  if (gfx || (term->mode&term->charset)) {
    term->line[y][x].attr |= K8T_ATTR_GFX;
  } else {
    term->line[y][x].attr &= ~K8T_ATTR_GFX;
  }
  //
  term->line[y][x].state = (K8T_GLYPH_SET|K8T_GLYPH_DIRTY);
  memcpy(term->line[y][x].c, c, K8T_UTF_SIZ);
  //
  if (K8T_ISGFX(term->line[y][x].attr)) {
    unsigned char c = (unsigned char)(term->line[y][x].c[0]);
    //
    if (c > 95 && c < 128) term->line[y][x].c[0] -= 95;
    else if (c > 127) term->line[y][x].c[0] = ' ';
  }
  if (term->sel.bx != -1 && k8t_isSelected(term, x, y)) k8t_selHide(term);
  //dlogf("k8t_tmSetChar(%d,%d): [%c] (%d); dirty=%d\n", x, y, c[0], term->wantRedraw, term->dirty[y]);
}


K8TERM_API void k8t_tmDeleteChar (K8Term *term, int n) {
  int src = term->c.x+n;
  int dst = term->c.x;
  int size = term->col-src;
  //
  k8t_tmDirtyMark(term, term->c.y, 2);
  if (src >= term->col) {
    k8t_tmClearRegion(term, term->c.x, term->c.y, term->col-1, term->c.y);
  } else {
    memmove(&term->line[term->c.y][dst], &term->line[term->c.y][src], size*sizeof(K8TGlyph));
    k8t_tmClearRegion(term, term->col-n, term->c.y, term->col-1, term->c.y);
  }
}


K8TERM_API void k8t_tmInsertBlank (K8Term *term, int n) {
  int src = term->c.x;
  int dst = src+n;
  int size = term->col-dst;
  //
  k8t_tmDirtyMark(term, term->c.y, 2);
  if (dst >= term->col) {
    k8t_tmClearRegion(term, term->c.x, term->c.y, term->col-1, term->c.y);
  } else {
    memmove(&term->line[term->c.y][dst], &term->line[term->c.y][src], size*sizeof(K8TGlyph));
    k8t_tmClearRegion(term, src, term->c.y, dst-1, term->c.y);
  }
}


K8TERM_API void k8t_tmInsertBlankLine (K8Term *term, int n) {
  if (term->c.y < term->top || term->c.y > term->bot) return;
  k8t_tmScrollDown(term, term->c.y, n);
}


K8TERM_API void k8t_tmDeleteLine (K8Term *term, int n) {
  if (term->c.y < term->top || term->c.y > term->bot) return;
  k8t_tmScrollUp(term, term->c.y, n, 0);
}


K8TERM_API void k8t_tmSetScrollRegion (K8Term *term, int t, int b) {
  int temp;
  //
  K8T_LIMIT(t, 0, term->row-1);
  K8T_LIMIT(b, 0, term->row-1);
  if (t > b) {
    temp = t;
    t = b;
    b = temp;
  }
  term->top = t;
  term->bot = b;
}


K8TERM_API void k8t_tmUnshowHistory (K8Term *term) {
  if (term != NULL && term->topline != 0) {
    term->topline = 0;
    k8t_tmWantRedraw(term, 1);
  }
}


K8TERM_API void k8t_tmSendFocusEvent (K8Term *term, int focused) {
  if (term != NULL && K8T_ISSET(term, K8T_MODE_FOCUSEVT)) {
    k8t_ttyWriteStr(term, "\x1b[");
    k8t_ttyWrite(term, focused?"I":"O", 1);
  }
}


////////////////////////////////////////////////////////////////////////////////
// term utilities
K8TERM_API void k8t_tmResetMode (K8Term *term) {
  term->esc = 0;
  term->top = 0;
  term->bot = term->row-1;
  term->mode = K8T_MODE_WRAP/*|K8T_MODE_MOUSEBTN*//*|K8T_MODE_GFX1*/;
  term->mousemode = 1000;
  term->charset = K8T_MODE_GFX0;
  //
  term->c.state = K8T_CURSOR_DEFAULT;
  k8t_tmResetAttrs(term);
  //
  k8t_tmCursor(term, K8T_CURSOR_SAVE);
}


K8TERM_API void k8t_tmReset (K8Term *term) {
  K8TGlyph g;
  //
  term->c = (K8TCursor){{
    .attr = K8T_ATTR_NORMAL,
    .fg = 0,
    .bg = 0
  }, .x = 0, .y = 0, .state = K8T_CURSOR_DEFAULT};
  //
  g.state = K8T_GLYPH_DIRTY;
  g.attr = term->c.attr.attr;
  g.fg = term->c.attr.fg;
  g.bg = term->c.attr.bg;
  g.c[0] = ' ';
  g.c[1] = 0;
  //
  k8t_tmResetMode(term);
  //k8t_tmClearRegion(0, 0, term->col-1, term->row-1);
  for (int y = 0; y < term->row; ++y) {
    k8t_tmDirtyMark(term, y, 2);
    for (int x = 0; x < term->col; ++x) term->alt[y][x] = term->line[y][x] = g;
  }
  for (int y = term->row; y < term->linecount; ++y) {
    for (int x = 0; x < term->col; ++x) term->line[y][x] = g;
  }
  //
  term->topline = 0;
  k8t_tmFullDirty(term);
}


K8TERM_API int k8t_tmInitialize (K8Term *term, int col, int row, int maxhistory) {
  //memset(term, 0, sizeof(K8Term));
  //term->needConv = needConversion ? 1 : 0;
  if (maxhistory < 0) maxhistory = 0;
  //
  /*
  term->dumpescapes = 0;
  term->dumpeskeys = 0;
  */
  //
  if (term->wrapOnCtrlChars < 0 || term->wrapOnCtrlChars > 1) term->wrapOnCtrlChars = 0;
  if (!term->tabsize) term->tabsize = 8;
  term->wrbufsize = K8T_WBUFSIZ;
  //
  // colors should be already set here
  /*
  term->deffg = 7;
  term->defbg = 0;
  term->defboldfg = -1;
  term->defunderfg = -1;
  //
  term->defcurfg = 0;
  term->defcurbg = 16;
  term->defcurinactivefg = 0;
  term->defcurinactivebg = 17;
  */
  //
  term->row = row;
  term->col = col;
  term->dirty = calloc(term->row, sizeof(*term->dirty));
  term->maxhistory = maxhistory;
  term->linecount = term->maxhistory+term->row;
  term->line = calloc(term->linecount, sizeof(K8TLine));
  term->alt = calloc(term->row, sizeof(K8TLine));
  for (int y = 0; y < term->linecount; ++y) term->line[y] = calloc(term->col, sizeof(K8TGlyph));
  for (int y = 0; y < term->row; ++y) term->alt[y] = calloc(term->col, sizeof(K8TGlyph));
  /* setup screen */
  k8t_tmReset(term);
  //
  return 1;
}


////////////////////////////////////////////////////////////////////////////////
// tty resising
K8TERM_API int k8t_tmResize (K8Term *term, int col, int row) {
  if (term != NULL && col > 0 && row > 0) {
    int mincol = K8T_MIN(col, term->col);
    int slide = term->c.y-row+1;
    K8TGlyph g;
    //
    k8t_selHide(term);
    //
    g.state = K8T_GLYPH_DIRTY;
    g.attr = K8T_ATTR_NORMAL;
    g.fg = term->deffg;
    g.bg = term->defbg;
    g.c[0] = ' ';
    g.c[1] = 0;
    //
    if (slide > 0) {
      /* cursor should be moved */
      k8t_tmSetScrollRegion(term, 0, term->row-1);
      for (; slide > 0; --slide) k8t_tmScrollUp(term, 0, 1, 1); /* to fill history */
    }
    //
    if (row < term->row) {
      /* free unneeded rows */
      for (int f = row; f < term->row; ++f) {
        free(term->alt[f]);
        free(term->line[f]);
      }
      /* move history */
      for (int f = term->row; f < term->linecount; ++f) term->line[f-term->row+row] = term->line[f];
      term->linecount -= (term->row-row);
      /* resize to new height */
      term->alt = realloc(term->alt, row*sizeof(K8TLine));
      term->line = realloc(term->line, term->linecount*sizeof(K8TLine));
    } else if (row > term->row) {
      /* resize to new height */
      K8TLine *newl = malloc((row+term->maxhistory)*sizeof(K8TLine));
      //
      /* now copy screen lines */
      for (int f = 0; f < term->row; ++f) newl[f] = term->line[f];
      /* empty new screen lines */
      for (int f = term->row; f < row; ++f) {
        newl[f] = calloc(col, sizeof(K8TGlyph));
        for (int x = 0; x < col; ++x) newl[f][x] = g;
      }
      /* copy history */
      for (int f = term->row; f < term->linecount; ++f) newl[f-term->row+row] = term->line[f];
      /* and free old lines */
      free(term->line);
      term->line = newl;
      term->linecount += (row-term->row);
      //
      term->alt = realloc(term->alt, row*sizeof(K8TLine));
      /* add more alt lines */
      for (int f = term->row; f < row; ++f) {
        term->alt[f] = calloc(col, sizeof(K8TGlyph));
        for (int x = 0; x < col; ++x) term->alt[f][x] = g;
      }
    }
    //
    if (row != term->row) {
      term->dirty = realloc(term->dirty, row*sizeof(*term->dirty));
      for (int f = 0; f < row; ++f) term->dirty[f] = 0;
    }
    //
    /* resize each row to new width, zero-pad if needed */
    for (int f = 0; f < term->linecount; ++f) {
      if (col != term->col) term->line[f] = realloc(term->line[f], col*sizeof(K8TGlyph));
      for (int x = mincol; x < col; ++x) term->line[f][x] = g;
      if (f < row) {
        k8t_tmDirtyMark(term, f, 2);
        term->alt[f] = realloc(term->alt[f], col*sizeof(K8TGlyph));
        for (int x = mincol; x < col; ++x) term->alt[f][x] = g;
        //for (int x = 0; x < mincol; ++x) term->line[f][x] = g;
      }
    }
    /* update terminal size */
    term->topline = 0;
    term->col = col;
    term->row = row;
    k8t_tmFullDirty(term);
    /* make use of the limit in k8t_tmMoveTo */
    k8t_tmMoveTo(term, term->c.x, term->c.y);
    /* reset scrolling region */
    k8t_tmSetScrollRegion(term, 0, row-1);
    return (slide > 0);
  }
  //
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
// tty resize ioctl
K8TERM_API void k8t_ttyResize (K8Term *term) {
  if (term != NULL && term->cmdfd >= 0) {
    struct winsize w;
    //
    w.ws_row = term->row;
    w.ws_col = term->col;
    w.ws_xpixel = w.ws_ypixel = 0;
    if (ioctl(term->cmdfd, TIOCSWINSZ, &w) < 0) fprintf(stderr, "Warning: couldn't set window size: %s\n", strerror(errno));
    k8t_tmWantRedraw(term, 0);
  }
}
