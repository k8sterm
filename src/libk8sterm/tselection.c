#include "k8sterm.h"


////////////////////////////////////////////////////////////////////////////////
// selection
K8TERM_API void k8t_selInit (K8Term *term) {
  if (term != NULL) {
    term->sel.tclick1 = term->sel.tclick2 = term->clockTicks(term);
    term->sel.mode = 0;
    term->sel.bx = -1;
    term->sel.clip = NULL;
  }
}


K8TERM_API void k8t_selHide (K8Term *term) {
  if (term != NULL && term->sel.bx != -1) {
    term->sel.mode = 0;
    term->sel.bx = -1;
    k8t_tmFullDirty(term);
  }
}


K8TERM_API int k8t_isSelected (K8Term *term, int x, int y) {
  if (term == NULL || term->sel.bx == -1) return 0;
  //
  if (y >= term->row) y = 0-(y-term->row+1);
  if (term->sel.ey == y && term->sel.by == y) {
    int bx = K8T_MIN(term->sel.bx, term->sel.ex);
    int ex = K8T_MAX(term->sel.bx, term->sel.ex);
    //
    return K8T_BETWEEN(x, bx, ex);
  }
  //
  return
    ((term->sel.b.y < y && y < term->sel.e.y) || (y == term->sel.e.y && x <= term->sel.e.x)) ||
    (y == term->sel.b.y && x >= term->sel.b.x && (x <= term->sel.e.x || term->sel.b.y != term->sel.e.y));
}


K8TERM_API void k8t_selXSet (K8Term *term, char *str) {
  /* register the selection for both the clipboard and the primary */
  if (term == NULL) return;
  if (term->sel.clip != NULL) free(term->sel.clip);
  term->sel.clip = str;
  if (term->fixSelection != NULL) term->fixSelection(term);
  //fprintf(stderr, "[%s]\n", str);
}


K8TERM_API void k8t_selClear (K8Term *term) {
  if (term != NULL) {
    if (term->clearSelection != NULL) term->clearSelection(term);
    if (term->sel.clip != NULL) free(term->sel.clip);
    term->sel.clip = NULL;
    term->sel.mode = 0;
    if (term->sel.bx != 0) {
      term->sel.bx = -1;
      k8t_tmFullDirty(term);
      k8t_drawTerm(term, 1);
    }
  }
}


K8TERM_API K8TLine k8t_selGet (K8Term *term, int y) {
  K8TLine l;
  //
  if (y >= term->row) return NULL;
  if (y < 0) {
    if (y < -(term->maxhistory)) return NULL;
    l = term->line[term->row+(-y)-1];
  } else {
    l = term->line[y];
  }
  return l;
}


K8TERM_API void k8t_selCopy (K8Term *term) {
  char *str, *ptr;
  int x, y, bufsize, is_selected = 0;
  //
  if (term == NULL || term->sel.bx == -1) {
    str = NULL;
  } else {
    int sy = K8T_MIN(term->sel.e.y, term->sel.b.y);
    int ey = K8T_MAX(term->sel.e.y, term->sel.b.y);
    //
    /*
    fprintf(stderr, "bx=%d; ex=%d; by=%d; ey=%d\n", term->sel.bx, term->sel.by, term->sel.ex, term->sel.ey);
    fprintf(stderr, "  b.x=%d; e.x=%d; b.y=%d; e.y=%d\n", term->sel.b.x, term->sel.b.y, term->sel.e.x, term->sel.e.y);
    fprintf(stderr, "  sy=%d; ey=%d\n", sy, ey);
    */
    if (ey >= term->row) { k8t_selClear(term); return; }
    bufsize = (term->col+1)*(ey-sy+1)*K8T_UTF_SIZ;
    ptr = str = malloc(bufsize);
    /* append every set and selected glyph to the selection */
    for (y = sy; y <= ey; ++y) {
      K8TLine l = k8t_selGet(term, y);
      char *pstart = ptr;
      //
      if (l == NULL) continue;
      for (x = 0; x < term->col; ++x) {
        if ((is_selected = k8t_isSelected(term, x, y)) != 0) {
          int size = k8t_UTF8Size(l[x].c);
          //
          //if (size == 1) fprintf(stderr, "x=%d; y=%d; size=%d; c=%d\n", x, y, size, l[x].c[0]);
          if (size == 1) {
            unsigned char c = (unsigned char)l[x].c[0];
            //
            *ptr = (c < 32 || c >= 127) ? ' ' : c;
          } else if (size > 0) {
            memcpy(ptr, l[x].c, size);
          }
          ptr += size;
        }
      }
      //fprintf(stderr, "y=%d; linebytes=%d\n", y, (int)(ptr-pstart));
      // trim trailing spaces
      while (ptr > pstart && ptr[-1] == ' ') --ptr;
      // \n at the end of every unwrapped selected line except for the last one
      if (is_selected && y < ey && k8t_isSelected(term, term->col-1, y) && !(l[term->col-1].state&K8T_GLYPH_WRAP)) *ptr++ = '\n';
    }
    *ptr = 0;
  }
  if (!str || !str[0]) {
    k8t_selClear(term);
  } else {
    k8t_selXSet(term, str);
  }
}


K8TERM_API void k8t_mouseReport (K8Term *term, int x, int y, int event, int button, int state) {
  if (term != NULL) {
    //int x = K8T_X2COL(e->xbutton.x);
    //int y = K8T_Y2ROW(term, e->xbutton.y);
    //int button = e->xbutton.button;
    //int state = e->xbutton.state;
    //char buf[] = { '\033', '[', 'M', 0, 32+x+1, 32+y+1 };
    char buf[32], *p;
    char lastCh = 'M';
    int ss;
    //
    if (x < 0 || x >= term->col || y < 0 || y >= term->row) return;
    //
    //fprintf(stderr, "mr(%d): x=%d; y=%d\n", term->mousemode, x, y);
    #if 0
      case 1000: /* X11 xterm mouse reporting */
      case 1005: /* utf-8 mouse encoding */
      case 1006: /* sgr mouse encoding */
      case 1015: /* urxvt mouse encoding */
    #endif
    //sprintf(buf, "\x1b[M%c%c%c", 0, 32+x+1, 32+y+1);
    p = buf+sprintf(buf, "\x1b[M");
    /* from urxvt */
    if (event == K8T_MOUSE_EVENT_MOTION) {
      if (!K8T_ISSET(term, K8T_MODE_MOUSEMOTION) || (x == term->mouseox && y == term->mouseoy)) return;
      button = term->mouseob+32;
      term->mouseox = x;
      term->mouseoy = y;
    } else if (event == K8T_MOUSE_EVENT_UP /*|| button == AnyButton*/) {
      if (term->mousemode != 1006) {
        button = 3; // 'release' flag
      } else {
        lastCh = 'm';
        button -= K8T_MOUSE_BUTTON_LEFT;
        if (button >= 3) button += 64-3;
      }
    } else {
      button -= K8T_MOUSE_BUTTON_LEFT;
      if (button >= 3) button += 64-3;
      if (event == K8T_MOUSE_EVENT_DOWN) {
        term->mouseob = button;
        term->mouseox = x;
        term->mouseoy = y;
      }
    }
    //
    ss =
      (state&K8T_MOUSE_STATE_SHIFT ? 4 : 0) |
      (state&K8T_MOUSE_STATE_ALT ? 8 : 0) | // was K8T_MOUSE_STATE_WIN
      (state&K8T_MOUSE_STATE_CTRL ? 16 : 0);
    //
    switch (term->mousemode) {
      case 1006: /* sgr */
        buf[2] = '<';
        p += sprintf(p, "%d;", button+ss);
        break;
      case 1015: /* urxvt */
        --p; // remove 'M'
        p += sprintf(p, "%d;", 32+button+ss);
        break;
      default:
        *p++ = 32+button+ss;
        break;
    }
    // coords
    switch (term->mousemode) {
      case 1005: /* utf-8 */
        p += k8t_UTF8Encode(p, x+1);
        p += k8t_UTF8Encode(p, y+1);
        break;
      case 1006: /* sgr */
        p += sprintf(p, "%d;%d%c", x+1, y+1, lastCh);
        break;
      case 1015: /* urxvt */
        p += sprintf(p, "%d;%dM", x+1, y+1);
        break;
      default:
        p += sprintf(p, "%c%c", 32+x+1, 32+y+1);
        break;
    }
    *p = 0;
    /*
    {
      fprintf(stderr, "(%d)<", term->mousemode);
      for (const unsigned char *s = (const unsigned char *)buf; *s; ++s) {
        if (s[0] < 32) fprintf(stderr, "{%d}", s[0]); else fputc(s[0], stderr);
      }
      fputs(">\n", stderr);
    }
    */
    k8t_ttyWriteStrNoEnc(term, buf);
  }
}
