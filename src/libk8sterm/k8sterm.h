/* See LICENSE for licence details. */
#ifndef K8STERM_H
#define K8STERM_H

#include <alloca.h>
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <iconv.h>
#include <limits.h>
#include <locale.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <signal.h>
#include <sys/ioctl.h>
#include <sys/select.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

#ifdef __cplusplus
extern "C" {
#endif


////////////////////////////////////////////////////////////////////////////////
#ifndef K8TERM_API
# define K8TERM_API
#endif

#ifndef K8TERM_EXTERN
# define K8TERM_EXTERN extern
#endif

// for debug
//#define K8TERM_COLLECT_CSI


////////////////////////////////////////////////////////////////////////////////
/* Arbitrary sizes */
#define K8T_ESC_TITLE_SIZ  (4096)
#define K8T_ESC_BUF_SIZ    (256)
#define K8T_ESC_ARG_SIZ    (16)
#define K8T_DRAW_BUF_SIZ   (2048)
#define K8T_UTF_SIZ        (4)
#define K8T_OBUFSIZ        (256)
#define K8T_WBUFSIZ        (256)


////////////////////////////////////////////////////////////////////////////////
/* misc utility macros */
#ifdef K8T_MAX
# undef K8T_MAX
#endif
#define K8T_MAX(__a,__b) \
  ({ typeof(__a)_a = (__a); \
     typeof(__b)_b = (__b); \
    _a > _b ? _a : _b; })

#ifdef K8T_MIN
# undef K8T_MIN
#endif
#define K8T_MIN(__a,__b) \
  ({ typeof(__a)_a = (__a); \
     typeof(__b)_b = (__b); \
    _a < _b ? _a : _b; })

#define K8T_ARRLEN(a)         (sizeof(a)/sizeof(a[0]))
#define K8T_DEFAULT(a, b)     (a) = ((a) ? (a) : (b))
#define K8T_BETWEEN(x, a, b)  ((a) <= (x) && (x) <= (b))
#define K8T_LIMIT(x, a, b)    ((x) = (x) < (a) ? (a) : (x) > (b) ? (b) : (x))
#define K8T_ATTRCMP(a, b)     ((a).attr != (b).attr || (a).fg != (b).fg || (a).bg != (b).bg)
#define K8T_ISSET(_tr,flag)   ((_tr)->mode&(flag))
#define K8T_X2COL(x)          ((x)/xw.cw)
#define K8T_Y2ROW(_tr, y)     ((y-(opt_tabposition==1 ? xw.tabheight : 0))/xw.ch-((_tr) != NULL ? (_tr)->topline : 0))
#define K8T_ISGFX(mode)       ((mode)&K8T_ATTR_GFX)


////////////////////////////////////////////////////////////////////////////////
enum {
  K8T_BELL_AUDIO = 0x01,
  K8T_BELL_URGENT = 0x02
};

// glyph attributes (bitmask)
enum  {
  K8T_ATTR_NORMAL    = 0x00,
  K8T_ATTR_REVERSE   = 0x01,
  K8T_ATTR_UNDERLINE = 0x02,
  K8T_ATTR_BOLD      = 0x04,
  K8T_ATTR_GFX       = 0x08,
  K8T_ATTR_FGSET     = 0x10,
  K8T_ATTR_BGSET     = 0x20,
};

// cursor operations for k8t_tmCursor()
enum {
  K8T_CURSOR_SAVE,
  K8T_CURSOR_LOAD
};

// cursor state (bitmask)
enum {
  K8T_CURSOR_DEFAULT  = 0x00,
  K8T_CURSOR_HIDE     = 0x01,
  K8T_CURSOR_WRAPNEXT = 0x02 /* this flag is set if we should move to new line *before* outputting next char */
};

// glyph state (bitmask)
enum {
  K8T_GLYPH_SET   = 0x01, /* for selection only */
  K8T_GLYPH_DIRTY = 0x02,
  K8T_GLYPH_WRAP  = 0x10, /* can be set for the last line glyph */
};

// terminal mode flags (bitmask)
enum {
  K8T_MODE_WRAP        = 0x01,
  K8T_MODE_INSERT      = 0x02,
  K8T_MODE_APPKEYPAD   = 0x04,
  K8T_MODE_ALTSCREEN   = 0x08,
  K8T_MODE_CRLF        = 0x10,
  K8T_MODE_MOUSEBTN    = 0x20,
  K8T_MODE_MOUSEMOTION = 0x40,
  K8T_MODE_MOUSE       = 0x20|0x40,
  K8T_MODE_REVERSE     = 0x80,
  K8T_MODE_BRACPASTE   = 0x100,
  K8T_MODE_FOCUSEVT    = 0x200,
  K8T_MODE_DISPCTRL    = 0x400, //TODO: not implemented yet
  K8T_MODE_GFX0        = 0x1000,
  K8T_MODE_GFX1        = 0x2000,
};

// escape sequence processor state
enum {
  K8T_ESC_START       = 0x01,
  K8T_ESC_CSI         = 0x02,
  K8T_ESC_OSC         = 0x04,
  K8T_OSC_TITLE       = 0x08,
  K8T_ESC_ALTCHARSET  = 0x10,
  K8T_ESC_HASH        = 0x20,
  K8T_ESC_PERCENT     = 0x40,
  K8T_ESC_ALTG1       = 0x80,
  K8T_ESC_OSC_FIRST   = 0x100,
  K8T_ESC_OSC_IGNORE  = 0x200,
  K8T_ESC_OSC_FOUR    = 0x400,
  K8T_ESC_OSC_TWO     = 0x800,
  // OSC, got 0x1b (execute the sequence)
  K8T_ESC_OSC_ESC     = 0x1000,
  // CSI should be parsed, and ignored
  K8T_ESC_CSI_IGNORE  = 0x2000,
};

// for mouse reports
enum {
  K8T_MOUSE_STATE_SHIFT = 0x01,
  K8T_MOUSE_STATE_CTRL  = 0x02,
  K8T_MOUSE_STATE_ALT   = 0x04,
  K8T_MOUSE_STATE_WIN   = 0x08
};

enum {
  K8T_MOUSE_BUTTON_LEFT   = 1,
  K8T_MOUSE_BUTTON_MIDDLE = 2,
  K8T_MOUSE_BUTTON_RIGHT  = 3
};

enum {
  K8T_MOUSE_EVENT_MOTION = -1,
  K8T_MOUSE_EVENT_UP     = 0,
  K8T_MOUSE_EVENT_DOWN   = 1
};


////////////////////////////////////////////////////////////////////////////////
typedef uint64_t K8TTimeMSec;
typedef struct K8Term K8Term;


typedef struct __attribute__((packed)) {
  /* character code */
  union __attribute__((packed)) {
    uint32_t uc;
    char c[K8T_UTF_SIZ];
  };
  uint8_t attr;  /* attribute flags */
  uint16_t fg;   /* foreground */
  uint16_t bg;   /* background */
  uint8_t state; /* state flags */
} K8TGlyph;

typedef K8TGlyph *K8TLine;


typedef struct {
  K8TGlyph attr; /* current char attributes */
  int x;
  int y;
  char state;
} K8TCursor;


/* CSI Escape sequence structs */
/* ESC '[' [[ [<priv>] <arg> [;]] <mode>] */
typedef struct {
  #ifdef K8TERM_COLLECT_CSI
  char buf[K8T_ESC_BUF_SIZ]; /* raw string */
  int len;  /* raw string length */
  #endif
  int seenarg;
  char priv;
  int arg[K8T_ESC_ARG_SIZ];
  int narg; /* nb of args */
  char mode;
} K8TCSIEscape;


/* TODO: use better name for vars... */
typedef struct {
  int mode;
  int bx, by;
  int ex, ey;
  struct { int x, y; } b, e;
  char *clip;
  K8TTimeMSec tclick1;
  K8TTimeMSec tclick2;
} K8TSelection;


/* Internal representation of the screen */
struct K8Term {
  // unicode conversion map
  uint16_t *unimap;
  //
  int cmdfd; // for tty i/o
  int dead; // !0: don't do tty i/o
  //
  int needConv; /* 0: utf-8 locale */
  int belltype;
  int blackandwhite;
  int fastredraw;
  //int justSwapped;
  //
  int curblink;
  int curbhidden; // 0: visible; <0: should be hidden; >0: already hidden
  K8TTimeMSec lastBlinkTime;
  int curblinkinactive;
  //
  int row;        /* nb row */
  int col;        /* nb col */
  int topline;    /* top line for drawing (0: no history; 1: show one history line; etc) */
  int linecount;  /* full, with history */
  int maxhistory; /* max history lines; 0: none; <0: infinite */
  K8TLine *line;  /* screen */
  K8TLine *alt;   /* alternate screen */
  char *dirty;    /* dirtyness of lines */
  K8TCursor c;    /* cursor */
  int top;        /* top    scroll limit */
  int bot;        /* bottom scroll limit */
  int mode;       /* terminal mode flags */
  int mousemode;  /* mouse mode: 1000, 1005, 1006, 1015 */
  int esc;        /* escape state flags */
  int charset;    /* K8T_MODE_GFX0 or K8T_MODE_GFX1 */
  int tabsize;
  int wrapOnCtrlChars; /* do wrapping on '\b', '\r', '\f', '\n', '\v'; default: no */
  int historyLinesUsed; // from 0 to linecount-row
  //
  K8TCursor csaved; /* saved cursor info */
  // old cursor position
  int oldcx;
  int oldcy;
  //
  char title[K8T_ESC_TITLE_SIZ+1];
  int titlelen;
  //
  int mouseob;
  int mouseox;
  int mouseoy;
  //
  char obuf[K8T_OBUFSIZ];
  int obuflen;
  //
  char ubuf[K8T_UTF_SIZ];
  int ubufpos;
  //
  char drawbuf[K8T_DRAW_BUF_SIZ];
  //
  char wrbuf[K8T_WBUFSIZ];
  int wrbufsize;
  int wrbufused;
  int wrbufpos;
  //
  K8TCSIEscape escseq;
  K8TSelection sel;
  K8TTimeMSec lastDrawTime;
  //
  int deffg, defbg;
  int defboldfg, defunderfg;
  int defcurfg, defcurbg;
  int defcurfg1, defcurbg1;
  int defcurinactivefg, defcurinactivebg;
  //
  // do k8t_tmClearRegion() on partial scrolls? (mc flickers with this not set)
  // -1: don't clear
  // 0: only chars (don't touch attributes)
  // 1: default mode (clear to default colors)
  // 2: smart mode (clear to the colors of the last line)
  int clearOnPartialScrollMode;
  //
  int wantRedraw;
  //
  K8TTimeMSec lastActiveTime;
  //
  int dumpescapes;
  int dumpeskeys;
  //
  void *udata;
  // callbacks
  //
  K8TTimeMSec (*clockTicks) (K8Term *term);
  void (*setLastDrawTime) (K8Term *term, K8TTimeMSec time);
  // locale conversions
  // returns new length
  int (*isConversionNecessary) (K8Term *term);
  int (*loc2utf) (K8Term *term, char *dest, const char *src, int len);
  int (*utf2loc) (K8Term *term, char *dest, const char *src, int len);
  //
  int (*isFocused) (K8Term *term); // used to draw various cursor shapes, for example
  int (*isVisible) (K8Term *term); // at least partially
  // draws
  void (*drawSetFG) (K8Term *term, int clr);
  void (*drawSetBG) (K8Term *term, int clr);
  // coords in chars
  void (*drawRect) (K8Term *term, int x0, int y0, int cols, int rows); // use FG
  void (*drawFillRect) (K8Term *term, int x0, int y0, int cols, int rows); // use FG
  void (*drawCopyArea) (K8Term *term, int x0, int y0, int cols, int rows); // copy pixmap to screen
  void (*drawString) (K8Term *term, int x, int y, int cols, const K8TGlyph *base, int fontset, const char *s, int bytelen);
  //TODO: FIX OVERLAY SYSTEM!
  // check and draw overlay, return 1 to stop built-in drawing
  int (*drawOverlay) (K8Term *term, int x0, int x1, int scry, int lineno, int dontcopy);
  //
  void (*fixSelection) (K8Term *term);
  void (*clearSelection) (K8Term *term);
  //
  void (*titleChanged) (K8Term *term);
  void (*doBell) (K8Term *term);
  //
  int (*isUnderOverlay) (K8Term *term, int x, int y, int w); // 0: none; 1: fully; -1: partially
  void (*clipToOverlay) (K8Term *term, int *x0, int y, int *w); // if w < 1: completely out of
  //
  void (*addHistoryLine) (K8Term *term); // called when a new line is pushed to history
  //
  //int (*doWriteBufferFlush) (K8Term *term, const void *buf, int bytes); // return # of bytes flushed, 0 to do stdflush, -1 on error
  void (*afterNewLine) (K8Term *term); // called after cursor moves to new line on output
  void (*afterScreenSwap) (K8Term *term); // called after screen swap
};


////////////////////////////////////////////////////////////////////////////////
K8TERM_EXTERN void k8t_tmWantRedraw (K8Term *term, int forceFast);
K8TERM_EXTERN void k8t_tmDirtyMark (K8Term *term, int lineno, int flag);
K8TERM_EXTERN void k8t_selInit (K8Term *term);
K8TERM_EXTERN void k8t_selHide (K8Term *term);
K8TERM_EXTERN int k8t_isSelected (K8Term *term, int x, int y);
K8TERM_EXTERN void k8t_mouseReport (K8Term *term, int x, int y, int event, int button, int state);
K8TERM_EXTERN void k8t_selXSet (K8Term *term, char *str);
K8TERM_EXTERN void k8t_selClear (K8Term *term);
K8TERM_EXTERN K8TLine k8t_selGet (K8Term *term, int y);
K8TERM_EXTERN void k8t_selCopy (K8Term *term);
//K8TERM_EXTERN void k8t_dbgCSIDump (K8Term *term);
K8TERM_EXTERN void k8t_tmResetAttrs (K8Term *term);
K8TERM_EXTERN void k8t_tmSetAttr (K8Term *term, int *attr, int argc);
K8TERM_EXTERN int k8t_tmDoWrap (K8Term *term);
//K8TERM_EXTERN void k8t_tmCSIHandle (K8Term *term);
//K8TERM_EXTERN void k8t_tmCSIReset (K8Term *term);
//K8TERM_EXTERN void k8t_tmCSIParse (K8Term *term);
K8TERM_EXTERN void k8t_tmPutTab (K8Term *term);
//K8TERM_EXTERN int k8t_tmPutCtrl (K8Term *term, char ascii);
K8TERM_EXTERN void k8t_tmPutC (K8Term *term, const char *c);
K8TERM_EXTERN int k8t_ttyCanRead (K8Term *term);
K8TERM_EXTERN int k8t_ttyCanWrite (K8Term *term);
K8TERM_EXTERN int k8t_ttyRead (K8Term *term);
K8TERM_EXTERN void k8t_tmFeed (K8Term *term, char *buffer, int bufLen); //WARNING! DO NOT USE! BAD, INTERNAL, ETC!
K8TERM_EXTERN int k8t_ttySendWriteBuf (K8Term *term);
K8TERM_EXTERN void k8t_ttyFlushWriteBuf (K8Term *term);
K8TERM_EXTERN int k8t_ttyWriteRawChar (K8Term *term, const char *s, int len, int noenc);
K8TERM_EXTERN void k8t_ttyWriteNoEnc (K8Term *term, const char *s, size_t n);
K8TERM_EXTERN void k8t_ttyWrite (K8Term *term, const char *s, size_t n);
K8TERM_EXTERN void k8t_tmDirty (K8Term *term, int top, int bot);
K8TERM_EXTERN void k8t_tmFullDirty (K8Term *term);
K8TERM_EXTERN void k8t_tmMoveTo (K8Term *term, int x, int y);
K8TERM_EXTERN void k8t_tmClearRegion (K8Term *term, int x1, int y1, int x2, int y2); // to default colors
K8TERM_EXTERN void k8t_tmClearRegionEx (K8Term *term, int x1, int y1, int x2, int y2); // use clearOnPartialScrollMode
K8TERM_EXTERN void k8t_tmCursor (K8Term *term, int mode);
K8TERM_EXTERN void k8t_tmAdjMaxHistory (K8Term *term, int maxh);
K8TERM_EXTERN void k8t_tmSwapScreen (K8Term *term);
K8TERM_EXTERN void k8t_tmScrollSelection (K8Term *term, int orig, int n, int tohistory);
K8TERM_EXTERN void k8t_tmScrollDown (K8Term *term, int orig, int n);
K8TERM_EXTERN void k8t_tmScrollUp (K8Term *term, int orig, int n, int tohistory);
K8TERM_EXTERN void k8t_tmCharWrap (K8Term *term, int y, int wrap);
K8TERM_EXTERN void k8t_tmNewLine (K8Term *term, int first_col); /* wrap==2: do wrapping */
K8TERM_EXTERN void k8t_tmSetChar (K8Term *term, const char *c);
K8TERM_EXTERN void k8t_tmDeleteChar (K8Term *term, int n);
K8TERM_EXTERN void k8t_tmInsertBlank (K8Term *term, int n);
K8TERM_EXTERN void k8t_tmInsertBlankLine (K8Term *term, int n);
K8TERM_EXTERN void k8t_tmDeleteLine (K8Term *term, int n);
K8TERM_EXTERN void k8t_tmSetScrollRegion (K8Term *term, int t, int b);
K8TERM_EXTERN void k8t_tmUnshowHistory (K8Term *term);
K8TERM_EXTERN void k8t_tmSendFocusEvent (K8Term *term, int focused);
K8TERM_EXTERN void k8t_tmResetMode (K8Term *term);
K8TERM_EXTERN void k8t_tmReset (K8Term *term);
K8TERM_EXTERN int k8t_tmInitialize (K8Term *term, int col, int row, int maxhistory);
K8TERM_EXTERN int k8t_tmResize (K8Term *term, int col, int row);
K8TERM_EXTERN void k8t_ttyResize (K8Term *term);
K8TERM_EXTERN int k8t_UTF8Decode (uint32_t *u, const void *buf);
K8TERM_EXTERN int k8t_UTF8Encode (void *buf, uint32_t uc);
K8TERM_EXTERN int k8t_UTF8IsFull (const void *buf, int buflen);
K8TERM_EXTERN int k8t_UTF8Size (const void *buf);
K8TERM_EXTERN int k8t_UTF8strlen (const char *s);
K8TERM_EXTERN void k8t_UTF8ChopLast (char *s);
K8TERM_EXTERN void k8t_drawLine (K8Term *term, int x1, int x2, int scry, int lineno, int dontcopy);
K8TERM_EXTERN void k8t_drawRegion (K8Term *term, int x1, int y1, int x2, int y2, int forced);
K8TERM_EXTERN void k8t_drawTerm (K8Term *term, int forced);
K8TERM_EXTERN void k8t_drawCursor (K8Term *term, int forceblit);
K8TERM_EXTERN void k8t_drawString (K8Term *term, const char *s, const K8TGlyph *base, int x, int y, int charlen, int bytelen);
K8TERM_EXTERN void k8t_drawClear (K8Term *term, int x1, int y1, int x2, int y2);
K8TERM_EXTERN void k8t_drawCopy (K8Term *term, int x, int y, int cols, int rows);


static inline void k8t_ttyWriteStr (K8Term *term, const char *s) { if (s != NULL && s[0]) k8t_ttyWrite(term, s, strlen(s)); }
static inline void k8t_ttyWriteStrNoEnc (K8Term *term, const char *s) { if (s != NULL && s[0]) k8t_ttyWriteNoEnc(term, s, strlen(s)); }

//FIXME: do utf-8!
static inline void k8t_ttyPutStr (K8Term *term, const char *s) { if (s != NULL) while (*s) { k8t_tmPutC(term, s); ++s; } }


////////////////////////////////////////////////////////////////////////////////
K8TERM_API void k8t_die (const char *errstr, ...) __attribute__((noreturn)) __attribute__((format(printf,1,2)));


#ifdef __cplusplus
}
#endif
#endif
