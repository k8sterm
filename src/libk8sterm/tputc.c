#include "k8sterm.h"


////////////////////////////////////////////////////////////////////////////////
static void k8t_dbgCSIDump (K8Term *term) {
  fprintf(stderr, "^[");
  #ifdef K8TERM_COLLECT_CSI
  for (int f = 0; f < term->escseq.len; ++f) {
    uint32_t c = (unsigned char)(term->escseq.buf[f]);
    if (c != 32 && isprint(c)) fputc(c, stderr);
    else if (c == '\n') fprintf(stderr, "(\\n)");
    else if (c == '\r') fprintf(stderr, "(\\r)");
    else if (c == 0x1b) fprintf(stderr, "(\\e)");
    else fprintf(stderr, "(%u)", c);
  }
  #else
  if (term->escseq.priv > 0) fputc('?', stderr);
  else if (term->escseq.priv < 0) fputc('>', stderr);
  for (int f = 0; f < term->escseq.narg; ++f) {
    if (f != 0) fputc(';', stderr);
    if (f < K8T_ESC_ARG_SIZ) {
      fprintf(stderr, "%d", term->escseq.arg[f]);
    } else {
      fprintf(stderr, "%d", -666);
    }
  }
  if (term->escseq.mode) {
    uint32_t c = (unsigned char)term->escseq.mode;
    if (c != 32 && isprint(c)) fputc(c, stderr);
    else if (c == '\n') fprintf(stderr, "(\\n)");
    else if (c == '\r') fprintf(stderr, "(\\r)");
    else if (c == 0x1b) fprintf(stderr, "(\\e)");
    else fprintf(stderr, "(%u)", c);
  } else {
    fprintf(stderr, "<nocmd>");
  }
  #endif
  fputc('\n', stderr);
}


////////////////////////////////////////////////////////////////////////////////
K8TERM_API void k8t_tmResetAttrs (K8Term *term) {
  term->c.attr.attr &= ~(K8T_ATTR_REVERSE|K8T_ATTR_UNDERLINE|K8T_ATTR_BOLD|K8T_ATTR_FGSET|K8T_ATTR_BGSET);
  term->c.attr.fg = term->deffg;
  term->c.attr.bg = term->defbg;
}


K8TERM_API void k8t_tmSetAttr (K8Term *term, int *attr, int argc) {
  int aidx = 0;
  do {
    int aa = (aidx < argc ? attr[aidx++] : 0);
    switch (aa) {
      case 0: k8t_tmResetAttrs(term); break;
      case 1: term->c.attr.attr |= K8T_ATTR_BOLD; break;
      case 4: term->c.attr.attr |= K8T_ATTR_UNDERLINE; break;
      case 7: term->c.attr.attr |= K8T_ATTR_REVERSE; break;
      case 22: term->c.attr.attr &= ~K8T_ATTR_BOLD; break;
      case 24: term->c.attr.attr &= ~K8T_ATTR_UNDERLINE; break;
      case 27: term->c.attr.attr &= ~K8T_ATTR_REVERSE; break;
      case 38:
        if (aidx+1 < argc && attr[aidx] == 5) {
          aa = attr[aidx+1];
          aidx += 2;
          if (K8T_BETWEEN(aa, 0, 255)) {
            term->c.attr.fg = aa;
            term->c.attr.attr |= K8T_ATTR_FGSET;
          } else {
            fprintf(stderr, "erresc: bad fgcolor %d\n", aa);
          }
        } else {
          //fprintf(stderr, "erresc: gfx attr %d unknown\n", aa);
          term->c.attr.fg = term->deffg;
          term->c.attr.attr &= ~K8T_ATTR_FGSET;
        }
        break;
      case 39:
        term->c.attr.fg = term->deffg;
        term->c.attr.attr &= ~K8T_ATTR_FGSET;
        break;
      case 48:
        if (aidx+1 < argc && attr[aidx] == 5) {
          aa = attr[aidx+1];
          aidx += 2;
          if (K8T_BETWEEN(aa, 0, 255)) {
            term->c.attr.bg = aa;
            term->c.attr.attr |= K8T_ATTR_BGSET;
          } else {
            fprintf(stderr, "erresc: bad bgcolor %d\n", aa);
          }
        } else {
          fprintf(stderr, "erresc: gfx attr %d unknown\n", aa);
        }
        break;
      case 49:
        term->c.attr.bg = term->defbg;
        term->c.attr.attr &= ~K8T_ATTR_BGSET;
        break;
      default:
        if (K8T_BETWEEN(aa, 30, 37)) { term->c.attr.fg = aa-30; term->c.attr.attr |= K8T_ATTR_FGSET; }
        else if (K8T_BETWEEN(aa, 40, 47)) { term->c.attr.bg = aa-40; term->c.attr.attr |= K8T_ATTR_BGSET; }
        else if (K8T_BETWEEN(aa, 90, 97)) { term->c.attr.fg = aa-90+8; term->c.attr.attr |= K8T_ATTR_FGSET; }
        else if (K8T_BETWEEN(aa, 100, 107)) { term->c.attr.bg = aa-100+8; term->c.attr.attr |= K8T_ATTR_BGSET; }
        else { fprintf(stderr, "erresc: gfx attr %d unknown\n", aa); k8t_dbgCSIDump(term); }
        break;
    }
  } while (aidx < argc);
}


// return:
//   0: no wrapping
//   1: wrapped
//  -1: no-wrap mode and tries to wrap
K8TERM_API int k8t_tmDoWrap (K8Term *term) {
  if (term->c.state&K8T_CURSOR_WRAPNEXT) {
    if (K8T_ISSET(term, K8T_MODE_WRAP)) {
      // always go to first col
      k8t_tmNewLine(term, 2); // this will reset K8T_CURSOR_WRAPNEXT
      return 1;
    } else {
      k8t_tmCharWrap(term, term->c.y, 0);
      return -1;
    }
  }
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
// esc processing
static void csiBad (K8Term *term, int argn) {
  if (argn >= 0) {
    fprintf(stderr, "erresc: unknown csi (argn=%d) ", argn);
  } else {
    fprintf(stderr, "erresc: unknown csi ");
  }
  k8t_dbgCSIDump(term);
}


static void csiPrivXT (K8Term *term) {
  switch (term->escseq.mode) {
    case 'T': // reset title mode; not implemented, and ingored
      // see https://invisible-island.net/xterm/ctlseqs/ctlseqs.html
      /*
        Ps = 0  --  Do not set window/icon labels using hexadecimal.
        Ps = 1  --  Do not query window/icon labels using hexadecimal.
        Ps = 2  --  Do not set window/icon labels using UTF-8.
        Ps = 3  --  Do not query window/icon labels using UTF-8.
      */
      break;
    case 'c': // send/query device attrs
      if (term->escseq.narg == 0 || (term->escseq.narg == 1 && term->escseq.arg[0] == 0)) {
        k8t_ttyWriteStr(term, "\x1b[>1;2c"); // VT100
      }
      break;
  }
}


static void csiPriv (K8Term *term) {
  char buf[32];
  int aidx;
  switch (term->escseq.mode) {
    case 'l': /* RM -- Reset Mode */
      if (term->escseq.narg < 1) {
        term->escseq.narg = 1;
        term->escseq.arg[0] = 0;
      }
      for (aidx = 0; aidx < term->escseq.narg; ++aidx) {
        switch (term->escseq.arg[aidx]) {
          case 1: // 1001 for xterm compatibility
            term->mode &= ~K8T_MODE_APPKEYPAD;
            if (term->dumpeskeys) fprintf(stderr, "*KPMODE: off (1l)\n");
            break;
          case 5: /* DECSCNM -- Remove reverse video */
            if (K8T_ISSET(term, K8T_MODE_REVERSE)) {
              term->mode &= ~K8T_MODE_REVERSE;
              k8t_tmFullDirty(term);
            }
            break;
          case 7: /* autowrap off */
            if (K8T_ISSET(term, K8T_MODE_WRAP)) {
              (void)k8t_tmDoWrap(term);
              term->mode &= ~K8T_MODE_WRAP;
            }
            break;
          case 12: /* att610 -- Stop blinking cursor (IGNORED) */
            break;
          case 20: /* non-standard code? */
            term->mode &= ~K8T_MODE_CRLF;
            break;
          case 25: /* hide cursor */
            if ((term->c.state&K8T_CURSOR_HIDE) == 0) {
              term->c.state |= K8T_CURSOR_HIDE;
              k8t_tmWantRedraw(term, 0);
            }
            break;
          case 1000: /* disable X11 xterm mouse reporting */
            term->mode &= ~K8T_MODE_MOUSEBTN;
            break;
          case 1002:
            term->mode &= ~K8T_MODE_MOUSEMOTION;
            break;
          case 1004:
            term->mode &= ~K8T_MODE_FOCUSEVT;
            break;
          case 1005: /* utf-8 mouse encoding */
          case 1006: /* sgr mouse encoding */
          case 1015: /* urxvt mouse encoding */
            term->mousemode = 1000;
            break;
          case 1049: /* = 1047 and 1048 */
          case 47:
          case 1047:
            if (K8T_ISSET(term, K8T_MODE_ALTSCREEN)) {
              k8t_tmClearRegion(term, 0, 0, term->col-1, term->row-1);
              k8t_tmSwapScreen(term);
              if (term->escseq.arg[aidx] == 1049) k8t_tmCursor(term, K8T_CURSOR_LOAD);
            }
            break;
          case 1048:
            k8t_tmCursor(term, K8T_CURSOR_LOAD);
            break;
          case 2004: /* reset bracketed paste mode */
            term->mode &= ~K8T_MODE_BRACPASTE;
            break;
          default:
            csiBad(term, aidx);
            break;
        }
      }
      break;
    case 'h': /* SM -- Set terminal mode */
      if (term->escseq.narg < 1) {
        term->escseq.narg = 1;
        term->escseq.arg[0] = 0;
      }
      for (aidx = 0; aidx < term->escseq.narg; ++aidx) {
        switch (term->escseq.arg[aidx]) {
          case 1:
            term->mode |= K8T_MODE_APPKEYPAD;
            if (term->dumpeskeys) fprintf(stderr, "*KPMODE: on (1h)\n");
            break;
          case 5: /* DECSCNM -- Reverve video */
            if (!K8T_ISSET(term, K8T_MODE_REVERSE)) {
              term->mode |= K8T_MODE_REVERSE;
              k8t_tmFullDirty(term);
            }
            break;
          case 7:
            if (!K8T_ISSET(term, K8T_MODE_WRAP)) {
              if (term->c.state&K8T_CURSOR_WRAPNEXT) k8t_tmMoveTo(term, term->c.x, term->c.y); // reset 'wrap-next' flag
              term->mode |= K8T_MODE_WRAP;
            }
            break;
          case 20:
            term->mode |= K8T_MODE_CRLF;
            break;
          case 12: /* att610 -- Start blinking cursor (IGNORED) */
            /* fallthrough for xterm cvvis = CSI [ ? 12 ; 25 h */
            if (term->escseq.narg > 1 && term->escseq.arg[1] != 25) break;
            /* fallthrough */
          case 25:
            if ((term->c.state&K8T_CURSOR_HIDE) != 0) {
              term->c.state &= ~K8T_CURSOR_HIDE;
              k8t_tmWantRedraw(term, 0);
            }
            break;
          case 1000: /* 1000,1002: enable xterm mouse report */
            term->mode |= K8T_MODE_MOUSEBTN;
            break;
          case 1002:
            term->mode |= K8T_MODE_MOUSEMOTION;
            break;
          case 1004:
            term->mode |= K8T_MODE_FOCUSEVT;
            break;
          case 1005: /* utf-8 mouse encoding */
          case 1006: /* sgr mouse encoding */
          case 1015: /* urxvt mouse encoding */
            term->mousemode = term->escseq.arg[aidx];
            break;
          case 1049: /* = 1047 and 1048 */
          case 47:
          case 1047:
            if (K8T_ISSET(term, K8T_MODE_ALTSCREEN)) {
              k8t_tmClearRegion(term, 0, 0, term->col-1, term->row-1);
            } else {
              if (term->escseq.arg[aidx] == 1049) k8t_tmCursor(term, K8T_CURSOR_SAVE);
              k8t_tmSwapScreen(term);
            }
            break;
          case 1048:
            k8t_tmCursor(term, K8T_CURSOR_SAVE);
            break;
          case 2004: /* set bracketed paste mode */
            term->mode |= K8T_MODE_BRACPASTE;
            break;
          default:
            csiBad(term, aidx);
            break;
        }
      }
      break;
    case 'n':
      switch (term->escseq.arg[0]) {
        case 5: /* Device status report (DSR) */
          k8t_ttyWriteStr(term, "\x1b[0n");
          break;
        case 6: /* cursor position report */
          sprintf(buf, "\x1b[%d;%dR", term->c.x+1, term->c.y+1);
          k8t_ttyWriteStr(term, buf);
          break;
      }
      break;
    case 'r': /* DECSTBM -- Set Scrolling Region */
      if (term->escseq.arg[0] == 1001) {
        // xterm compatibility
        term->mode &= ~K8T_MODE_APPKEYPAD;
        if (term->dumpeskeys) fprintf(stderr, "*KPMODE: off (1001r)\n");
      } else {
        csiBad(term, -1);
      }
      break;
    case 's': /* DECSC -- Save cursor position (ANSI.SYS) */
      if (term->escseq.arg[0] == 1001) {
        // xterm compatibility
        term->mode |= K8T_MODE_APPKEYPAD;
        if (term->dumpeskeys) fprintf(stderr, "*KPMODE: on (1001s)\n");
      } else {
        csiBad(term, -1);
      }
      break;
    default:
      csiBad(term, -1);
      break;
  }
}


static void csiNormal (K8Term *term) {
  char buf[32];
  int aidx;
  switch (term->escseq.mode) {
    case '@': /* ICH -- Insert <n> blank char */
      K8T_DEFAULT(term->escseq.arg[0], 1);
      k8t_tmInsertBlank(term, term->escseq.arg[0]);
      break;
    case 'A': /* CUU -- Cursor <n> Up */
    case 'e':
      K8T_DEFAULT(term->escseq.arg[0], 1);
      k8t_tmMoveTo(term, term->c.x, term->c.y-term->escseq.arg[0]);
      break;
    case 'B': /* CUD -- Cursor <n> Down */
      K8T_DEFAULT(term->escseq.arg[0], 1);
      k8t_tmMoveTo(term, term->c.x, term->c.y+term->escseq.arg[0]);
      break;
    case 'C': /* CUF -- Cursor <n> Forward */
    case 'a':
      K8T_DEFAULT(term->escseq.arg[0], 1);
      k8t_tmMoveTo(term, term->c.x+term->escseq.arg[0], term->c.y);
      break;
    case 'D': /* CUB -- Cursor <n> Backward */
      K8T_DEFAULT(term->escseq.arg[0], 1);
      k8t_tmMoveTo(term, term->c.x-term->escseq.arg[0], term->c.y);
      break;
    case 'E': /* CNL -- Cursor <n> Down and first col */
      K8T_DEFAULT(term->escseq.arg[0], 1);
      k8t_tmMoveTo(term, 0, term->c.y+term->escseq.arg[0]);
      break;
    case 'F': /* CPL -- Cursor <n> Up and first col */
      K8T_DEFAULT(term->escseq.arg[0], 1);
      k8t_tmMoveTo(term, 0, term->c.y-term->escseq.arg[0]);
      break;
    case 'G': /* CHA -- Move to <col> */
    case '`': /* XXX: HPA -- same? */
      K8T_DEFAULT(term->escseq.arg[0], 1);
      k8t_tmMoveTo(term, term->escseq.arg[0]-1, term->c.y);
      break;
    case 'H': /* CUP -- Move to <row> <col> */
    case 'f': /* XXX: HVP -- same? */
      K8T_DEFAULT(term->escseq.arg[0], 1);
      K8T_DEFAULT(term->escseq.arg[1], 1);
      k8t_tmMoveTo(term, term->escseq.arg[1]-1, term->escseq.arg[0]-1);
      break;
    /* XXX: (CSI n I) CHT -- Cursor Forward Tabulation <n> tab stops */
    case 'J': /* ED -- Clear screen */
      term->sel.bx = -1;
      switch (term->escseq.arg[0]) {
        case 0: /* below */
          k8t_tmClearRegion(term, term->c.x, term->c.y, term->col-1, term->c.y);
          if (term->c.y < term->row-1) k8t_tmClearRegion(term, 0, term->c.y+1, term->col-1, term->row-1);
          break;
        case 1: /* above */
          if (term->c.y > 1) k8t_tmClearRegion(term, 0, 0, term->col-1, term->c.y-1);
          k8t_tmClearRegion(term, 0, term->c.y, term->c.x, term->c.y);
          break;
        case 2: /* all */
          k8t_tmClearRegion(term, 0, 0, term->col-1, term->row-1);
          break;
        default:
          csiBad(term, -1);
          break;
      }
      break;
    case 'K': /* EL -- Clear line */
      switch (term->escseq.arg[0]) {
        case 0: /* right */
          k8t_tmClearRegion(term, term->c.x, term->c.y, term->col-1, term->c.y);
          break;
        case 1: /* left */
          k8t_tmClearRegion(term, 0, term->c.y, term->c.x, term->c.y);
          break;
        case 2: /* all */
          k8t_tmClearRegion(term, 0, term->c.y, term->col-1, term->c.y);
          break;
      }
      break;
    case 'S': /* SU -- Scroll <n> line up */
      K8T_DEFAULT(term->escseq.arg[0], 1);
      k8t_tmScrollUp(term, term->top, term->escseq.arg[0], 0);
      break;
    case 'T': /* SD -- Scroll <n> line down */
      K8T_DEFAULT(term->escseq.arg[0], 1);
      k8t_tmScrollDown(term, term->top, term->escseq.arg[0]);
      break;
    case 'L': /* IL -- Insert <n> blank lines */
      K8T_DEFAULT(term->escseq.arg[0], 1);
      k8t_tmInsertBlankLine(term, term->escseq.arg[0]);
      break;
    case 'l': /* RM -- Reset Mode */
      if (term->escseq.narg < 1) {
        term->escseq.narg = 1;
        term->escseq.arg[0] = 0;
      }
      for (aidx = 0; aidx < term->escseq.narg; ++aidx) {
        switch (term->escseq.arg[aidx]) {
          //2: Keyboard Action Mode (AM)
          //12: Send/receive (SRM)
          //20: Normal Linefeed (LNM)
          case 3:
            term->mode &= ~K8T_MODE_DISPCTRL;
            break;
          case 4:
            term->mode &= ~K8T_MODE_INSERT;
            break;
          default:
            csiBad(term, aidx);
            break;
        }
      }
      break;
    case 'M': /* DL -- Delete <n> lines */
      K8T_DEFAULT(term->escseq.arg[0], 1);
      k8t_tmDeleteLine(term, term->escseq.arg[0]);
      break;
    case 'X': /* ECH -- Erase <n> char */
      K8T_DEFAULT(term->escseq.arg[0], 1);
      k8t_tmClearRegion(term, term->c.x, term->c.y, term->c.x + term->escseq.arg[0], term->c.y);
      break;
    case 'P': /* DCH -- Delete <n> char */
      K8T_DEFAULT(term->escseq.arg[0], 1);
      k8t_tmDeleteChar(term, term->escseq.arg[0]);
      break;
    /* XXX: (CSI n Z) CBT -- Cursor Backward Tabulation <n> tab stops */
    case 'd': /* VPA -- Move to <row> */
      K8T_DEFAULT(term->escseq.arg[0], 1);
      k8t_tmMoveTo(term, term->c.x, term->escseq.arg[0]-1);
      break;
    case 'h': /* SM -- Set terminal mode */
      if (term->escseq.narg < 1) {
        term->escseq.narg = 1;
        term->escseq.arg[0] = 0;
      }
      //
      for (aidx = 0; aidx < term->escseq.narg; ++aidx) {
        switch (term->escseq.arg[aidx]) {
          case 3:
            term->mode |= K8T_MODE_DISPCTRL;
            break;
          case 4:
            term->mode |= K8T_MODE_INSERT;
            break;
          default:
            csiBad(term, aidx);
            break;
        }
      }
      break;
    case 'm': /* SGR -- Terminal attribute (color) */
      k8t_tmSetAttr(term, term->escseq.arg, term->escseq.narg);
      break;
    case 'n':
      switch (term->escseq.arg[0]) {
        case 5: /* Device status report (DSR) */
          k8t_ttyWriteStr(term, "\x1b[0n");
          break;
        case 6: /* cursor position report */
          sprintf(buf, "\x1b[%d;%dR", term->c.x+1, term->c.y+1);
          k8t_ttyWriteStr(term, buf);
          break;
      }
      break;
    case 'r': /* DECSTBM -- Set Scrolling Region */
      K8T_DEFAULT(term->escseq.arg[0], 1);
      K8T_DEFAULT(term->escseq.arg[1], term->row);
      k8t_tmSetScrollRegion(term, term->escseq.arg[0]-1, term->escseq.arg[1]-1);
      k8t_tmMoveTo(term, 0, 0);
      break;
    case 's': /* DECSC -- Save cursor position (ANSI.SYS) */
      k8t_tmCursor(term, K8T_CURSOR_SAVE);
      break;
    case 'u': /* DECRC -- Restore cursor position (ANSI.SYS) */
      k8t_tmCursor(term, K8T_CURSOR_LOAD);
      break;
    default:
      csiBad(term, -1);
      break;
  }
}


static void csiHandle (K8Term *term) {
  if (term->dumpescapes) {
    fprintf(stderr, "CSI: ");
    k8t_dbgCSIDump(term);
  }

  if ((term->esc&K8T_ESC_CSI_IGNORE) || term->escseq.mode == 0) {
    term->esc = 0;
  } else {
    term->esc = 0;
    if (term->escseq.narg > K8T_ESC_ARG_SIZ) term->escseq.narg = K8T_ESC_ARG_SIZ;
    // '>'?
    if (term->escseq.priv < 0) {
      csiPrivXT(term);
    } else if (term->escseq.priv > 0) {
      csiPriv(term);
    } else {
      csiNormal(term);
    }
  }
}


static inline void k8t_tmRestartEsq (K8Term *term) {
  memset(&term->escseq, 0, sizeof(term->escseq));
  term->escseq.seenarg = -1; // "first char" mark
  term->esc = K8T_ESC_START;
}


// return non-zero if the sequence is finished
static int csiNextChar (K8Term *term, char ascii) {
  int res = 0;

  /*
    this parser is not completely right, i believe, because ';;m'
    will register two args, not three. i'm not sure that this is
    completely right, but meh.
  */

  // check for private sequence
  if (ascii == '?' && term->escseq.narg == 0 && term->escseq.seenarg == -1) {
    term->escseq.priv = 1;
    term->escseq.seenarg = 0;
  } else if (ascii == '>' && term->escseq.narg == 0 && term->escseq.seenarg == -1) {
    term->escseq.priv = -1;
    term->escseq.seenarg = 0;
  } else {
    /* advance on non-digit if we've seen any arg digit, and always advance on semicolon */
    if ((ascii == ';' || term->escseq.seenarg > 0) && (ascii < '0' || ascii > '9')) {
      if (term->escseq.narg < 32760) ++term->escseq.narg;
      term->escseq.seenarg = 0; // no arg digits yet
    }

    if (ascii == ':') {
      /* keep parsing, but ignore */
      term->esc |= K8T_ESC_CSI_IGNORE;
    } else if (K8T_BETWEEN(ascii, 0x40, 0x7E)) {
      /* final char -- command */
      term->escseq.mode = ascii;
      res = 1;
    } else if (ascii == ';') {
      /* next arg; do nothing, because it is already processed */
    } else if (ascii >= '0' && ascii <= '9') {
      term->escseq.seenarg = 1;
      int idx = term->escseq.narg;
      if (idx < 0) {
        fprintf(stderr, "csiNextChar: internal error (invalid arg index)\n");
        abort();
      }
      if (idx < K8T_ESC_ARG_SIZ) {
        int val = term->escseq.arg[idx];
        val = val*10+(ascii-'0');
        if (val > 65535) val = 65535; /* arbitrary clamping */
        term->escseq.arg[idx] = val;
      }
    } else if (ascii == 0x18 || ascii == 0x1a) {
      // do nothing, interrupt current escape sequence
      // 0x1a should print a reversed question mark (DEC does this), but meh
      term->esc |= K8T_ESC_CSI_IGNORE;
      res = 1;
    } else if (ascii == 0x1b) {
      // esc, cancel and restart
      k8t_tmRestartEsq(term);
    } else if (ascii >= 0 && ascii < 32) {
      /* other control chars are ignored */
    } else {
      /* invalid char, abort */
      term->esc |= K8T_ESC_CSI_IGNORE;
      res = 1;
    }
  }

  return res;
}


////////////////////////////////////////////////////////////////////////////////
K8TERM_API void k8t_tmPutTab (K8Term *term) {
  if (term->tabsize > 0) {
    int space = term->tabsize-term->c.x%term->tabsize;
    if (space > 0) k8t_tmMoveTo(term, term->c.x+space, term->c.y);
  }
}


////////////////////////////////////////////////////////////////////////////////
// put char to output buffer or process command

#define CC_ESC_BREAK   (-1)
#define CC_PROCESS     (0)
#define CC_IGNORE      (1)

// return 1 if this was control character
// return -1 if this should break esape sequence
static int k8t_tmPutCtrl (K8Term *term, char ascii) {
  int res = CC_IGNORE;

  // the thing that should not be
  if (term->esc&K8T_ESC_OSC) {
    // the caller should take care of it
    res = CC_PROCESS;
  }
  /*
  else if (term->esc&K8T_ESC_CSI) {
    // in CSI, C0 control sequences are ignored
    if (ascii >= 0 && ascii < 32) {
      switch (ascii) {
        case 0x18: case 0x1a: // do nothing, interrupt current escape sequence
          // 0x1a should print a reversed question mark (DEC does this), but meh
          res = CC_ESC_BREAK;
          break;
        case 0x1b: // esc, cancel and restart
          k8t_tmRestartEsq(term);
          res = CC_IGNORE;
          break;
        default:
          res = CC_IGNORE;
          break;
      }
    } else if (ascii == 127) {
      res = CC_IGNORE;
    } else {
      res = CC_PROCESS;
    }
  }
  */
  else {
    // control chars in ESC sequences are processed, and ignored (except 0x18 and 0x1a)
    switch (ascii) {
      case '\t':
        if (k8t_tmDoWrap(term) >= 0) k8t_tmPutTab(term);
        break;
      case '\b':
        if (term->wrapOnCtrlChars) (void)k8t_tmDoWrap(term);
        k8t_tmMoveTo(term, term->c.x-1, term->c.y);
        break;
      case '\r':
        if (term->wrapOnCtrlChars) (void)k8t_tmDoWrap(term);
        k8t_tmMoveTo(term, 0, term->c.y);
        break;
      case '\f': case '\n': case '\v':
        if (term->wrapOnCtrlChars) (void)k8t_tmDoWrap(term);
        /* go to first col if the mode is set */
        k8t_tmNewLine(term, (K8T_ISSET(term, K8T_MODE_CRLF) ? 1 : 0));
        break;
      case '\a': // 0x07
        // k8: hack: do not bell if we are in ESC sequence
        if ((term->esc&K8T_ESC_START) == 0) {
          if (term->doBell != NULL) term->doBell(term);
        }
        break;
      case 14: term->charset = K8T_MODE_GFX1; break;
      case 15: term->charset = K8T_MODE_GFX0; break;
      case 0x18: case 0x1a: // do nothing, interrupt current escape sequence
        // 0x1a should print a reversed question mark (DEC does this), but meh
        res = CC_ESC_BREAK;
        break;
      case 127: break; // ignore it
      case 0x1b: // esc, cancel and restart
        k8t_tmRestartEsq(term);
        res = CC_IGNORE;
        break;
      //case 0x9b: k8t_tmCSIReset(term); term->esc = K8T_ESC_START|K8T_ESC_CSI; break;
      default:
        if (ascii >= 0 && ascii < 32) res = CC_IGNORE; else res = CC_PROCESS;
        break;
    }
  }

  return res;
}


//==========================================================================
//
//  k8t_tmPutC
//
//==========================================================================
K8TERM_API void k8t_tmPutC (K8Term *term, const char *charptr) {
  char ascii = *charptr;
  int ctl, ulen, putc;
  uint32_t uc;

  #if 0
  FILE *fo = fopen("zzz.log", "a+");
  if (fo) {
    fwrite(charptr, 1, 1, fo);
    fclose(fo);
  }
  #endif

  ctl = k8t_tmPutCtrl(term, ascii);
  if (ctl == CC_IGNORE) return; // control char; should not break escape sequence
  if (ctl == CC_ESC_BREAK) { term->esc = 0; return; } // control char; should break escape sequence
  if (ctl != CC_PROCESS) {
    fprintf(stderr, "FATAL: invalid `k8t_tmPutCtrl` result (%d)\n", ctl);
    abort();
  }

  //dlogf("k8t_tmPutC: [%c]\n", c[0]);
  if (term->esc&K8T_ESC_START) {
    if (term->esc&K8T_ESC_CSI) {
      /* CSI processing -- ESC [ */
      if (csiNextChar(term, ascii) != 0) {
        csiHandle(term);
      }
    } else if (term->esc&K8T_OSC_TITLE) {
      /* OSC window title sequence in progress */
      if (ascii >= 0 && ascii < 32) {
        /* C0, no special meaning (except BEL and ESC) */
        if (ascii == '\a' || ascii == 0x1b) {
          term->title[term->titlelen] = '\0';
          if (term->titleChanged != NULL) term->titleChanged(term);
        }
        if (ascii == 0x1b) k8t_tmRestartEsq(term); else term->esc = 0;
      } else {
        ulen = k8t_UTF8Size(charptr);
        if (term->titlelen+ulen >= K8T_ESC_TITLE_SIZ) {
          // too long, put what we were collected so far, and ignore the rest
          term->esc = 0;
          term->title[term->titlelen] = '\0';
          if (term->titleChanged != NULL) term->titleChanged(term);
          term->esc ^= K8T_OSC_TITLE;
          term->esc |= K8T_ESC_OSC_IGNORE;
        } else if (ulen > 0) {
          memcpy(term->title+term->titlelen, charptr, ulen);
          term->titlelen += ulen;
          term->title[term->titlelen] = '\0';
        }
      }
    } else if (term->esc&K8T_ESC_OSC) {
      /* OSC processing -- ESC ] */
      /* TODO: handle other OSC */
      if (ascii == '\a') {
        term->esc = 0; // ST
      } else if (ascii == 0x1b) {
        k8t_tmRestartEsq(term);
      } else if (term->esc&K8T_ESC_OSC_FIRST) {
        /* first char after ']' */
        term->esc ^= K8T_ESC_OSC_FIRST;
        if (ascii == '2' || ascii == '0') {
          term->esc |= K8T_ESC_OSC_TWO;
        } else {
          term->esc |= K8T_ESC_OSC_IGNORE;
        }
      } else if (term->esc&K8T_ESC_OSC_TWO) {
        term->esc ^= K8T_ESC_OSC_TWO;
        if (ascii == ';') {
          term->title[0] = 0;
          term->titlelen = 0;
          term->esc |= K8T_OSC_TITLE;
          //updateTabBar = 1;
        } else {
          term->esc |= K8T_ESC_OSC_IGNORE;
        }
      } /* else: "ignore OSC" mode */
    } else if (term->esc&K8T_ESC_ALTCHARSET) {
      term->esc = 0;
      switch (ascii) {
        case '0': /* Line drawing crap */
          term->mode |= K8T_MODE_GFX0;
          break;
        case 'B': /* Back to regular text */
          term->mode &= ~K8T_MODE_GFX0;
          break;
        default:
          fprintf(stderr, "esc unhandled charset: ESC ( %c\n", ascii);
          term->mode &= ~K8T_MODE_GFX0;
          break;
      }
    } else if (term->esc&K8T_ESC_ALTG1) {
      term->esc = 0;
      switch (ascii) {
        case '0': /* Line drawing crap */
          term->mode |= K8T_MODE_GFX1;
          break;
        case 'B': /* Back to regular text */
          term->mode &= ~K8T_MODE_GFX1;
          break;
        default:
          fprintf(stderr, "esc unhandled charset: ESC ) %c\n", ascii);
          term->mode &= ~K8T_MODE_GFX1;
          break;
      }
    } else if (term->esc&K8T_ESC_HASH) {
      term->esc = 0;
      switch (ascii) {
        case '8': /* DECALN -- DEC screen alignment test -- fill screen with E's */
          //tfillscreenwithE();
          break;
      }
    } else if (term->esc&K8T_ESC_PERCENT) {
      term->esc = 0;
      switch (ascii) {
        case 'G': case '8':
          term->needConv = 0;
          break;
        default:
          term->needConv = term->isConversionNecessary(term);
          break;
      }
    } else {
      switch (ascii) {
        case '[': term->esc |= K8T_ESC_CSI; break;
        case ']': term->esc |= K8T_ESC_OSC|K8T_ESC_OSC_FIRST; break;
        case '(': term->esc |= K8T_ESC_ALTCHARSET; break;
        case ')': term->esc |= K8T_ESC_ALTG1; break;
        case '#': term->esc |= K8T_ESC_HASH; break;
        case '%': term->esc |= K8T_ESC_PERCENT; break;
        case '\\': term->esc = 0; break; /* noop sequence; used to finish OSCs */
        default: /* other escapes */
          term->esc = 0;
          if (term->dumpescapes) {
            fprintf(stderr, "ESC: ESC 0x%02X ('%c'; %u)\n", (uint8_t)ascii, (isprint(ascii) ? ascii : '.'), (uint8_t)ascii);
          }
          switch (ascii) {
            case 'D': /* IND -- Linefeed */
              (void)k8t_tmDoWrap(term);
              if (term->c.y == term->bot) k8t_tmScrollUp(term, term->top, 1, 1); else k8t_tmMoveTo(term, term->c.x, term->c.y+1);
              break;
            case 'E': /* NEL -- Next line */
              (void)k8t_tmDoWrap(term);
              k8t_tmNewLine(term, 1); /* always go to first col */
              break;
            case 'M': /* RI -- Reverse linefeed */
              if (term->c.state&K8T_CURSOR_WRAPNEXT) {
                k8t_tmCharWrap(term, term->c.y, 0);
                term->c.state &= ~K8T_CURSOR_WRAPNEXT;
                if (K8T_ISSET(term, K8T_MODE_WRAP)) {
                  // we should move to the next line, so just stay where we are
                  k8t_tmMoveTo(term, term->c.x, term->c.y);
                  break;
                }
              }
              if (term->c.y == term->top) k8t_tmScrollDown(term, term->top, 1);
              else k8t_tmMoveTo(term, term->c.x, term->c.y-1);
              break;
            case 'c': /* RIS -- Reset to inital state */
              k8t_tmReset(term);
              break;
            case '=': /* DECPAM -- Application keypad */
            case '>': /* DECPNM -- Normal keypad */
              break;
            case '7': /* DECSC -- Save Cursor */
              /* Save current state (cursor coordinates, attributes, character sets pointed at by G0, G1) */
              //TODO?
              k8t_tmCursor(term, K8T_CURSOR_SAVE);
              break;
            case '8': /* DECRC -- Restore Cursor */
              //TODO?
              k8t_tmCursor(term, K8T_CURSOR_LOAD);
              break;
            case 'F': /* Cursor to lower left corner of screen */
              k8t_tmMoveTo(term, 0, term->row-1);
              break;
            case 'Z': /* DEC private identification */
              k8t_ttyWriteStr(term, "\x1b[?1;2c");
              break;
            default:
              fprintf(stderr, "erresc: unknown sequence ESC 0x%02X ('%c')\n", (uint8_t)ascii, isprint(ascii)?ascii:'.');
              break;
          }
      }
    }
  } else {
    putc = 1;
    if (term->needConv && K8T_ISGFX(term->c.attr.attr)) {
      k8t_UTF8Decode(&uc, charptr);
      putc = (uc >= 32 && uc < 127); //FIXME: nothing at all?
    } else {
      putc = (ascii < 0 || (ascii >= 32 && ascii < 127)); // ignore unknown control chars
    }
    if (putc && k8t_tmDoWrap(term) >= 0) { // if wrapping, but wrap is off, don't want more chars
      k8t_tmSetChar(term, charptr);
      if (term->c.x+1 < term->col) k8t_tmMoveTo(term, term->c.x+1, term->c.y);
      else term->c.state |= K8T_CURSOR_WRAPNEXT;
    }
  }
}
