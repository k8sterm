#include "k8sterm.h"


////////////////////////////////////////////////////////////////////////////////
K8TERM_API void k8t_drawClear (K8Term *term, int x1, int y1, int x2, int y2) {
  term->drawSetFG(term, K8T_ISSET(term, K8T_MODE_REVERSE) ? term->deffg : term->defbg);
  term->drawFillRect(term, x1, y1, x2-x1+1, y2-y1+1);
}


/* copy buffer pixmap to screen pixmap */
K8TERM_API void k8t_drawCopy (K8Term *term, int x, int y, int cols, int rows) {
  if (term != NULL) term->drawCopyArea(term, x, y, cols, rows);
}


K8TERM_API void k8t_drawCursor (K8Term *term, int forceblit) {
  (void)forceblit;
  K8TGlyph g;
  int sl, scrx, scry;
  //int cmy;
  int focused;
  //
  if (term == NULL) return;
  //
  K8T_LIMIT(term->oldcx, 0, term->col-1);
  K8T_LIMIT(term->oldcy, 0, term->row-1);
  //
  focused = term->isFocused(term);
  //cmy = term->row-term->topline-1;
  //
  //fprintf(stderr, "cur: cx=%d; cy=%d; sy=%d; cmy=%d\n", term->c.x, term->c.y, term->c.y+term->topline, cmy);
  if (!focused && !term->curblinkinactive) term->curbhidden = 0;
  //
  if (1/*term->oldcy != cmy*/) {
    scrx = term->oldcx;
    scry = term->oldcy+term->topline;
    //
    //FIXME: avoid unnecessary cursor erase when window is not focused
    if (scry >= 0 && scry < term->row) {
      if (term->curbhidden < 0 ||
          (term->c.state&K8T_CURSOR_HIDE) ||
          (!focused && term->defcurinactivebg) ||
          term->oldcy != term->c.y || term->oldcx != term->c.x) {
        /* remove the old cursor */
        if (!term->isUnderOverlay(term, scrx, scry, 1)) {
          sl = k8t_UTF8Size(term->line[term->oldcy][scrx].c);
          g = term->line[term->oldcy][scrx];
          if (k8t_isSelected(term, scrx, scry)) g.attr ^= K8T_ATTR_REVERSE;
          k8t_drawString(term, g.c, &g, scrx, scry, 1, sl);
          //k8t_drawClear(term, scrx, term->oldcy, scrx, term->oldcy);
          //TODO
          /*if (forceblit)*/ {
            k8t_drawCopy(term, scrx, scry, 1, 1);
          }/* else {
            term->line[term->oldcy][scrx].state |= K8T_GLYPH_DIRTY;
            k8t_tmDirtyMark(term, term->oldcy, 1);
          }*/
        }
      }
      if (term->curbhidden) term->curbhidden = 1;
    }
  }
  //
  if (/*term->oldcy == cmy ||*/ (term->c.state&K8T_CURSOR_HIDE) || (term->curbhidden && term->defcurbg < 0)) return;
  /* draw the new one */
  scrx = term->c.x;
  scry = term->c.y+term->topline;
  //
  if (scry >= 0 && scry < term->row && !term->isUnderOverlay(term, scrx, scry, 1)) {
    int nodraw = 0;
    //
    //fprintf(stderr, "cx=%d; cy=%d\n", scrx, scry);
    if (!focused) {
      if (term->defcurinactivebg < 0) {
        term->drawSetFG(term, (term->defcurbg1 < 0 ? term->defcurbg : term->defcurbg1));
        term->drawRect(term, scrx, scry, 1, 1);
        nodraw = 1;
      } else {
        g.bg = term->defcurinactivebg;
        g.fg = term->defcurinactivefg;
      }
    } else {
      if (term->curbhidden) term->curbhidden = 1;
      if (term->curbhidden && term->defcurbg1 < 0) return;
      g.bg = (term->curbhidden ? term->defcurbg1 : term->defcurbg);
      g.fg = (term->curbhidden && term->defcurfg1 < 0 ? term->defcurfg : term->defcurfg1);
    }
    //
    if (!nodraw) {
      g.uc = term->line[term->c.y][scrx].uc;
      g.state = 0;
      g.attr = K8T_ATTR_NORMAL|K8T_ATTR_FGSET|K8T_ATTR_BGSET;
      if (K8T_ISSET(term, K8T_MODE_REVERSE)) g.attr |= K8T_ATTR_REVERSE;
      sl = k8t_UTF8Size(g.c);
      k8t_drawString(term, g.c, &g, scrx, scry, 1, sl);
    }
    term->oldcx = scrx;
    term->oldcy = term->c.y;
    //TODO
    /*if (forceblit)*/ {
      k8t_drawCopy(term, scrx, scry, 1, 1);
    }
  }
}


K8TERM_API void k8t_drawString (K8Term *term, const char *s, const K8TGlyph *base, int x, int y, int charlen, int bytelen) {
  if (term != NULL) {
    int fg = base->fg, bg = base->bg, temp;
    int defF = ((base->attr&K8T_ATTR_FGSET) == 0), defB = ((base->attr&K8T_ATTR_BGSET) == 0);
    int fontset = 0; /* 0: normal; 1: bold */
    //
    /* only switch default fg/bg if term is in RV mode */
    if (K8T_ISSET(term, K8T_MODE_REVERSE)) {
      if (defF) fg = term->defbg;
      if (defB) bg = term->deffg;
    } else {
      if (defF) fg = term->deffg;
      if (defB) bg = term->defbg;
    }
    if (base->attr&K8T_ATTR_REVERSE) defF = defB = 0;
    if (base->attr&K8T_ATTR_BOLD) {
      if (defF && defB && term->defboldfg >= 0) fg = term->defboldfg;
      else if (fg < 8) fg += 8;
      fontset = 1;
    }
    if ((base->attr&K8T_ATTR_UNDERLINE) && term->defunderfg >= 0) {
      if (defF && defB) fg = term->defunderfg;
    }
    //
    if (base->attr&K8T_ATTR_REVERSE) { temp = fg; fg = bg; bg = temp; }
    //
    term->drawSetBG(term, bg);
    term->drawSetFG(term, fg);
    term->drawString(term, x, y, charlen, base, fontset, s, bytelen);
  }
}


K8TERM_API void k8t_drawLine (K8Term *term, int x1, int x2, int scry, int lineno, int dontcopy) {
  int ovr;
  //
  //fprintf(stderr, "%d: k8t_drawLine: x1=%d; x2=%d; scry=%d; row:%d; lineno=%d\n", term->clockTicks(term), x1, x2, scry, term->row, lineno);
  if (term == NULL || scry < 0 || scry >= term->row || x2 <= x1) return;
  //
  // draw overlays
  ovr = term->isUnderOverlay(term, x1, scry, x2-x1);
  //
  if (ovr > 0) {
    // fully inside
    if (term->drawOverlay(term, x1, x2, scry, lineno, dontcopy)) return;
  } else if (ovr < 0) {
    // partially inside
    int sx = x1, sw = x2-x1, ex;
    //
    term->clipToOverlay(term, &sx, scry, &sw);
    if (sw != 0) {
      // draw string part and overlay part
      ex = sx+sw;
      // left string part
      if (sx > x1) {
        k8t_drawLine(term, x1, sx-x1, scry, lineno, dontcopy);
      }
      // right string part
      if (ex < x2) {
        k8t_drawLine(term, ex, x2, scry, lineno, dontcopy);
      }
      // overlay
      x1 = sx;
      x2 = ex;
      if (term->drawOverlay(term, x1, x2, scry, lineno, dontcopy)) return;
    }
  }
  //
  if (lineno < 0 || lineno >= term->linecount) {
    k8t_drawClear(term, 0, scry, term->col-1, scry);
    k8t_drawCopy(term, 0, scry, term->col, 1);
  } else {
    int ic, ib, ox, sl;
    int stx, ex;
    K8TGlyph base, new;
    K8TLine l = term->line[lineno];
    //
    if (lineno < term->row && term->topline == 0) {
      if (!term->dirty[lineno]) return;
      if (!dontcopy) {
        // fix 'dirty' flag for line
        if (term->dirty[lineno]&0x02) {
          // mark full line as dirty
          stx = 0;
          ex = term->col;
          term->dirty[lineno] = 0;
        } else {
          term->dirty[lineno] = 0;
          if (x1 > 0) for (int x = 0; x < x1; ++x) if (l[x].state&K8T_GLYPH_DIRTY) { term->dirty[lineno] = 1; break; }
          if (!term->dirty[lineno] && x2 < term->col) for (int x = x2; x < term->col; ++x) if (l[x].state&K8T_GLYPH_DIRTY) { term->dirty[lineno] = 1; break; }
          //
          // find dirty region
          for (stx = x1; stx < x2; ++stx) if (l[stx].state&K8T_GLYPH_DIRTY) break;
          for (ex = x2; ex > stx; --ex) if (l[ex-1].state&K8T_GLYPH_DIRTY) break;
          if (stx >= x2 || ex <= stx) return; // nothing to do
        }
      } else {
        // 'dontcopy' means that the whole screen is dirty
        stx = 0;
        ex = term->col;
        term->dirty[lineno] = 0;
      }
    } else {
      //if (lineno < term->row) term->dirty[lineno] = 0;
      stx = 0;
      ex = term->col;
    }
    //
    base = l[stx];
    if (term->sel.bx != -1 && k8t_isSelected(term, stx, lineno)) base.attr ^= K8T_ATTR_REVERSE;
    ic = ib = 0;
    ox = stx;
    //k8t_drawClear(term, stx, scry, ex-1, scry); //k8: actually, we don't need this for good monospace fonts, so...
    for (int x = stx; x < ex; ++x) {
      new = l[x];
      l[x].state &= ~K8T_GLYPH_DIRTY; //!
      if (term->sel.bx != -1 && k8t_isSelected(term, x, lineno)) new.attr ^= K8T_ATTR_REVERSE;
      if (ib > 0 && (K8T_ATTRCMP(base, new) || ib >= K8T_DRAW_BUF_SIZ-K8T_UTF_SIZ)) {
        // flush draw buffer
        k8t_drawString(term, term->drawbuf, &base, ox, scry, ic, ib);
        ic = ib = 0;
      }
      if (ib == 0) { ox = x; base = new; }
      sl = k8t_UTF8Size(new.c);
      memcpy(term->drawbuf+ib, new.c, sl);
      ib += sl;
      ++ic;
    }
    if (ib > 0) k8t_drawString(term, term->drawbuf, &base, ox, scry, ic, ib);
    //k8t_drawCopy(term, 0, scry, term->col, 1);
    //if (term->c.y == lineno && term->c.x >= stx && term->c.x < ex) k8t_drawCursor(term, 0);
    if (!dontcopy) k8t_drawCopy(term, stx, scry, ex-stx, 1);
  }
}


K8TERM_API void k8t_drawRegion (K8Term *term, int x1, int y1, int x2, int y2, int forced) {
  if (term != NULL) {
    int fulldirty = 0;
    //
    //term->justSwapped = 0;
    //
    if (!forced && !term->isVisible(term)) {
      //dlogf("invisible");
      term->lastDrawTime = 1;
      if (term->setLastDrawTime) term->setLastDrawTime(term, term->lastDrawTime);
      term->wantRedraw = 1;
      return;
    }
    //
    if (y1 == 0 && y2 == term->row) {
      fulldirty = 1;
      for (int y = 0; y < y2; ++y) if (!(term->dirty[y]&0x02)) { fulldirty = 0; break; }
    }
    //
    if (term->topline < term->row) {
      for (int y = y1; y < y2; ++y) k8t_drawLine(term, x1, x2, y+term->topline, y, fulldirty);
    }
    if (term->topline > 0) {
      int scry = K8T_MIN(term->topline, term->row), y = term->row;
      //
      fulldirty = 1;
      if (term->topline >= term->row) y += term->topline-term->row;
      while (--scry >= 0) {
        k8t_drawLine(term, 0, term->col, scry, y, 0);
        ++y;
      }
    }
    if (fulldirty) k8t_drawCopy(term, 0, 0, term->col, term->row);
    k8t_drawCursor(term, 0);
    term->lastDrawTime = term->clockTicks(term);
    if (term->setLastDrawTime) term->setLastDrawTime(term, term->lastDrawTime);
    term->wantRedraw = 0;
  }
}


K8TERM_API void k8t_drawTerm (K8Term *term, int forced) {
  if (term != NULL) {
    //fprintf(stderr, "k8t_drawTerm(%d) (%d)\n", forced, term->clockTicks(term));
    k8t_drawRegion(term, 0, 0, term->col, term->row, forced);
  }
}
