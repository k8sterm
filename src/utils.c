////////////////////////////////////////////////////////////////////////////////
// utilities
static void trimstr (char *s) {
  char *e;
  //
  while (*s && isspace(*s)) ++s;
  for (e = s+strlen(s); e > s; --e) if (!isspace(e[-1])) break;
  if (e <= s) *s = 0; else *e = 0;
}


static char *SPrintfVA (const char *fmt, va_list vaorig) {
  char *buf = NULL;
  int olen, len = 128;
  //
  buf = malloc(len);
  if (buf == NULL) { fprintf(stderr, "\nFATAL: out of memory!\n"); abort(); }
  for (;;) {
    char *nb;
    va_list va;
    //
    va_copy(va, vaorig);
    olen = vsnprintf(buf, len, fmt, va);
    va_end(va);
    if (olen >= 0 && olen < len) return buf;
    if (olen < 0) olen = len*2-1;
    nb = realloc(buf, olen+1);
    if (nb == NULL) { fprintf(stderr, "\nFATAL: out of memory!\n"); abort(); }
    buf = nb;
    len = olen+1;
  }
}


static __attribute__((format(printf,1,2))) char *SPrintf (const char *fmt, ...) {
  char *buf = NULL;
  va_list va;
  //
  va_start(va, fmt);
  buf = SPrintfVA(fmt, va);
  va_end(va);
  return buf;
}


K8TERM_API __attribute__((noreturn)) __attribute__((format(printf,1,2))) void k8t_die (const char *errstr, ...) {
  va_list ap;
  //
  fprintf(stderr, "FATAL: ");
  va_start(ap, errstr);
  vfprintf(stderr, errstr, ap);
  va_end(ap);
  fprintf(stderr, "\n");
  exit(EXIT_FAILURE);
}
