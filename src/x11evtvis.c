static void xevtcbexpose (XEvent *ev) {
  XExposeEvent *e = &ev->xexpose;
  //
  if (xw.state&K8T_WIN_REDRAW) {
    if (!e->count && curterm != NULL) {
      xw.state &= ~K8T_WIN_REDRAW;
      if (!updateTabBar) updateTabBar = -1;
      xdrawTabBar();
      k8t_tmFullDirty(curterm);
      k8t_drawTerm(curterm, 1);
      k8t_drawCopy(curterm, 0, 0, curterm->col, curterm->row);
      //if (!updateTabBar) updateTabBar = -1;
    }
    xclearunused();
  } else if (curterm != NULL) {
    int taby = (opt_tabposition==1 ? 0 : xw.h-xw.tabheight);
    int termy = (opt_tabposition==1 ? xw.tabheight : 0);
    int x0 = e->x/xw.cw, y0 = (e->y-termy)/xw.ch;
    int x1 = (e->x+e->width+xw.cw-1)/xw.cw, y1 = (e->y-termy+e->height+xw.ch-1)/xw.ch;
    //
    //fprintf(stderr, "x=%d; y=%d; w=%d; h=%d\n", e->x, e->y, e->width, e->height);
    //fprintf(stderr, "taby=%d; tabh=%d\n", taby, xw.tabheight);
    //fprintf(stderr, "x0=%d; y0=%d; x1=%d; y1=%d\n", x0, y0, x1, y1);
    //
    if (e->y <= taby+xw.tabheight && e->y+e->height >= taby) {
      //fprintf(stderr, "tabbar!\n");
      if (!updateTabBar) updateTabBar = -1;
    }
    //XCopyArea(xw.dpy, K8T_DATA(curterm)->picbuf, xw.win, dc.gc, e->x, e->y, e->width, e->height, e->x, e->y+(opt_tabposition==1?xw.height:0)));
    //k8t_drawCopy(curterm, 0, 0, curterm->col, curterm->row);
    //k8t_drawClear(curterm, x0, y0, x1-x0+1, y1-y0+1);
    K8T_LIMIT(x0, 0, curterm->col-1);
    K8T_LIMIT(x1, 0, curterm->col-1);
    K8T_LIMIT(y0, 0, curterm->row-1);
    K8T_LIMIT(y1, 0, curterm->row-1);
    //fprintf(stderr, "*:x0=%d; y0=%d; x1=%d; y1=%d\n", x0, y0, x1, y1);
    k8t_drawCopy(curterm, x0, y0, x1-x0+1, y1-y0+1);
    //xclearunused(); //FIXME: optimize this
  } else {
    if (!updateTabBar) updateTabBar = -1;
  }
  xdrawTabBar();
  //XFlush(xw.dpy);
}


static void xevtcbvisibility (XEvent *ev) {
  XVisibilityEvent *e = &ev->xvisibility;
  //
  if (e->state == VisibilityFullyObscured) xw.state &= ~K8T_WIN_VISIBLE;
  else if ((xw.state&K8T_WIN_VISIBLE) == 0) xw.state |= K8T_WIN_VISIBLE|K8T_WIN_REDRAW; /* need a full redraw for next Expose, not just a buf copy */
}


static void xevtcbunmap (XEvent *ev) {
  (void)ev;
  xw.state &= ~K8T_WIN_VISIBLE;
}


static void xseturgency (int add) {
  XWMHints *h = XGetWMHints(xw.dpy, xw.win);
  //
  h->flags = add ? (h->flags|XUrgencyHint) : (h->flags&~XUrgencyHint);
  XSetWMHints(xw.dpy, xw.win, h);
  XFree(h);
}


static void xevtcbfocus (XEvent *ev) {
  if (ev->type == FocusIn) {
    xw.state |= K8T_WIN_FOCUSED;
    xseturgency(0);
    k8t_tmSendFocusEvent(curterm, 1);
  } else {
    xw.state &= ~K8T_WIN_FOCUSED;
    k8t_tmSendFocusEvent(curterm, 0);
  }
  //k8t_drawTerm(curterm, 1);
  /*
  if (!updateTabBar) updateTabBar = -1;
  xdrawTabBar();
  */
  k8t_drawCursor(curterm, 1);
  //k8t_tmWantRedraw(curterm, 0);
  //k8t_drawCopy(curterm, 0, 0, curterm->col, curterm->row);
}


static void xevtcbresise (XEvent *e) {
  int col, row;
  //
  //if (e->xconfigure.width == 65535 || e->xconfigure.width == -1) e->xconfigure.width = xw.w;
  if (e->xconfigure.height == 65535 || e->xconfigure.height == -1) e->xconfigure.height = xw.h;
  //if ((short int)e->xconfigure.height < xw.ch) return;
  //
  if (e->xconfigure.width == xw.w && e->xconfigure.height == xw.h) return;
  xw.w = e->xconfigure.width;
  xw.h = e->xconfigure.height;
  col = xw.w/xw.cw;
  row = (xw.h-xw.tabheight)/xw.ch;
  //fprintf(stderr, "neww=%d; newh=%d; ch=%d; th=%d; col=%d; row=%d; ocol=%d; orow=%d\n", xw.w, xw.h, xw.ch, xw.tabheight, col, row, curterm->col, curterm->row);
  if (curterm != NULL && col == curterm->col && row == curterm->row) return;
  //
  for (int f = 0; f < term_count; ++f) {
    K8Term *t = term_array[f];
    /*int trs =*/ k8t_tmResize(t, col, row);
    //
    k8t_ttyResize(t);
    k8t_tmFullDirty(t);
    //if (trs && t == curterm) k8t_drawTerm(t, 1);
  }
  //
  termCreateXPixmap(curterm);
  XFreePixmap(xw.dpy, xw.pictab);
  xw.pictab = XCreatePixmap(xw.dpy, xw.win, xw.w, (xw.tabheight > 0 ? xw.tabheight : 1), XDefaultDepth(xw.dpy, xw.scr));
  updateTabBar = 1;
  xw.state |= K8T_WIN_REDRAW;
  //
  //k8t_drawTerm(curterm, 1);
}
