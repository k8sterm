////////////////////////////////////////////////////////////////////////////////
static int x2tab (int x) {
  if (x >= 0 && x < xw.w && xw.tabheight > 0) {
    x /= (xw.w/opt_tabcount)+firstVisibleTab;
    return (x >= 0 && x < term_count) ? x : -1;
  }
  return -1;
}


static void msTabSwitch (XEvent *e) {
  int tabn = x2tab(e->xbutton.x)+firstVisibleTab;
  //
  if (tabn >= 0 && tabn != termidx) switchToTerm(tabn, 1);
}


static void msTabScrollLeft (void) {
  if (firstVisibleTab > 0) {
    --firstVisibleTab;
    updateTabBar = 1;
    xdrawTabBar();
  }
}


static void msTabScrollRight (void) {
  int newidx = firstVisibleTab+1;
  //
  if (newidx > term_count-opt_tabcount) return;
  firstVisibleTab = newidx;
  updateTabBar = 1;
  xdrawTabBar();
}


////////////////////////////////////////////////////////////////////////////////
static void xdoMouseReport (K8Term *term, XEvent *e) {
  int x = K8T_X2COL(e->xbutton.x);
  int y = K8T_Y2ROW(term, e->xbutton.y);
  int button = e->xbutton.button-Button1+K8T_MOUSE_BUTTON_LEFT;
  int state, event;
  //
       if (e->xbutton.type == MotionNotify) event = K8T_MOUSE_EVENT_MOTION;
  else if (e->xbutton.type == ButtonPress) event = K8T_MOUSE_EVENT_DOWN;
  else if (e->xbutton.type == ButtonRelease) event = K8T_MOUSE_EVENT_UP;
  else return;
  //
  state =
    (e->xbutton.state&ShiftMask ? K8T_MOUSE_STATE_SHIFT : 0) |
    (e->xbutton.state&Mod1Mask ? K8T_MOUSE_STATE_ALT : 0) |
    (e->xbutton.state&ControlMask ? K8T_MOUSE_STATE_CTRL : 0) |
    (e->xbutton.state&Mod4Mask ? K8T_MOUSE_STATE_WIN : 0);
  //
  k8t_mouseReport(term, x, y, event, button, state);
}


static void getButtonInfo (K8Term *term, XEvent *e, int *b, int *x, int *y) {
  if (b != NULL) *b = e->xbutton.button;
  if (x != NULL) *x = K8T_X2COL(e->xbutton.x);
  if (y != NULL) *y = K8T_Y2ROW(term, e->xbutton.y);
  term->sel.b.x = (term->sel.by < term->sel.ey ? term->sel.bx : term->sel.ex);
  term->sel.b.y = K8T_MIN(term->sel.by, term->sel.ey);
  term->sel.e.x = (term->sel.by < term->sel.ey ? term->sel.ex : term->sel.bx);
  term->sel.e.y = K8T_MAX(term->sel.by, term->sel.ey);
}


static int exselect = 0; //EVIL!

static void xevtcbbpress (XEvent *e) {
  if (curterm == NULL) return;
  //
  if (xw.tabheight > 0) {
    if ((opt_tabposition == 0 && e->xbutton.y >= xw.h-xw.tabheight) ||
        (opt_tabposition != 0 && e->xbutton.y < xw.tabheight)) {
      switch (e->xbutton.button) {
        case Button1: // left
          msTabSwitch(e);
          break;
        case Button4: // wheel up
          msTabScrollLeft();
          break;
        case Button5: // wheel down
          msTabScrollRight();
          break;
      }
      return;
    }
  }
  // <shift|alt>+mouse: some cool shit
  if ((e->xbutton.state&(ShiftMask|Mod1Mask)) != 0) {
    switch (e->xbutton.button) {
      case Button1: // left
        exselect = e->xbutton.state&ControlMask;
        if (curterm->sel.bx != -1) k8t_tmDirty(curterm, curterm->sel.b.y, curterm->sel.e.y);
        curterm->sel.mode = 1;
        curterm->sel.b.y = curterm->sel.e.y = curterm->row+1;
        curterm->sel.ex = curterm->sel.bx = K8T_X2COL(e->xbutton.x);
        curterm->sel.ey = curterm->sel.by = K8T_Y2ROW(curterm, e->xbutton.y);
        //fprintf(stderr, "x=%d; y=%d\n", curterm->sel.bx, curterm->sel.by);
        k8t_drawTerm(curterm, 1);
        break;
      /*
      case Button3: // middle
        curterm->sel.bx = -1;
        k8t_selCopy(curterm);
        k8t_drawTerm(curterm, 1);
        break;
      */
      case Button4: // wheel up
        scrollHistory(curterm, 3);
        break;
      case Button5: // wheel down
        scrollHistory(curterm, -3);
        break;
    }
    return;
  }
  //
  if (K8T_ISSET(curterm, K8T_MODE_MOUSE)) xdoMouseReport(curterm, e);
}


static int isAlpha (int ch) {
  if (ch >= '0' && ch <= '9') return 1;
  if (ch >= 'A' && ch <= 'Z') return 1;
  if (ch >= 'a' && ch <= 'z') return 1;
  if (ch == '_' || ch == '$') return 1;
  if (ch > 255) return 1;
  return 0;
}


static void doSelect (K8TLine l, int (*pred)(int x, const K8TGlyph *g)) {
  while (curterm->sel.bx > 0 && pred(curterm->sel.bx-1, &l[curterm->sel.bx-1])) --curterm->sel.bx;
  while (curterm->sel.ex < curterm->col-1 && pred(curterm->sel.ex+1, &l[curterm->sel.ex+1])) ++curterm->sel.ex;
}


static int selpGfx (int x, const K8TGlyph *g) {
  (void)x;
  return (g->attr&K8T_ATTR_GFX);
}


static int selpWord (int x, const K8TGlyph *g) {
  (void)x;
  return ((g->attr&K8T_ATTR_GFX) == 0 && isAlpha(g->c[0]));
}


static int selpNonWord (int x, const K8TGlyph *g) {
  (void)x;
  return ((g->attr&K8T_ATTR_GFX) == 0 && g->c[0] != ' ' && !isAlpha(g->c[0]));
}


static int selpNonSpace (int x, const K8TGlyph *g) {
  (void)x;
  return ((g->attr&K8T_ATTR_GFX) == 0 && g->c[0] != ' ');
}


static void xevtcbbrelease (XEvent *e) {
  if (curterm == NULL) return;
  //
  switch (opt_tabposition) {
    case 0: // bottom
      if (e->xbutton.y >= xw.h-xw.tabheight) return;
      break;
    case 1: // top
      if (e->xbutton.y < xw.tabheight) return;
      break;
  }
  //
  if ((e->xbutton.state&ShiftMask) == 0 && !curterm->sel.mode) {
    if (K8T_ISSET(curterm, K8T_MODE_MOUSE)) xdoMouseReport(curterm, e);
    return;
  }
  //
  if (e->xbutton.button == Button2) {
    selpaste(curterm, XA_PRIMARY);
  } else if (e->xbutton.button == Button1) {
    curterm->sel.mode = 0;
    getButtonInfo(curterm, e, NULL, &curterm->sel.ex, &curterm->sel.ey); // this sets sel.b and sel.e
    //
    if (curterm->sel.bx == curterm->sel.ex && curterm->sel.by == curterm->sel.ey) {
      // single line, single char selection
      K8TTimeMSec now;
      k8t_tmDirtyMark(curterm, curterm->sel.ey, 2);
      curterm->sel.bx = -1;
      now = mclock_ticks();
      if (now-curterm->sel.tclick2 <= (unsigned)opt_tripleclick_timeout) {
        /* triple click on the line */
        curterm->sel.b.x = curterm->sel.bx = 0;
        curterm->sel.e.x = curterm->sel.ex = curterm->col;
        curterm->sel.b.y = curterm->sel.e.y = curterm->sel.ey;
      } else if (now-curterm->sel.tclick1 <= (unsigned)opt_doubleclick_timeout) {
        /* double click to select word */
        K8TLine l = k8t_selGet(curterm, curterm->sel.ey);
        if (l != NULL) {
          //FIXME: write better word selection code
          curterm->sel.bx = curterm->sel.ex;
          if (K8T_ISGFX(l[curterm->sel.bx].attr)) {
            doSelect(l, selpGfx);
          } else {
            if (exselect && l[curterm->sel.bx].c[0] != ' ') {
              // simple
              doSelect(l, selpNonSpace);
            } else if (isAlpha(l[curterm->sel.bx].c[0])) {
              doSelect(l, selpWord);
            } else if (l[curterm->sel.bx].c[0] == ' ') {
              // find first non-blank char
              //FIXME: this code smells
              int found = -1;
              // go right
              for (int x = curterm->sel.ex+1; x < curterm->col; ++x) {
                if (K8T_ISGFX(l[x].attr) || l[x].c[0] != ' ') {
                  if (!K8T_ISGFX(l[x].attr)) found = x;
                  curterm->sel.ex = x;
                  break;
                }
              }
              if (found < 0) {
                curterm->sel.ex = curterm->col-1;
                // go left
                for (int x = curterm->sel.bx-1; x >= 0; --x) {
                  if (K8T_ISGFX(l[x].attr) || l[x].c[0] != ' ') {
                    if (!K8T_ISGFX(l[x].attr)) found = x;
                    curterm->sel.bx = x;
                    break;
                  }
                }
                if (found < 0) curterm->sel.bx = 0;
              }
              if (found >= 0 && !K8T_ISGFX(l[found].attr)) {
                if (isAlpha(l[found].c[0])) doSelect(l, selpWord); else doSelect(l, selpNonWord);
              }
            } else {
              doSelect(l, selpNonWord);
            }
          }
          curterm->sel.b.x = curterm->sel.bx;
          curterm->sel.e.x = curterm->sel.ex;
          curterm->sel.b.y = curterm->sel.e.y = curterm->sel.ey;
        }
      }
      k8t_selCopy(curterm);
      k8t_drawTerm(curterm, 1);
    } else {
      // multiline or multichar selection
      k8t_selCopy(curterm);
    }
  }
  curterm->sel.tclick2 = curterm->sel.tclick1;
  curterm->sel.tclick1 = mclock_ticks();
  //k8t_drawTerm(1);
}


static void xevtcbbmotion (XEvent *e) {
  if (curterm == NULL) return;
  //
  switch (opt_tabposition) {
    case 0: // bottom
      if (e->xbutton.y >= xw.h-xw.tabheight) { changeXCursor(1); return; }
      break;
    case 1: // top
      if (e->xbutton.y < xw.tabheight) { changeXCursor(1); return; }
      break;
  }
  changeXCursor(0);
  //
  if (curterm->sel.mode) {
    int oldey = curterm->sel.ey, oldex = curterm->sel.ex;
    //
    getButtonInfo(curterm, e, NULL, &curterm->sel.ex, &curterm->sel.ey); // this sets sel.b and sel.e
    if (oldey != curterm->sel.ey || oldex != curterm->sel.ex) {
      int starty = K8T_MIN(oldey, curterm->sel.ey);
      int endy = K8T_MAX(oldey, curterm->sel.ey);
      //
      k8t_tmDirty(curterm, starty, endy);
      k8t_drawTerm(curterm, 1);
    }
    return;
  }
  if (K8T_ISSET(curterm, K8T_MODE_MOUSE) && (e->xmotion.state&(Button1Mask|Button2Mask|Button3Mask|Button4Mask|Button5Mask)) != 0 && (e->xmotion.state&ShiftMask) == 0) {
    xdoMouseReport(curterm, e);
  }
}
