#include <sys/time.h>
#include <sys/resource.h>

//#define K8STERM_DEBUG_EXEC


////////////////////////////////////////////////////////////////////////////////
// tty init
/*
static void dump (char c) {
  static int col;
  //
  fprintf(stderr, " %02x '%c' ", c, isprint(c)?c:'.');
  if (++col % 10 == 0) fprintf(stderr, "\n");
}
*/

typedef struct {
  char **args;
  int argc;
} ExecData;


static void execsh_free (ExecData *ed) {
  if (!ed) return;
  if (ed->args) {
    for (int f = 0; f < ed->argc; ++f) free(ed->args[f]);
    free(ed->args);
  }
  memset(ed, 0, sizeof(*ed));
}


static int execsh_prepare (ExecData *ed, const char *str) {
  if (!ed) return -1;
  #ifdef K8STERM_DEBUG_EXEC
  FILE *fo = stderr; //fopen("/tmp/z_k8sterm_exec_debug.log", "w");
  #endif

  memset(ed, 0, sizeof(*ed));
  char **args;
  int argc;
  if (str == NULL) {
    char *envshell = getenv("SHELL");
    if (envshell == NULL) envshell = "/bin/sh";
    K8T_DEFAULT(envshell, opt_shell);
    setenv("TERM", opt_term, 1);
    //args = (opt_cmd ? opt_cmd : (char *[]){envshell, "-i", NULL});
    if (opt_cmd && opt_cmd_count > 0) {
      #ifdef K8STERM_DEBUG_EXEC
      if (fo) fprintf(fo, "EXECSH: executing `opt_cmd` (%d): <%s>\n", opt_cmd_count, opt_cmd[0]);
      #endif
      args = calloc(opt_cmd_count+1, sizeof(char*));
      if (!args) return -1;
      argc = opt_cmd_count;
      for (int f = 0; f < opt_cmd_count; ++f) {
        #ifdef K8STERM_DEBUG_EXEC
        if (fo) fprintf(fo, "EXECSH:   arg #%d of %d: <%s>\n", f, opt_cmd_count, opt_cmd[f]);
        #endif
        args[f] = strdup(opt_cmd[f]);
        if (!args[f]) {
          for (int c = 0; c < f; ++c) free(args[c]);
          free(args);
          return -1;
        }
      }
      args[argc] = NULL;
    } else {
      args = calloc(3, sizeof(char*));
      if (!args) return -1;
      #ifdef K8STERM_DEBUG_EXEC
      if (fo) fprintf(fo, "EXECSH: executing `opt_cmd` shell: <%s>\n", envshell);
      #endif
      argc = 2;
      args[0] = strdup(envshell);
      if (!args[0]) { free(args); return -1; }
      args[1] = strdup("-i");
      if (!args[1]) { free(args[0]); free(args); return -1; }
      args[2] = NULL;
    }
  } else {
    #ifdef K8STERM_DEBUG_EXEC
    if (fo) fprintf(fo, "EXECSH: executing `str`: <%s>\n", str);
    #endif
    argc = 0;
    args = calloc(32768, sizeof(char *));
    if (args == NULL) return -1;
    while (*str) {
      if (argc >= 32766) break;
      const char *b;
      while (*str && isspace(*str)) ++str;
      if (!str[0]) break;
      b = str;
      while (*str && !isspace(*str)) {
        if (*str++ == '\\') {
          if (*str) ++str;
        }
      }
      args[argc] = calloc(str-b+1, 1);
      if (!args[argc]) {
        for (int f = 0; f < argc; ++f) free(args[f]);
        free(args);
        return -1;
      }
      memcpy(args[argc], b, str-b);
      #ifdef K8STERM_DEBUG_EXEC
      if (fo) fprintf(fo, "EXECSH:   argc=%d; argv=<%s>\n", argc, args[argc]);
      #endif
      ++argc;
    }
    args[argc] = NULL;
  }
  #ifdef K8STERM_DEBUG_EXEC
  if (fo && fo != stdout && fo != stderr) fclose(fo);
  #endif

  ed->args = args;
  ed->argc = argc;
  if (argc < 1) { execsh_free(ed); return -1; }
  return 0;
}


static __attribute__((noreturn)) void execsh (ExecData *ed) {
  if (!ed) exit(EXIT_FAILURE);
  if (ed->argc == 0) exit(EXIT_FAILURE);
  /*
  {
    extern char **environ;
    FILE *fo = fopen("z.log", "a");
    for (char **e = environ; *e; ++e) {
      fprintf(fo, "[%s]\n", *e);
    }
    fclose(fo);
  }
  */
  // close FDs
  {
    struct rlimit r;
    getrlimit(RLIMIT_NOFILE, &r);
    for (rlim_t f = 3; f < r.rlim_cur; ++f) close(f);
  }
  execvp(ed->args[0], ed->args);
  exit(EXIT_FAILURE);
}


static int k8t_ttyNew (K8Term *term) {
  int masterfd = -1;
  struct winsize w = {term->row, term->col, 0, 0};
  ExecData ed;
  if (execsh_prepare(&ed, K8T_DATA(term)->execcmd) != 0) {
    fprintf(stderr, "EXEC <%s> failed!\n", K8T_DATA(term)->execcmd);
    return -1;
  }
  switch (K8T_DATA(term)->pid = forkpty(&masterfd, NULL, NULL, &w)) {
    case -1: /* error */
      execsh_free(&ed);
      fprintf(stderr, "fork failed");
      if (masterfd != -1) close(masterfd);
      return -1;
    case 0: /* child */
      setsid(); /* create a new process group */
      execsh(&ed);
      break;
    default: /* master */
      execsh_free(&ed);
      term->cmdfd = masterfd;
      term->dead = 0;
      k8t_ttyResize(term);
      break;
  }
  return 0;
}

#if 0
static int k8t_ttyNew (K8Term *term) {
  int m, s;
  struct winsize w = {term->row, term->col, 0, 0};
  //
  if (openpty(&m, &s, NULL, NULL, &w) < 0) k8t_die("openpty failed: %s", strerror(errno));
  term->cmdfd = m;
  k8t_ttyResize(term);
  term->cmdfd = -1;
  switch (K8T_DATA(term)->pid = fork()) {
    case -1: /* error */
      fprintf(stderr, "fork failed");
      return -1;
    case 0: /* child */
      setsid(); /* create a new process group */
      dup2(s, STDIN_FILENO);
      dup2(s, STDOUT_FILENO);
      dup2(s, STDERR_FILENO);
      if (ioctl(s, TIOCSCTTY, NULL) < 0) k8t_die("ioctl TIOCSCTTY failed: %s", strerror(errno));
      close(s);
      close(m);
      execsh(K8T_DATA(term)->execcmd);
      break;
    default: /* master */
      close(s);
      term->cmdfd = m;
      term->dead = 0;
      k8t_ttyResize(term);
      break;
  }
  return 0;
}
#endif