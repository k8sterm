////////////////////////////////////////////////////////////////////////////////
// keyboard mapping
static const char *kmap (KeySym k, uint32_t state) {
  const char *res = NULL;
  //
  state &= ~Mod2Mask; // numlock
  for (int f = 0; f < keymap_used; ++f) {
    uint32_t mask = keymap[f].mask;
    //
    if (keymap[f].key == k && ((mask == XK_NO_MOD && !state) || state == mask)) {
      //fprintf(stderr, "kp: %d\n", K8T_ISSET(curterm, term, K8T_MODE_APPKEYPAD));
      if (!K8T_ISSET(curterm, K8T_MODE_APPKEYPAD)) {
        if (!keymap[f].kp) {
          //fprintf(stderr, "nokp! %d %s\n", keymap[f].kp, keymap[f].str+1);
          return keymap[f].str; // non-keypad hit
        }
        continue;
      }
      //fprintf(stderr, "kp! %d %s\n", keymap[f].kp, keymap[f].str+1);
      if (keymap[f].kp) return keymap[f].str; // keypad hit
      res = keymap[f].str; // kp mode, but non-kp mapping found
    }
  }
  return res;
}


static const char *kbind (KeySym k, uint32_t state) {
  state &= ~Mod2Mask; // numlock
  for (int f = 0; f < keybinds_used; ++f) {
    uint32_t mask = keybinds[f].mask;
    //
    if (keybinds[f].key == k && ((mask == XK_NO_MOD && !state) || state == mask)) return keybinds[f].str;
  }
  return NULL;
}


static KeySym do_keytrans (KeySym ks) {
  for (int f = 0; f < keytrans_used; ++f) if (keytrans[f].src == ks) return keytrans[f].dst;
  return ks;
}


static void cmdline_closequeryexec (K8Term *term, K8TCmdLine *cmdline, int cancelled) {
  (void)term;
  if (!cancelled) {
    char *rep = cmdline->cmdline+cmdline->cmdreslen;
    //
    trimstr(rep);
    if (rep[0] && (tolower(rep[0]) == 'y' || tolower(rep[0]) == 't')) {
      closeRequestComes = 2;
      return;
    }
  }
  closeRequestComes = 0;
}


static void eskdump (const char *kstr) {
  if (kstr != NULL && kstr[0]) {
    if (K8T_ISSET(curterm, K8T_MODE_APPKEYPAD)) fprintf(stderr, "<KP>");
    while (*kstr) {
      uint32_t c = (uint32_t)(*kstr++);
      //
      if (c != 32 && isprint(c)) fputc(c, stderr);
      else if (c == '\n') fprintf(stderr, "(\\n)");
      else if (c == '\r') fprintf(stderr, "(\\r)");
      else if (c == 0x1b) fprintf(stderr, "(^[)");
      else fprintf(stderr, "(%u)", c);
    }
  }
  fputc('\n', stderr);
}


static void xevtcbkpress (XEvent *ev) {
  XKeyEvent *e = &ev->xkey;
  KeySym ksym = NoSymbol;
  const char *kstr;
  int len;
  Status status;
  char buf[32];
  //
  if (curterm == NULL) return;
  //
  if (!ptrBlanked && opt_ptrblank > 0) xblankPointer();
  //
  //if (len < 1) len = XmbLookupString(xw.xic, e, buf, sizeof(buf), &ksym, &status);
  if ((len = Xutf8LookupString(xw.xic, e, buf, sizeof(buf), &ksym, &status)) > 0) buf[len] = 0;
  // leave only known mods
  e->state &= (Mod1Mask|Mod4Mask|ControlMask|ShiftMask);
//#define DUMP_KEYSYMS
#ifdef DUMP_KEYSYMS
  {
    const char *ksname = XKeysymToString(ksym);
    fprintf(stderr, "utf(%d):[%s] (%s) 0x%08x\n", len, len>=0?buf:"<shit>", ksname, (unsigned int)e->state);
  }
#endif
  if ((kstr = kbind(ksym, e->state)) != NULL) {
    // keybind found
    executeCommands(kstr);
    return;
  }
  //
  if (K8T_DATA(curterm)->cmdline.cmdMode != K8T_CMDMODE_NONE) {
    if (tcmdlProcessKeys(curterm, &K8T_DATA(curterm)->cmdline, do_keytrans(ksym), buf, len, e)) return;
  }
  //
  if ((kstr = kmap(do_keytrans(ksym), e->state)) != NULL) {
    /*
    {
      const char *ksname = XKeysymToString(ksym);
      fprintf(stderr, "0x%04x*0x%04x: utf(%d):[%s] (%s) 0x%08x\n", (unsigned)ksym, (unsigned)do_keytrans(ksym), len, len>=0?buf:"<shit>", ksname, (unsigned int)e->state);
      fprintf(stderr, " slen=%u", strlen(kstr));
      for (const char *p = kstr; *p; ++p) {
        if (*p <= ' ' || *p == 127) fprintf(stderr, " {%u}", (unsigned)*p); else fprintf(stderr, " {%c}", (char)*p);
      }
      fprintf(stderr, "\n");
    }
    */
    if (kstr[0]) {
      k8t_tmUnshowHistory(curterm);
      if (curterm->dumpeskeys) {
        if (!isprint(kstr[0]) || kstr[1]) {
          fprintf(stderr, "KEY: ");
          eskdump(kstr);
        }
      }
      k8t_ttyWriteStr(curterm, kstr);
    }
  } else {
    int meta = (e->state&Mod1Mask);
    int shift = (e->state&ShiftMask);
    //int ctrl = (e->state&ControlMask);
    //
    if (K8T_DATA(curterm)->waitkeypress) {
      switch (ksym) {
        case XK_Return:
        case XK_KP_Enter:
        case XK_space:
        case XK_Escape:
          K8T_DATA(curterm)->waitkeypress = 0;
          curterm->dead = 1;
          if (xw.paste_term == curterm) xw.paste_term = NULL;
          break;
      }
    } else {
      switch (ksym) {
        case XK_Return:
          k8t_tmUnshowHistory(curterm);
          if (meta) {
            k8t_ttyWriteStr(curterm, "\x1b\x0a");
          } else {
            if (K8T_ISSET(curterm, K8T_MODE_CRLF)) k8t_ttyWriteStr(curterm, "\r\n"); else k8t_ttyWriteStr(curterm, "\r");
          }
          break;
        default:
          if (!curterm->dead && len > 0) {
            k8t_tmUnshowHistory(curterm);
            KeySym ksx = (meta ? XLookupKeysym(e, 0) : NoSymbol);
            if (meta && ((ksx >= 'A' && ksx <= 'Z') || (ksx >= 'a' && ksx <= 'z'))) {
              // alt+key: ignore current xcb language
              // but with shift key we have to transform case
              if (shift) {
                if (ksx >= 'a' && ksx <= 'z') ksx -= 32; // upcase
              } else {
                if (ksx >= 'A' && ksx <= 'Z') ksx += 32; // locase
              }
              buf[0] = '\x1b';
              buf[1] = ksx;
              k8t_ttyWrite(curterm, buf, 2);
            } else {
              if (meta && len == 1) k8t_ttyWriteStr(curterm, "\x1b");
              k8t_ttyWrite(curterm, buf, len);
            }
          }
          break;
      }
    }
  }
}
