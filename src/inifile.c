////////////////////////////////////////////////////////////////////////////////
typedef const char * (*IniHandlerFn) (const char *optname, const char *fmt, char *argstr, void *udata);


typedef struct {
  const char *name;
  const char *fmt;
  void *udata;
  IniHandlerFn fn;
} IniCommand;


static const char *inifnGenericOneArg (const char *optname, const char *fmt, char *argstr, void *udata) {
  (void)optname;
  return iniParseArguments(argstr, fmt, udata);
}


static const char *inifnGenericOneStr (const char *optname, const char *fmt, char *argstr, void *udata) {
  (void)optname;
  char *s = NULL;
  const char *err = iniParseArguments(argstr, fmt, &s);
  //
  if (err != NULL) return err;
  if ((s = strdup(s)) == NULL) return "out of memory";
  if (udata) {
    char **ustr = (char **)udata;
    //
    if (*ustr) free(*ustr);
    *ustr = s;
  }
  return NULL;
}


static const char *inifnTabPosition (const char *optname, const char *fmt, char *argstr, void *udata) {
  (void)optname; (void)udata;
  int newpos = -1;
  const char *err = NULL;
  //
  while (argstr[0]) {
    char *s = NULL;
    //
    if ((err = iniParseArguments(argstr, "R-", &argstr)) != NULL) return err;
    if (!argstr[0]) break;
    //
    if ((err = iniParseArguments(argstr, "s!-R-", &s, &argstr)) != NULL) return err;
    if (tolower(s[0]) == 't') newpos = 1;
    else if (tolower(s[0]) == 'b') newpos = 0;
    else if ((err = iniParseArguments(s, fmt, &newpos)) != NULL) return err;
  }
  //
  if (newpos == -1) return "invalid tabbar position";
  opt_tabposition = newpos;
  return NULL;
}


static int dummyopt;

static const IniCommand iniCommands[] = {
  {"term", "s!-", &opt_term, inifnGenericOneStr},
  {"class", "s!-", &opt_class, inifnGenericOneStr},
  {"title", "s!-", &opt_title, inifnGenericOneStr},
  {"fontnorm", "s!-", &opt_fontnorm, inifnGenericOneStr},
  {"fontbold", "s!-", &opt_fontbold, inifnGenericOneStr},
  {"fonttab", "s!-", &opt_fonttab, inifnGenericOneStr},
  {"shell", "s!-", &opt_shell, inifnGenericOneStr},
  {"doubleclicktimeout", "i{0,10000}", &opt_doubleclick_timeout, inifnGenericOneArg},
  {"tripleclicktimeout", "i{0,10000}", &opt_tripleclick_timeout, inifnGenericOneArg},
  {"tabsize", "i{1,256}", &opt_tabsize, inifnGenericOneArg},
  {"defaultfg", "i{0,511}", &defaultFG, inifnGenericOneArg},
  {"defaultbg", "i{0,511}", &defaultBG, inifnGenericOneArg},
  {"defaultcursorfg", "i{0,511}", &defaultCursorFG, inifnGenericOneArg},
  {"defaultcursorbg", "i{0,511}", &defaultCursorBG, inifnGenericOneArg},
  {"defaultcursorfg1", "i{0,511}", &defaultCursorFG1, inifnGenericOneArg},
  {"defaultcursorbg1", "i{-1,511}", &defaultCursorBG1, inifnGenericOneArg},
  {"defaultinactivecursorfg", "i{0,511}", &defaultCursorInactiveFG, inifnGenericOneArg},
  {"defaultinactivecursorbg", "i{-1,511}", &defaultCursorInactiveBG, inifnGenericOneArg},
  {"defaultboldfg", "i{-1,511}", &defaultBoldFG, inifnGenericOneArg},
  {"defaultunderlinefg", "i{-1,511}", &defaultUnderlineFG, inifnGenericOneArg},
  {"normaltabfg", "i{0,511}", &normalTabFG, inifnGenericOneArg},
  {"normaltabbg", "i{0,511}", &normalTabBG, inifnGenericOneArg},
  {"activetabfg", "i{0,511}", &activeTabFG, inifnGenericOneArg},
  {"activetabbg", "i{0,511}", &activeTabBG, inifnGenericOneArg},
  {"maxhistory", "i{0,65535}", &opt_maxhistory, inifnGenericOneArg},
  {"ptrblank", "i{0,65535}", &opt_ptrblank, inifnGenericOneArg},
  {"tabcount", "i{1,128}", &opt_tabcount, inifnGenericOneArg},
  {"tabposition", "i{0,1}", &opt_tabposition, inifnTabPosition},
  {"drawtimeout", "i{5,30000}", &opt_drawtimeout, inifnGenericOneArg},
  {"audiblebell", "b", &opt_audiblebell, inifnGenericOneArg},
  {"urgentbell", "b", &opt_urgentbell, inifnGenericOneArg},
  {"cursorblink", "i{0,10000}", &opt_cursorBlink, inifnGenericOneArg},
  {"cursorblinkinactive", "b", &opt_cursorBlinkInactive, inifnGenericOneArg},
  {"drawunderline", "b", &opt_drawunderline, inifnGenericOneArg},
  {"ignoreclose", "i{-1,1}", &opt_ignoreclose, inifnGenericOneArg},
  {"maxdrawtimeout", "i{10,60000}", &opt_maxdrawtimeout, inifnGenericOneArg},
  {"swapdrawtimeout", "i{10,60000}", &/*opt_swapdrawtimeout*/dummyopt, inifnGenericOneArg},
  {"fastredraw", "b", &opt_fastredraw, inifnGenericOneArg},
  {"fastupdate", "b", &/*opt_fastupdate*/dummyopt, inifnGenericOneArg},
  {"scrollclear", "i{-1,2}", &opt_scrollclear, inifnGenericOneArg},
  {"tabellipsis", "i{0,3}", &opt_tabellipsis, inifnGenericOneArg},
  {NULL, NULL, NULL, NULL}
};


#define MISC_CMD_NONE  ((const char *)-1)


// NULL: command processed; MISC_CMD_NONE: unknown command; !NULL: error
static const char *processMiscCmds (const char *optname, char *argstr) {
  const char *err = NULL;
  //
  if (strcasecmp(optname, "unimap") == 0) {
    int uni, ch;
    char *alt = NULL;
    //
    if (iniParseArguments(argstr, "R!-", &argstr) == NULL) {
      if (strcasecmp(argstr, "reset") == 0 || strcasecmp(argstr, "clear") == 0) {
        if (unimap) free(unimap);
        unimap = NULL;
        return NULL;
      }
    }
    //unimap 0x2592 0x61 alt
    if ((err = iniParseArguments(argstr, "i{0,65535}i{0,255}|s!-", &uni, &ch, &alt)) != NULL) return err;
    if (alt != NULL && strcasecmp(alt, "alt") != 0) return "invalid unimap";
    if (unimap == NULL) {
      if ((unimap = calloc(65536, sizeof(unimap[0]))) == NULL) return "out of memory";
    }
    if (alt != NULL && ch == 0) alt = NULL;
    if (alt != NULL && ch < 96) return "invalid unimap";
    unimap[uni] = ch;
    if (alt != NULL) unimap[uni] |= 0x8000;
    return NULL;
  }
  //
  if (strcasecmp(optname, "keytrans") == 0) {
    char *src = NULL, *dst = NULL;
    //
    if (iniParseArguments(argstr, "R!-", &argstr) == NULL) {
      if (strcasecmp(argstr, "reset") == 0 || strcasecmp(argstr, "clear") == 0) {
        keytrans_reset();
        return NULL;
      }
    }
    if ((err = iniParseArguments(argstr, "s!-s!-", &src, &dst)) != NULL) return err;
    keytrans_add(src, dst);
    return NULL;
  }
  //
  if (strcasecmp(optname, "keybind") == 0) {
    char *key = NULL, *act = NULL;
    if (iniParseArguments(argstr, "R!-", &argstr) == NULL) {
      if (strcasecmp(argstr, "reset") == 0 || strcasecmp(argstr, "clear") == 0) {
        keybinds_reset();
        return NULL;
      }
    }
    if ((err = iniParseArguments(argstr, "s!-R!", &key, &act)) != NULL) return err;
    if (act[0] == '"') {
      char *t;
      //
      if ((err = iniParseArguments(act, "s!", &t)) != NULL) return err;
      act = t;
    }
    keybind_add(key, act);
    return NULL;
  }
  //
  if (strcasecmp(optname, "keymap") == 0) {
    char *key = NULL, *str = NULL;
    //
    if (iniParseArguments(argstr, "R!-", &argstr) == NULL) {
      if (strcasecmp(argstr, "reset") == 0 || strcasecmp(argstr, "clear") == 0) {
        keymap_reset();
        return NULL;
      }
    }
    if ((err = iniParseArguments(argstr, "s!-s!-", &key, &str)) != NULL) return err;
    keymap_add(key, str);
    return NULL;
  }
  //
  return MISC_CMD_NONE;
}


#define INI_LINE_SIZE  (32768)

// <0: file not found
// >0: file loading error
//  0: ok
static int loadConfig (const char *fname) {
  int inifelse = 0; // 0: not; 1: doing true; 2: doing false; -1: waiting else/endif; -2: waiting endif
  FILE *fi = fopen(fname, "r");
  const char *err = NULL;
  char *line;
  int lineno = 0;
  //
  if (fi == NULL) return -1;
  if ((line = malloc(INI_LINE_SIZE)) == NULL) { err = "out of memory"; goto quit; }
  //
  while (fgets(line, INI_LINE_SIZE-1, fi) != NULL) {
    char *optname, *argstr, *d;
    int goodoption = 0;
    //
    ++lineno;
    line[INI_LINE_SIZE-1] = 0;
    // get option name
    for (optname = line; *optname && isspace(*optname); ++optname) ;
    if (!optname[0] || optname[0] == '#') continue; // comment
    if (!isalnum(optname[0])) { err = "invalid option name"; goto quit; }
    d = argstr = optname;
    while (*argstr) {
      if (!argstr[0] || isspace(argstr[0])) break;
      if (!isalnum(argstr[0]) && argstr[0] != '_' && argstr[0] != '.') { err = "invalid option name"; goto quit; }
      if (argstr[0] != '_') *d++ = tolower(*argstr);
      ++argstr;
    }
    if (*argstr) ++argstr;
    *d = 0;
    if (!isalnum(optname[0])) { err = "invalid option name"; goto quit; }
    //
    if (strcasecmp(optname, "ifterm") == 0) {
      char *val;
      //
      if (inifelse != 0) { err = "nested ifs are not allowed"; goto quit; }
      inifelse = -1;
      if ((err = iniParseArguments(argstr, "s", &val)) != NULL) goto quit;
      if (strcasecmp(opt_term, val) == 0) inifelse = 1;
      continue;
    }
    if (strcasecmp(optname, "else") == 0) {
      switch (inifelse) {
        case -1: inifelse = 2; break;
        case 2: case -2: case 0: err = "else without if"; goto quit;
        case 1: inifelse = -2; break;
      }
      continue;
    }
    if (strcasecmp(optname, "endif") == 0) {
      switch (inifelse) {
        case -1: case -2: case 1: case 2: inifelse = 0; break;
        case 0: err = "endif without if"; goto quit;
      }
      continue;
    }
    //
    if (inifelse < 0) {
      //trimstr(argstr);
      //fprintf(stderr, "skip: [%s]\n", argstr);
      continue;
    }
    if (opt_term_locked && strcasecmp(optname, "term") == 0) continue; // termname given in command line
    // ok, we have option name in `optname` and arguments in `argstr`
    if (strncmp(optname, "color.", 6) == 0) {
      int n = 0;
      char *s = NULL;
      //
      optname += 6;
      if (!optname[0]) { err = "invalid color option"; goto quit; }
      while (*optname) {
        if (!isdigit(*optname)) { err = "invalid color option"; goto quit; }
        n = (n*10)+(optname[0]-'0');
        ++optname;
      }
      if (n < 0 || n > 511) { err = "invalid color index"; goto quit; }
      //
      if ((err = iniParseArguments(argstr, "s!-", &s)) != NULL) goto quit;
      if ((s = strdup(s)) == NULL) { err = "out of memory"; goto quit; }
      if (opt_colornames[n] != NULL) free(opt_colornames[n]);
      opt_colornames[n] = s;
      continue;
    }
    //
    if ((err = processMiscCmds(optname, argstr)) != MISC_CMD_NONE) {
      if (err != NULL) goto quit;
      continue;
    } else {
      err = NULL;
    }
    //
    for (int f = 0; iniCommands[f].name != NULL; ++f) {
      if (strcmp(iniCommands[f].name, optname) == 0) {
        if ((err = iniCommands[f].fn(optname, iniCommands[f].fmt, argstr, iniCommands[f].udata)) != NULL) goto quit;
        goodoption = 1;
        break;
      }
    }
    if (!goodoption) {
      fprintf(stderr, "ini error at line %d: unknown option '%s'!\n", lineno, optname);
    }
  }
quit:
  if (line != NULL) free(line);
  fclose(fi);
  if (err == NULL && inifelse != 0) err = "if without endif";
  if (err != NULL) k8t_die("ini error at line %d: %s", lineno, err);
  return 0;
}


static void initDefaultOptions (void) {
  opt_title = strdup("sterm");
  opt_class = strdup("sterm");
  opt_term = strdup(TNAME);
  opt_fontnorm = strdup(FONT);
  opt_fontbold = strdup(FONTBOLD);
  opt_fonttab = strdup(FONTTAB);
  opt_shell = strdup(SHELL);
  //
  memset(opt_colornames, 0, sizeof(opt_colornames));
  for (size_t f = 0; f < K8T_ARRLEN(defcolornames); ++f) opt_colornames[f] = strdup(defcolornames[f]);
  for (size_t f = 0; f < K8T_ARRLEN(defextcolornames); ++f) opt_colornames[f+256] = strdup(defextcolornames[f]);
  //
  keytrans_add("KP_Home", "Home");
  keytrans_add("KP_Left", "Left");
  keytrans_add("KP_Up", "Up");
  keytrans_add("KP_Right", "Right");
  keytrans_add("KP_Down", "Down");
  keytrans_add("KP_Prior", "Prior");
  keytrans_add("KP_Next", "Next");
  keytrans_add("KP_End", "End");
  keytrans_add("KP_Begin", "Begin");
  keytrans_add("KP_Insert", "Insert");
  keytrans_add("KP_Delete", "Delete");
  //
  keybind_add("shift+Insert", "PastePrimary");
  keybind_add("alt+Insert", "PasteCliboard");
  keybind_add("ctrl+alt+t", "NewTab");
  keybind_add("ctrl+alt+Left", "SwitchToTab prev");
  keybind_add("ctrl+alt+Right", "SwitchToTab next");
  //
  keymap_add("BackSpace", "\177");
  keymap_add("Insert", "\x1b[2~");
  keymap_add("Delete", "\x1b[3~");
  keymap_add("Home", "\x1b[1~");
  keymap_add("End", "\x1b[4~");
  keymap_add("Prior", "\x1b[5~");
  keymap_add("Next", "\x1b[6~");
  keymap_add("F1", "\x1bOP");
  keymap_add("F2", "\x1bOQ");
  keymap_add("F3", "\x1bOR");
  keymap_add("F4", "\x1bOS");
  keymap_add("F5", "\x1b[15~");
  keymap_add("F6", "\x1b[17~");
  keymap_add("F7", "\x1b[18~");
  keymap_add("F8", "\x1b[19~");
  keymap_add("F9", "\x1b[20~");
  keymap_add("F10", "\x1b[21~");
  keymap_add("Up", "\x1bOA");
  keymap_add("Down", "\x1bOB");
  keymap_add("Right", "\x1bOC");
  keymap_add("Left", "\x1bOD");
}
