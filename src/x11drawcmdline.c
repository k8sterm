static void xdrawcmdline (K8Term *term, K8TCmdLine *cmdline, int scry) {
  K8TGlyph base;
  int cpos = cmdline->cmdofs, bc = 0, x, sx;
  int back = (cmdline->cmdMode == K8T_CMDMODE_INPUT ? 21 : 124);
  int rslen = (cmdline->cmdMode == K8T_CMDMODE_INPUT ? cmdline->cmdreslen : 32760);
  //
  base.attr = K8T_ATTR_NORMAL|K8T_ATTR_FGSET|K8T_ATTR_BGSET;
  base.fg = 255;
  base.bg = back;
  base.state = 0;
  // hilighted
  for (sx = x = 0; x < term->col && cmdline->cmdline[cpos] && cpos < rslen; ++x) {
    int l = k8t_UTF8Size(cmdline->cmdline+cpos);
    //
    if (bc+l > K8T_DRAW_BUF_SIZ) {
      k8t_drawString(term, term->drawbuf, &base, sx, scry, x-sx, bc);
      bc = 0;
      sx = x;
    }
    memcpy(term->drawbuf+bc, cmdline->cmdline+cpos, l);
    cpos += l;
    bc += l;
  }
  if (bc > 0) k8t_drawString(term, term->drawbuf, &base, sx, scry, x-sx, bc);
  // user input
  bc = 0;
  base.fg = 220;
  for (sx = x; x < term->col && cmdline->cmdline[cpos]; ++x) {
    int l = k8t_UTF8Size(cmdline->cmdline+cpos);
    //
    if (bc+l > K8T_DRAW_BUF_SIZ) {
      k8t_drawString(term, term->drawbuf, &base, sx, scry, x-sx, bc);
      bc = 0;
      sx = x;
    }
    memcpy(term->drawbuf+bc, cmdline->cmdline+cpos, l);
    cpos += l;
    bc += l;
  }
  if (bc > 0) k8t_drawString(term, term->drawbuf, &base, sx, scry, x-sx, bc);
  //
  if (x < term->col && cmdline->cmdMode == K8T_CMDMODE_INPUT) {
    base.bg = base.fg;
    base.fg = back;
    k8t_drawString(term, " ", &base, x, scry, 1, 1);
    ++x;
  }
  //
  if (x < term->col) {
    base.fg = 255;
    base.bg = back;
    memset(term->drawbuf, ' ', K8T_DRAW_BUF_SIZ);
    while (x < term->col) {
      sx = x;
      x += K8T_DRAW_BUF_SIZ;
      if (x > term->col) x = term->col;
      k8t_drawString(term, term->drawbuf, &base, sx, scry, x-sx, x-sx);
    }
  }
  //
  k8t_drawCopy(term, 0, scry, term->col, 1);
}
