////////////////////////////////////////////////////////////////////////////////
// x11 drawing and utils
static void changeXCursor (int intabbar) {
  Cursor newcur = (intabbar ? xw.defcursor : xw.cursor);
  //
  if (xw.lastcursor != newcur) {
    xw.lastcursor = newcur;
    if (!ptrBlanked) {
      XDefineCursor(xw.dpy, xw.win, xw.lastcursor);
      XFlush(xw.dpy);
    }
  }
}


static void xblankPointer (void) {
  if (!ptrBlanked && xw.blankPtr != None) {
    ptrBlanked = 1;
    XDefineCursor(xw.dpy, xw.win, xw.blankPtr);
    XFlush(xw.dpy);
  }
}


static void xunblankPointer (void) {
  if (ptrBlanked && xw.cursor != None) {
    ptrBlanked = 0;
    XDefineCursor(xw.dpy, xw.win, xw.lastcursor);
    XFlush(xw.dpy);
    ptrLastMove = mclock_ticks();
  }
}


static void xclearunused (void) {
  if (xw.tabheight > 0 && curterm != NULL) {
    int unh = xw.h-(curterm->row*xw.ch)-xw.tabheight;
    //
    if (unh > 0) {
      switch (opt_tabposition) {
        case 0: // bottom
          XClearArea(xw.dpy, xw.win, 0, xw.h-xw.tabheight-unh, xw.w, unh, False);
          break;
        case 1: // top
          XClearArea(xw.dpy, xw.win, 0, xw.tabheight+(curterm->row*xw.ch), xw.w, unh, False);
          break;
      }
    }
  }
}


/*
static int xisyinunused (int y) {
  if (xw.tabheight > 0 && curterm != NULL) {
    int unh = xw.h-(curterm->row*xw.ch)-xw.tabheight;
    //
    if (unh > 0) {
      switch (opt_tabposition) {
        case 0: // bottom
          return (y >= xw.h-xw.tabheight-unh && y < xw.h-xw.tabheight);
          break;
        case 1: // top
          return (y >= xw.tabheight+(curterm->row*xw.ch)t);
          break;
      }
    }
  }
  return 0;
}
*/
