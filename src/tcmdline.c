////////////////////////////////////////////////////////////////////////////////
static char **cmdHistory = NULL;
static int cmdHistoryCount = 0;
static int cmdHistoryCurrent = -1; // <0: command line
static char cmdLineCur[K8T_UTF_SIZ*CMDLINE_SIZE];


static void cmdAddToHistory (const char *cmd) {
  if (cmd == NULL) return;
  while (*cmd && isspace(*cmd)) ++cmd;
  if (!cmd[0]) return;
  // check if we already have such command
  for (int f = 0; f < cmdHistoryCount; ++f) {
    if (strcmp(cmdHistory[f], cmd) == 0) {
      // bingo! move it down
      char *c = cmdHistory[f];
      for (int c = f; c < cmdHistoryCount-1; ++c) cmdHistory[c] = cmdHistory[c+1];
      cmdHistory[cmdHistoryCount-1] = c;
      return;
    }
  }
  // not found, add new
  if (cmdHistoryCount < 128) {
    // allocate memory for new command
    ++cmdHistoryCount;
    cmdHistory = realloc(cmdHistory, sizeof(char *)*cmdHistoryCount);
  } else {
    // remove top command, add this one
    free(cmdHistory[0]);
    for (int f = 1; f < cmdHistoryCount; ++f) cmdHistory[f-1] = cmdHistory[f];
  }
  cmdHistory[cmdHistoryCount-1] = strdup(cmd);
}


////////////////////////////////////////////////////////////////////////////////
static void tcmdlinedirty (K8Term *term, K8TCmdLine *cmdline) {
  (void)cmdline;
  if (term != NULL) {
    k8t_tmDirtyMark(term, term->row-term->topline-1, 2);
    k8t_tmWantRedraw(term, 1);
  }
}


static void tcmdlinefixofs (K8Term *term, K8TCmdLine *cmdline) {
  int len = k8t_UTF8strlen(cmdline->cmdline);
  int ofs = len-(term->col-1);
  if (ofs < 0) ofs = 0;
  for (cmdline->cmdofs = 0; ofs > 0; --ofs) cmdline->cmdofs += k8t_UTF8Size(cmdline->cmdline+cmdline->cmdofs);
  tcmdlinedirty(term, cmdline);
}


static void tcmdlinehide (K8Term *term, K8TCmdLine *cmdline) {
  if (cmdline->cmdMode != K8T_CMDMODE_NONE && term == xw.paste_term) k8stx_finish_paste();
  cmdHistoryCurrent = -1;
  cmdline->cmdMode = K8T_CMDMODE_NONE;
  cmdline->cmdcurtabc = NULL;
  tcmdlinedirty(term, cmdline);
}


// utf-8
static void tcmdlinemsg (K8Term *term, K8TCmdLine *cmdline, const char *msg) {
  if (msg != NULL) {
    int ofs = 0;
    //
    cmdHistoryCurrent = -1;
    cmdline->cmdMode = K8T_CMDMODE_MESSAGE;
    cmdline->cmdofs = 0;
    cmdline->cmdtabpos = -1;
    cmdline->cmdcurtabc = NULL;
    //
    while (*msg) {
      int len = k8t_UTF8Size(msg);
      //
      if (len < 1 || ofs+len >= (int)sizeof(cmdline->cmdline)-1) break;
      memcpy(cmdline->cmdline+ofs, msg, len);
      ofs += len;
      msg += len;
    }
    //
    cmdline->cmdline[ofs] = 0;
    tcmdlinedirty(term, cmdline);
  }
}


static __attribute__((format(printf,3,4))) void tcmdlinemsgf (K8Term *term, K8TCmdLine *cmdline, const char *fmt, ...) {
  char buf[128];
  char *xbuf = buf;
  int size = sizeof(buf)-1;
  va_list ap;
  //
  for (;;) {
    int n;
    char *t;
    //
    va_start(ap, fmt);
    n = vsnprintf(xbuf, size, fmt, ap);
    va_end(ap);
    if (n > -1 && n < size) break;
    if (n > -1) size = n+1; else size += 4096;
    if (xbuf == buf) xbuf = NULL;
    if ((t = realloc(xbuf, size)) == NULL) { if (xbuf) free(xbuf); return; }
    xbuf = t;
  }
  tcmdlinemsg(term, cmdline, xbuf);
  if (xbuf != buf) free(xbuf);
}


static void tcmdlineinitex (K8Term *term, K8TCmdLine *cmdline, const char *msg) {
  if (term == xw.paste_term) k8stx_finish_paste();
  cmdline->cmdMode = K8T_CMDMODE_INPUT;
  cmdline->cmdofs = 0;
  cmdline->cmdline[0] = 0;
  cmdline->cmdc[0] = 0;
  cmdline->cmdcl = 0;
  cmdline->cmdtabpos = -1;
  cmdline->cmdcurtabc = NULL;
  cmdline->cmdreslen = 0;
  cmdline->cmdexecfn = NULL;
  if (msg != NULL && msg[0]) {
    strcpy(cmdline->cmdline, msg);
    cmdline->cmdreslen = strlen(cmdline->cmdline);
  }
  tcmdlinefixofs(term, cmdline);
}


static void tcmdlineinit (K8Term *term, K8TCmdLine *cmdline) {
  tcmdlineinitex(term, cmdline, NULL);
}


static void tcmdlinechoplast (K8Term *term, K8TCmdLine *cmdline) {
  if (cmdline->cmdcl != 0) {
    cmdline->cmdcl = 0;
  } else {
    if ((int)strlen(cmdline->cmdline) > cmdline->cmdreslen) {
      k8t_UTF8ChopLast(cmdline->cmdline);
    }
  }
  tcmdlinefixofs(term, cmdline);
}


// utf-8
static void tcmdaddchar (K8Term *term, K8TCmdLine *cmdline, const char *s) {
  int len = k8t_UTF8Size(s);
  if (len > 0) {
    int slen = strlen(cmdline->cmdline);
    if (slen+len < (int)sizeof(cmdline->cmdline)) {
      memcpy(cmdline->cmdline+slen, s, len);
      cmdline->cmdline[slen+len] = 0;
      tcmdlinefixofs(term, cmdline);
    }
  }
}


// externally called (from selection processing code)
static void tcmdput (K8Term *term, K8TCmdLine *cmdline, const char *s, int len) {
  cmdHistoryCurrent = -1;
  while (len-- > 0) {
    int ok;
    cmdline->cmdc[cmdline->cmdcl++] = *s++;
    cmdline->cmdc[cmdline->cmdcl] = 0;
    if ((ok = k8t_UTF8IsFull(cmdline->cmdc, cmdline->cmdcl)) != 0 || cmdline->cmdcl == K8T_UTF_SIZ) {
      if (ok) tcmdaddchar(term, cmdline, cmdline->cmdc);
      cmdline->cmdcl = 0;
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
static void tcmdlHistoryMove (K8Term *term, K8TCmdLine *cmdline, int delta) {
  if (delta) {
    if (cmdHistoryCurrent < 0) {
      // save current command line
      snprintf(cmdLineCur, sizeof(cmdLineCur), "%s", cmdline->cmdline);
      cmdHistoryCurrent = (delta < 0 ? cmdHistoryCount-1 : 0);
    } else {
      cmdHistoryCurrent += (delta < 0 ? -1 : 1);
    }
    if (cmdHistoryCurrent < 0 || cmdHistoryCurrent >= cmdHistoryCount) {
      cmdHistoryCurrent = -1;
      // restore command line
      snprintf(cmdline->cmdline, sizeof(cmdline->cmdline), "%s", cmdLineCur);
    } else {
      snprintf(cmdline->cmdline, sizeof(cmdline->cmdline), "%s", cmdHistory[cmdHistoryCurrent]);
    }
    tcmdlinefixofs(term, cmdline);
  }
}


// return !0 if event was eaten
static int tcmdlProcessKeys (K8Term *term, K8TCmdLine *cmdline, KeySym ksym, const char *ksbuf, int ksbuflen, XKeyEvent *e) {
  int mode = cmdline->cmdMode;
  switch (ksym) {
    case XK_Insert:
      if (cmdline->cmdline[0] && !(e->state&(Mod1Mask|ShiftMask)) && (e->state&ControlMask)) {
        k8t_selClear(term);
        if (lastSelStr != NULL) free(lastSelStr);
        lastSelStr = strdup(cmdline->cmdline);
        xfixsel();
      }
      break;
    case XK_Return:
      tcmdlinehide(term, cmdline);
      if (cmdline->cmdexecfn != NULL) {
        cmdline->cmdexecfn(term, cmdline, 0);
      } else if (mode == K8T_CMDMODE_INPUT) {
        cmdAddToHistory(cmdline->cmdline);
        executeCommands(cmdline->cmdline);
      }
      break;
    case XK_BackSpace:
      if (mode == K8T_CMDMODE_INPUT) {
        cmdHistoryCurrent = -1;
        tcmdlinechoplast(term, cmdline);
        cmdline->cmdtabpos = -1;
        cmdline->cmdcurtabc = NULL;
      } else {
        tcmdlinehide(term, cmdline);
        if (cmdline->cmdexecfn != NULL) cmdline->cmdexecfn(term, cmdline, 1);
      }
      break;
    case XK_Escape:
      tcmdlinehide(term, cmdline);
      if (cmdline->cmdexecfn != NULL) cmdline->cmdexecfn(term, cmdline, 1);
      break;
    case XK_Tab:
      if (mode == K8T_CMDMODE_INPUT && cmdline->cmdline[0] && cmdline->cmdcl == 0 && cmdline->cmdexecfn == NULL) {
        const char *cpl;
        cmdHistoryCurrent = -1;
        if (cmdline->cmdtabpos < 0) {
          cmdline->cmdtabpos = 0;
          while (cmdline->cmdline[cmdline->cmdtabpos] && isalnum(cmdline->cmdline[cmdline->cmdtabpos])) ++cmdline->cmdtabpos;
          if (cmdline->cmdline[cmdline->cmdtabpos]) {
            cmdline->cmdtabpos = -1;
            break;
          }
          cmdline->cmdcurtabc = NULL;
        }
        cpl = findCommandCompletion(cmdline->cmdline, cmdline->cmdtabpos, cmdline->cmdcurtabc);
        if (cpl == NULL && cmdline->cmdcurtabc != NULL) cpl = findCommandCompletion(cmdline->cmdline, cmdline->cmdtabpos, NULL);
        cmdline->cmdcurtabc = cpl;
        if (cpl != NULL) { strcpy(cmdline->cmdline, cpl); tcmdaddchar(term, cmdline, " "); }
        tcmdlinefixofs(term, cmdline);
      } else if (mode != K8T_CMDMODE_INPUT) {
        tcmdlinehide(term, cmdline);
        if (cmdline->cmdexecfn != NULL) cmdline->cmdexecfn(term, cmdline, 1);
      }
      break;
    case XK_Up: case XK_KP_8:
      if (mode == K8T_CMDMODE_INPUT && cmdline->cmdexecfn == NULL) tcmdlHistoryMove(term, cmdline, -1);
      break;
    case XK_Down: case XK_KP_2:
      if (mode == K8T_CMDMODE_INPUT && cmdline->cmdexecfn == NULL) tcmdlHistoryMove(term, cmdline, 1);
      break;
    case XK_space:
      if (mode != K8T_CMDMODE_INPUT) {
        tcmdlinehide(term, cmdline);
        break;
      }
      // fallthru
    default:
      if (mode == K8T_CMDMODE_INPUT) {
        if (ksbuflen > 0 && (unsigned char)ksbuf[0] >= 32) {
          cmdHistoryCurrent = -1;
          tcmdput(term, cmdline, ksbuf, ksbuflen);
          cmdline->cmdtabpos = -1;
          cmdline->cmdcurtabc = NULL;
        }
      }
      break;
  }
  //
  return 1; // eat any event
}
