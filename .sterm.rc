# select one of two predefined terminals below
#term  xterm
term  rxvt

title  sterm
class  sterm

doubleclicktimeout  300
tripleclicktimeout  600

drawtimeout  30
maxdrawtimeout 2500  # 100..60000, milliseconds
swapdrawtimeout 200  # max timeout after screen swapping
fastredraw no  # don't set this to yes, it actually will SLOWDOWN redraw
fastupdate yes  # this can eat alot of resources if terminal is updating very fast

# -1: don't clear
# 0: only chars (don't touch attributes)
# 1: default mode (clear to default colors)
# 2: smart mode (clear to the colors of the last line)
scrollclear 1

audiblebell  1

# set 'urgent' flag on '\a' if terminal window is inactive
urgentbell  1


# set to 1 if you want sterm to ignore WM close requests
# set to 0 if you want close query
# set to -1 if you want sterm to die silently
ignoreclose 0

# 0: none
# 1: center
# 2: left
# 3: right
tabellipsis 1

#visiblebell  0  # not yet

ptrblank 1500
#tabcount 2
#tabposition 1

fontnorm  "-*-terminus-bold-*-*-*-22-*-*-*-*-*-iso10646-1"
fontbold  "-*-terminus-bold-*-*-*-22-*-*-*-*-*-iso10646-1"
#fonttab   "-*-andale mono-*-*-*-*-17-*-*-*-*-*-iso10646-1"
#fonttab   "-*-calibri-medium-r-*-*-17-*-*-*-*-*-iso10646-1"
fonttab   "-*-helvetica-medium-r-*-*-14-*-*-*-*-*-iso10646-1"

tabsize  8

# default shell to use if SHELL is not set in the env
shell /bin/sh

# 8 normal colors
color.0   "#000000"
color.1   "#b21818"
color.2   "#18b218"
color.3   "#b26818"
color.4   "#1818b2"
color.5   "#b218b2"
color.6   "#18b2b2"
color.7   "#b2b2b2"
# 8 bright colors
color.8   "#686868"
color.9   "#ff5454"
color.10  "#54ff54"
color.11  "#ffff54"
color.12  "#5454ff"
color.13  "#ff54ff"
color.14  "#54ffff"
color.15  "#ffffff"


# xterm colors
## color.0   "#000000"
## color.1   "#cd0000"
## color.2   "#00cd00"
## color.3   "#cdcd00"
## color.4   "#0000ee"
## color.5   "#cd00cd"
## color.6   "#00cdcd"
## color.7   "#e5e5e5"
## color.8   "#7f7f7f"
## color.9   "#ff0000"
## color.10  "#00ff00"
## color.11  "#ffff00"
## color.12  "#5c5cff"
## color.13  "#ff00ff"
## color.14  "#00ffff"
## color.15  "#ffffff"


# more colors can be added after 255 to use with DefaultXX
# for cursor
color.256  "#cccccc"
color.257  "#333333"
# root terminal fg and bg
color.258  "#809a70"
color.259  "#002000"
# bold and underline
color.260  "#00afaf"
color.261  "#00af00"
# for blinking cursor
color.262  "#00ff00"
color.263  "#00cc00"
# inactive cursor
color.264  "#009900"
# active cursor fgs
color.265  "#005500"
color.266  "#005500"


defaultFG        7
defaultBG        0
defaultCursorFG  265
defaultCursorBG  262
defaultCursorFG1 266
defaultCursorBG1 263
defaultInactiveCursorFG  0
defaultInactiveCursorBG  -1
#defaultInactiveCursorBG  264

defaultBoldFG       260
defaultUnderlineFG  261
drawunderline  0  # heh; draw 'underline underline'?

normalTabFG  258
normalTabBG  257
activeTabFG  258
activeTabBG  0


#cursorBlink 0 -- turn off; time in milliseconds
cursorBlink  700
#cursorBlinkInactive  1  # boolean


maxhistory 512


# unicode --> linedrawing
# TODO: double-frames (160-190)
# 'alt' means 'alternate (line drawing) charset'
# '0' is special value: 'inverted space'
#unimap 0x2000 0x3F
#unimap 0x2592 0x61 alt
#unimap 0x2588 0x00 alt
#unimap 0x2589 0x00 alt

unimap 0x2518 0x6a alt
unimap 0x2510 0x6b alt
unimap 0x250c 0x6c alt
unimap 0x2514 0x6d alt
unimap 0x253c 0x6e alt
unimap 0x2500 0x71 alt
unimap 0x251c 0x74 alt
unimap 0x2524 0x75 alt
unimap 0x2534 0x76 alt
unimap 0x252c 0x77 alt
unimap 0x2502 0x78 alt

unimap 0x2580 0x8b
unimap 0x2584 0x8c
unimap 0x258c 0x8e
unimap 0x2590 0x8f

unimap 0x2588 0x8d
unimap 0x25a0 0x8d
unimap 0x2591 0x90
unimap 0x2592 0x91
unimap 0x2593 0x92


# key bindings
keybind reset

# X11 clipboards
keybind shift+Insert      PastePrimary
keybind alt+Insert        PasteClipboard
keybind alt+shift+Insert  PasteSecondary
keybind ctrl+Delete       HideSelection

# new tab
keybind ctrl+alt+t  NewTab
keybind ctrl+alt+m  NewTab; exec mc

# new utf-8 tab
keybind ctrl+alt+u  NewTab; UTF8Locale tan

# new root tab
#keybind ctrl+alt+r  NewTab; defaultFG 258; defaultBG 259; exec su -
keybind ctrl+alt+r  NewTab; mono 2; defaultBG 259; exec su -
# the following will not work due to security fix in su. fuck!
#keybind ctrl+alt+p  NewTab; mono 2; defaultFG 258; defaultBG 259; exec su -l -c mc
keybind ctrl+alt+p  NewTab; mono 2; defaultFG 258; defaultBG 259; exec sudo -u root -i mc

# on-the-fly switching between UTF-8 and default locale
keybind win+alt+8  UTF8Locale tan
keybind win+alt+1  UTF8Locale ona

keybind win+alt+b  AudibleBell toggle

# press win+c to enter 'command mode'
keybind win+c  CommandMode

keybind ctrl+win+q  CloseTab
keybind ctrl+win+k  KillTab

keybind ctrl+alt+Left   SwitchToTab prev nowrap
keybind ctrl+alt+Right  SwitchToTab next nowrap
keybind ctrl+alt+Home   SwitchToTab first
keybind ctrl+alt+End    SwitchToTab last

keybind ctrl+alt+1      SwitchToTab 1
keybind ctrl+alt+2      SwitchToTab 2
keybind ctrl+alt+3      SwitchToTab 3
keybind ctrl+alt+4      SwitchToTab 4
keybind ctrl+alt+5      SwitchToTab 5
keybind ctrl+alt+6      SwitchToTab 6
keybind ctrl+alt+7      SwitchToTab 7
keybind ctrl+alt+8      SwitchToTab 8
keybind ctrl+alt+9      SwitchToTab 9
keybind ctrl+alt+0      SwitchToTab 0

keybind alt+shift+Left   MoveTabTo prev nowrap
keybind alt+shift+Right  MoveTabTo next nowrap

keybind alt+Shift+Up     ScrollHistoryLineUp
keybind alt+Shift+Down   ScrollHistoryLineDown
keybind alt+Shift+Prior  ScrollHistoryPageUp
keybind alt+Shift+Next   ScrollHistoryPageDown
keybind alt+Shift+Home   ScrollHistoryTop
keybind alt+Shift+End    ScrollHistoryBottom

# keypad translation for keymap (only!)
keytrans reset
keytrans KP_Home    Home
keytrans KP_Left    Left
keytrans KP_Up      Up
keytrans KP_Right   Right
keytrans KP_Down    Down
keytrans KP_Prior   Prior
keytrans KP_Next    Next
keytrans KP_End     End
keytrans KP_Begin   Begin
keytrans KP_Insert  Insert
keytrans KP_Delete  Delete
# x11 is fucked
keytrans ISO_Left_Tab Tab


# keycodes
# modificators: ctrl, shift, alt, win, any
keymap reset
keymap BackSpace  \177
keymap Insert     \e[2~
keymap Delete     \e[3~
ifterm "rxvt"
  keymap Home       \e[7~
  keymap End        \e[8~
else
  keymap Home       \e[1~
  keymap End        \e[4~
endif
keymap Prior      \e[5~
keymap Next       \e[6~

keymap ctrl+Home    \e[1^
keymap ctrl+Insert  \e[2^
keymap ctrl+Delete  \e[3^
keymap ctrl+End     \e[4^
keymap ctrl+Prior   \e[5^
keymap ctrl+Next    \e[6^

ifterm "xterm"
  # xterm
  keymap F1   \eOP
  keymap F2   \eOQ
  keymap F3   \eOR
  keymap F4   \eOS
else
  # rxvt
  keymap F1   \e[11~
  keymap F2   \e[12~
  keymap F3   \e[13~
  keymap F4   \e[14~
endif
keymap F5   \e[15~
keymap F6   \e[17~
keymap F7   \e[18~
keymap F8   \e[19~
keymap F9   \e[20~
keymap F10  \e[21~
keymap F11  \e[23~
keymap F12  \e[24~

keymap shift+F1   \e[23~
keymap shift+F2   \e[24~
keymap shift+F3   \e[25~
keymap shift+F4   \e[26~
keymap shift+F5   \e[28~
keymap shift+F6   \e[29~
keymap shift+F7   \e[31~
keymap shift+F8   \e[32~
keymap shift+F9   \e[33~
keymap shift+F10  \e[34~

keymap ctrl+F1   \e[11^
keymap ctrl+F2   \e[12^
keymap ctrl+F3   \e[13^
keymap ctrl+F4   \e[14^
keymap ctrl+F5   \e[15^
keymap ctrl+F6   \e[17^
keymap ctrl+F7   \e[18^
keymap ctrl+F8   \e[19^
keymap ctrl+F9   \e[20^
keymap ctrl+F10  \e[21^

keymap ctrl+shift+F1   \e[11@
keymap ctrl+shift+F2   \e[12@
keymap ctrl+shift+F3   \e[13@
keymap ctrl+shift+F4   \e[14@
keymap ctrl+shift+F5   \e[15@
keymap ctrl+shift+F6   \e[17@
keymap ctrl+shift+F7   \e[18@
keymap ctrl+shift+F8   \e[19@
keymap ctrl+shift+F9   \e[20@
keymap ctrl+shift+F10  \e[21@

ifterm "xterm"
 # xterm
 keymap Up     \eOA
 keymap Down   \eOB
 keymap Right  \eOC
 keymap Left   \eOD
else
 # rxvt
  keymap Up     \e[A
  keymap Down   \e[B
  keymap Right  \e[C
  keymap Left   \e[D
  #
  keymap kpad+Up     \eOA
  keymap kpad+Down   \eOB
  keymap kpad+Right  \eOC
  keymap kpad+Left   \eOD
  #
endif

### keymap kpad+Up     \eOA
### keymap kpad+Down   \eOB
### keymap kpad+Right  \eOC
### keymap kpad+Left   \eOD

keymap shift+Up     \e[1;2A
keymap shift+Down   \e[1;2B
keymap shift+Right  \e[1;2C
keymap shift+Left   \e[1;2D

keymap alt+Up     \e[1;3A
keymap alt+Down   \e[1;3B
keymap alt+Right  \e[1;3C
keymap alt+Left   \e[1;3D

keymap shift+alt+Up     \e[1;4A
keymap shift+alt+Down   \e[1;4B
keymap shift+alt+Right  \e[1;4C
keymap shift+alt+Left   \e[1;4D

keymap ctrl+Up     \e[1;5A
keymap ctrl+Down   \e[1;5B
keymap ctrl+Right  \e[1;5C
keymap ctrl+Left   \e[1;5D

keymap ctrl+shift+Up     \e[1;6A
keymap ctrl+shift+Down   \e[1;6B
keymap ctrl+shift+Right  \e[1;6C
keymap ctrl+shift+Left   \e[1;6D

keymap ctrl+alt+Up     \e[1;7A
keymap ctrl+alt+Down   \e[1;7B
keymap ctrl+alt+Right  \e[1;7C
keymap ctrl+alt+Left   \e[1;7D

keymap ctrl+alt+shift+Up     \e[1;8A
keymap ctrl+alt+shift+Down   \e[1;8B
keymap ctrl+alt+shift+Right  \e[1;8C
keymap ctrl+alt+shift+Left   \e[1;8D

#keymap alt+Return  \e\x0a


ifterm "rxvt"
  keymap Ctrl+Prior  \e\133\065\136
  keymap Ctrl+Next   \e\133\066\136
  keymap Ctrl+Home   \e\133\067\136
  keymap Ctrl+End    \e\133\070\136
  keymap Shift+Home  \e\133\067\044
  keymap Shift+End   \e\133\070\044
  keymap Shift+F1    \e\133\062\063\176
  keymap Shift+F2    \e\133\062\064\176
  keymap Shift+F3    \e\133\062\065\176
  keymap Shift+F4    \e\133\062\066\176
  keymap Shift+F5    \e\133\062\070\176
  keymap Shift+F6    \e\133\062\071\176
  keymap Shift+F7    \e\133\063\061\176
  keymap Shift+F8    \e\133\063\062\176
  keymap Shift+F9    \e\133\063\063\176
  keymap Shift+F10   \e\133\063\064\176
  #keymap Alt+Return  \e\012
  #keymap Alt+Tab     \e\t
endif


# keycodes
# modificators: ctrl, shift, alt, win, any
keymap reset
keymap BackSpace  \177
keymap Insert     \e[2~
keymap Delete     \e[3~
ifterm "rxvt"
  keymap Home       \e[7~
  keymap End        \e[8~
else
  keymap Home       \e[1~
  keymap End        \e[4~
endif
keymap Prior      \e[5~
keymap Next       \e[6~

keymap ctrl+Home    \e[1^
keymap ctrl+Insert  \e[2^
keymap ctrl+Delete  \e[3^
keymap ctrl+End     \e[4^
keymap ctrl+Prior   \e[5^
keymap ctrl+Next    \e[6^

ifterm "xterm"
  # xterm
  keymap F1   \eOP
  keymap F2   \eOQ
  keymap F3   \eOR
  keymap F4   \eOS
else
  # rxvt
  keymap F1   \e[11~
  keymap F2   \e[12~
  keymap F3   \e[13~
  keymap F4   \e[14~
endif
keymap F5   \e[15~
keymap F6   \e[17~
keymap F7   \e[18~
keymap F8   \e[19~
keymap F9   \e[20~
keymap F10  \e[21~
keymap F11  \e[23~
keymap F12  \e[24~

keymap ctrl+F1   \e[11^
keymap ctrl+F2   \e[12^
keymap ctrl+F3   \e[13^
keymap ctrl+F4   \e[14^
keymap ctrl+F5   \e[15^
keymap ctrl+F6   \e[17^
keymap ctrl+F7   \e[18^
keymap ctrl+F8   \e[19^
keymap ctrl+F9   \e[20^
keymap ctrl+F10  \e[21^

ifterm "xterm"
  # xterm
  keymap alt+F1   \e[1;3P
  keymap alt+F2   \e[1;3Q
  keymap alt+F3   \e[1;3R
  keymap alt+F4   \e[1;3S
else
  # rxvt
  keymap alt+F1   \e[11;3~
  keymap alt+F2   \e[12;3~
  keymap alt+F3   \e[13;3~
  keymap alt+F4   \e[14;3~
endif
keymap alt+F5   \e[15;3~
keymap alt+F6   \e[17;3~
keymap alt+F7   \e[18;3~
keymap alt+F8   \e[19;3~
keymap alt+F9   \e[20;3~
keymap alt+F10  \e[21;3~
keymap alt+F11  \e[23;3~
keymap alt+F12  \e[24;3~

keymap shift+F1   \e[23~
keymap shift+F2   \e[24~
keymap shift+F3   \e[25~
keymap shift+F4   \e[26~
keymap shift+F5   \e[28~
keymap shift+F6   \e[29~
keymap shift+F7   \e[31~
keymap shift+F8   \e[32~
keymap shift+F9   \e[33~
keymap shift+F10  \e[34~

keymap ctrl+shift+F1   \e[11@
keymap ctrl+shift+F2   \e[12@
keymap ctrl+shift+F3   \e[13@
keymap ctrl+shift+F4   \e[14@
keymap ctrl+shift+F5   \e[15@
keymap ctrl+shift+F6   \e[17@
keymap ctrl+shift+F7   \e[18@
keymap ctrl+shift+F8   \e[19@
keymap ctrl+shift+F9   \e[20@
keymap ctrl+shift+F10  \e[21@

ifterm "xterm"
  # xterm
  keymap alt+shift+F1   \e[1;4P
  keymap alt+shift+F2   \e[1;4Q
  keymap alt+shift+F3   \e[1;4R
  keymap alt+shift+F4   \e[1;4S
else
  # rxvt
  keymap alt+shift+F1   \e[11;4~
  keymap alt+shift+F2   \e[12;4~
  keymap alt+shift+F3   \e[13;4~
  keymap alt+shift+F4   \e[14;4~
endif
keymap alt+shift+F5   \e[15;4~
keymap alt+shift+F6   \e[17;4~
keymap alt+shift+F7   \e[18;4~
keymap alt+shift+F8   \e[19;4~
keymap alt+shift+F9   \e[20;4~
keymap alt+shift+F10  \e[21;4~
keymap alt+shift+F11  \e[23;4~
keymap alt+shift+F12  \e[24;4~

ifterm "xterm"
  # xterm
  keymap ctrl+alt+F1   \e[1;7P
  keymap ctrl+alt+F2   \e[1;7Q
  keymap ctrl+alt+F3   \e[1;7R
  keymap ctrl+alt+F4   \e[1;7S
else
  # rxvt
  keymap ctrl+alt+F1   \e[11;7~
  keymap ctrl+alt+F2   \e[12;7~
  keymap ctrl+alt+F3   \e[13;7~
  keymap ctrl+alt+F4   \e[14;7~
endif
keymap ctrl+alt+F5   \e[15;7~
keymap ctrl+alt+F6   \e[17;7~
keymap ctrl+alt+F7   \e[18;7~
keymap ctrl+alt+F8   \e[19;7~
keymap ctrl+alt+F9   \e[20;7~
keymap ctrl+alt+F10  \e[21;7~
keymap ctrl+alt+F11  \e[23;7~
keymap ctrl+alt+F12  \e[24;7~

ifterm "xterm"
  # xterm
  keymap ctrl+alt+shift+F1   \e[1;8P
  keymap ctrl+alt+shift+F2   \e[1;8Q
  keymap ctrl+alt+shift+F3   \e[1;8R
  keymap ctrl+alt+shift+F4   \e[1;8S
else
  # rxvt
  keymap ctrl+alt+shift+F1   \e[11;8~
  keymap ctrl+alt+shift+F2   \e[12;8~
  keymap ctrl+alt+shift+F3   \e[13;8~
  keymap ctrl+alt+shift+F4   \e[14;8~
endif
keymap ctrl+alt+shift+F5   \e[15;8~
keymap ctrl+alt+shift+F6   \e[17;8~
keymap ctrl+alt+shift+F7   \e[18;8~
keymap ctrl+alt+shift+F8   \e[19;8~
keymap ctrl+alt+shift+F9   \e[20;8~
keymap ctrl+alt+shift+F10  \e[21;8~
keymap ctrl+alt+shift+F11  \e[23;8~
keymap ctrl+alt+shift+F12  \e[24;8~


ifterm "xterm"
 # xterm
 keymap Up     \eOA
 keymap Down   \eOB
 keymap Right  \eOC
 keymap Left   \eOD
else
 # rxvt
  keymap Up     \e[A
  keymap Down   \e[B
  keymap Right  \e[C
  keymap Left   \e[D
  #
  keymap kpad+Up     \eOA
  keymap kpad+Down   \eOB
  keymap kpad+Right  \eOC
  keymap kpad+Left   \eOD
  #
endif

### keymap kpad+Up     \eOA
### keymap kpad+Down   \eOB
### keymap kpad+Right  \eOC
### keymap kpad+Left   \eOD

keymap shift+Up     \e[1;2A
keymap shift+Down   \e[1;2B
keymap shift+Right  \e[1;2C
keymap shift+Left   \e[1;2D

keymap alt+Up     \e[1;3A
keymap alt+Down   \e[1;3B
keymap alt+Right  \e[1;3C
keymap alt+Left   \e[1;3D

keymap shift+alt+Up     \e[1;4A
keymap shift+alt+Down   \e[1;4B
keymap shift+alt+Right  \e[1;4C
keymap shift+alt+Left   \e[1;4D

keymap ctrl+Up     \e[1;5A
keymap ctrl+Down   \e[1;5B
keymap ctrl+Right  \e[1;5C
keymap ctrl+Left   \e[1;5D

keymap ctrl+shift+Up     \e[1;6A
keymap ctrl+shift+Down   \e[1;6B
keymap ctrl+shift+Right  \e[1;6C
keymap ctrl+shift+Left   \e[1;6D

keymap ctrl+alt+Up     \e[1;7A
keymap ctrl+alt+Down   \e[1;7B
keymap ctrl+alt+Right  \e[1;7C
keymap ctrl+alt+Left   \e[1;7D

keymap ctrl+alt+shift+Up     \e[1;8A
keymap ctrl+alt+shift+Down   \e[1;8B
keymap ctrl+alt+shift+Right  \e[1;8C
keymap ctrl+alt+shift+Left   \e[1;8D

#keymap alt+Return  \e\x0a


ifterm "rxvt"
  keymap Ctrl+Prior  \e\133\065\136
  keymap Ctrl+Next   \e\133\066\136
  keymap Ctrl+Home   \e\133\067\136
  keymap Ctrl+End    \e\133\070\136
  keymap Shift+Home  \e\133\067\044
  keymap Shift+End   \e\133\070\044
  keymap Shift+F1    \e\133\062\063\176
  keymap Shift+F2    \e\133\062\064\176
  keymap Shift+F3    \e\133\062\065\176
  keymap Shift+F4    \e\133\062\066\176
  keymap Shift+F5    \e\133\062\070\176
  keymap Shift+F6    \e\133\062\071\176
  keymap Shift+F7    \e\133\063\061\176
  keymap Shift+F8    \e\133\063\062\176
  keymap Shift+F9    \e\133\063\063\176
  keymap Shift+F10   \e\133\063\064\176
  #keymap Alt+Return  \e\012
  #keymap Alt+Tab     \e\t
endif

# xterm does this (my: \e[1;2Z)
keymap shift+Tab  \e[Z
# x11 is fucked: this is for shift+tab
#keymap shift+ISO_Left_Tab  \e[Z
# my own
#keymap alt+Tab  \e[1;3Z
keymap alt+shift+Tab       \e[1;4Z
keymap ctrl+Tab            \e[1;5Z
keymap ctrl+shift+Tab      \e[1;6Z
keymap alt+ctrl+Tab        \e[1;7Z
keymap alt+ctrl+shift+Tab  \e[1;8Z
